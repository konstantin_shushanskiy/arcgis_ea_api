/**
 * Модуль содержащий класс HTML кнопки включения/выключения панорам
 *
 * @module gcapi/panorams/UI
 * @see gcapi.panorams.UI
 * @author Шушанский К.П.
 */
define("gcapi/panorams/UI", [
        "dojo/_base/declare"
    ],
    function (declare) {
        /**
         * Класс HTML кнопки включения/выключения панорам
         *
         * Может работать в связке с инстансом [gcapi.panorams.Tool]{@link gcapi.panorams.Tool}
         * @class gcapi.panorams.UI
         * @alias gcapi.panorams.UI
         * @memberof! <global>
         */
        return declare("gcapi.panorams.UI", [], {


        });
    }
);
