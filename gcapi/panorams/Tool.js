/**
 * Модуль содержащий класс контроллера панорам
 *
 * @module gcapi/panorams/Tool
 * @see gcapi.panorams.Tool
 * @author Шушанский К.П.
 */
define("gcapi/panorams/Tool", [
        "dojo/_base/declare"
    ], function (declare) {

        /**
         * Класс инструмента измерения расстояния на карте
         * Может работать в связке с графическим элементом [gcapi.panorams.UI]{@link gcapi.panorams.UI}
         *
         * @class gcapi.panorams.Tool
         * @alias gcapi.panorams.Tool
         * @memberof! <global>
         */
        return declare("gcapi.panorams.Tool", [], {

            /**
             * Cсылка на инстанс esri.Map
             * @member {esri.Map} _mapInstance
             * @default undefined
             * @memberof gcapi.panorams.Tool
             * @instance
             * @private
             */
            _mapInstance: undefined,

            /**
             * Будет вызвана автоматически при добавлении модуля на карту
             * @memberof gcapi.panorams.Tool
             * @function _bind
             * @param {gcapi.Map} map Инстанс gcapi.Map
             * @instance
             * @private
             */
            _bind: function (map) {
                var me = this;
                me._mapInstance = map;
            }

        });
    }
);