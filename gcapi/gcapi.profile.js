var profile = (function () {

    var copyOnly = function (filename, mid) {
        var list = {
            'gcapi/gcapi.profile': true,
            'gcapi/package.json': true,
            'gcapi/zoom/template': true
        };
        return (mid in list)
    };

    return {
        resourceTags: {
            copyOnly: function (filename, mid) {
                return copyOnly(filename, mid);
            },

            amd: function (filename, mid) {
                return !copyOnly(filename, mid)
                    && /\.js$/.test(filename);
            }
        },

        trees: [
            [".", ".", /(\/\.)|(~$)/]
        ],

        basePath: "./",
        //layerOptimize: "shrinksafe",
        releaseDir: "../tmp_release",
        hasReport: true,
        //optimize: "shrinksafe",
        action: "clean,release",
        //mini: true,
        selectorEngine: 'acme',
        localeList: ['en', 'ru'],

        staticHasFeatures: {
            'dojo-firebug': false,
            'dojo-debug-messages': false,
            'dojo-preload-i18n-Api': true,
            'dojo-amd-factory-scan': false,
            'dojo-v1x-i18n-Api': true,
            'dojo-amd-factory-scan': true
        },

        packages: [
            {
                name: "gcapi",
                location: "./"
            },
            {
                name: "dojo",
                location: "../vendor/dojo-release-1.7.3-src/dojo/"
            },
            {
                name: "dijit",
                location: "../vendor/dojo-release-1.7.3-src/dijit/"
            },
            {
                name: "dojox",
                location: "../vendor/dojo-release-1.7.3-src/dojox/"
            },
            {
                name: "esri",
                location: "../vendor/arcgis_js_v32_api/arcgis_js_api/library/3.2/jsapi/js/esri/"
            }

        ],

        layers: {
            "gcapi": {
                include: [
                    "dojo/require",

                    "dijit/main",
                    
                    "dijit/_base",

                    "dijit/_base/focus",

                    "dijit/_base/place",

                    "dijit/_base/popup",

                    "dijit/_base/scroll",

                    "dijit/_base/sniff",

                    "dijit/_base/typematic",

                    "dijit/_base/wai",

                    "dijit/_base/window",

                    "dijit/form/ComboButton",

                    "dojox/gfx/svg",

                    "dojox/gfx/shape",

                    "dojox/gfx/path",

                    "dojo/fx/Toggler",

                    "gcapi/main",
                    'gcapi/Config',
                    'gcapi/Map',
                    "gcapi/Jams",
                    "gcapi/Zoom",
                    "gcapi/LayerSwitcher",
                    "gcapi/MeasureLine",
                    "gcapi/MeasureArea",
                    "gcapi/Search",
                    "gcapi/Panorams",
                    "gcapi/TraceRoute",
                    "gcapi/PublicTransport",
                    "gcapi/Catalog",
                    "gcapi/LanguageSwitcher"
                ]
            }
        }

    };
})();
