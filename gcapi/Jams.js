/**
 * Модуль содержащий класс связывающий кнопку включения/выключения пробок на карте с модулем включения/выключения пробок на карте
 * @module gcapi/Jams
 * @see gcapi.Jams
 * @author Шушанский К.П.
 */
define("gcapi/Jams", [
        "dojo/_base/declare",
        "gcapi/jams/Tool",
        "gcapi/jams/UI"
    ],
    function (declare, toolJams, uiJams) {

        /**
         * Класс связывающий кнопку включения/выключения пробок на карте с модулем включения/выключения пробок на карте
         * @class gcapi.Jams
         * @alias gcapi.Jams
         * @param {gcapi.Map} map Инстанс gcapi.Map
         * @memberof! <global>
         */
        return declare("gcapi.Jams", [], {
            
            constructor: function(instance){
                var jamsButton = new uiJams();
                
                var jamsTool = new toolJams();

                instance.add(jamsButton);

                instance.add(jamsTool);

                jamsButton.onClick = function () {
                    this.checked ? jamsTool.show() : jamsTool.hide();
                };

                instance.on('langChange', function(lang){
                    jamsButton.changeLang(lang)
                });

            }
            
        });
    }
);