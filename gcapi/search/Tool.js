/**
 * Модуль содержащий класс контроллера поиска
 *
 * @module gcapi/search/Tool
 * @see gcapi.search.Tool
 * @author Шушанский К.П.
 */
define("gcapi/search/Tool", [
        "dojo/_base/declare"
    ], function (declare) {

        /**
         * Класс инструмента измерения расстояния на карте
         * Может работать в связке с графическим элементом [gcapi.search.UI]{@link gcapi.search.UI}
         *
         * @class gcapi.search.Tool
         * @alias gcapi.search.Tool
         * @memberof! <global>
         */
        return declare("gcapi.search.Tool", [], {

            /**
             * Cсылка на инстанс esri.Map
             * @member {esri.Map} _mapInstance
             * @default undefined
             * @memberof gcapi.search.Tool
             * @instance
             * @private
             */
            _mapInstance: undefined,

            /**
             * Будет вызвана автоматически при добавлении модуля на карту
             * @memberof gcapi.search.Tool
             * @function _bind
             * @param {gcapi.Map} map Инстанс gcapi.Map
             * @instance
             * @private
             */
            _bind: function (map) {
                var me = this;
                me._mapInstance = map;
            }

        });
    }
);