/**
 * Модуль содержащий класс HTML контрола поиска
 *
 * @module gcapi/search/UI
 * @see gcapi.search.UI
 * @author Шушанский К.П.
 */
define("gcapi/search/UI", [
        "dojo/_base/declare"
    ],
    function (declare) {
        /**
         * Класс HTML контрола поиска
         *
         * Может работать в связке с инстансом [gcapi.search.Tool]{@link gcapi.search.Tool}
         * @class gcapi.search.UI
         * @alias gcapi.search.UI
         * @memberof! <global>
         */
        return declare("gcapi.search.UI", [], {


        });
    }
);
