/**
 * Модуль содержащий класс включения/выключения пробок на карте
 *
 * @module gcapi/jams/Tool
 * @see gcapi.jams.Tool
 * @author Шушанский К.П.
 */
define("gcapi/jams/Tool", [
    "dojo/_base/declare",
    "esri/layers/WebTiledLayer"
], function (declare, WebTiledLayer) {

    /**
     * Класс включения/выключения пробок на карте.
     * Включает/выключает отображение пробок на карте.
     * Может работать в связке с графическим элементом [gcapi.jams.UI]{@link gcapi.jams.UI}
     * @class gcapi.jams.Tool
     * @alias gcapi.jams.Tool
     * @memberof! <global>
     */
    return declare("gcapi.jams.Tool", [], {
        
        /**
         * Cсылка на инстанс esri.Map
         * @member {esri.Map} _mapInstance
         * @default undefined
         * @memberof gcapi.jams.Tool
         * @instance
         * @private
         */
        _mapInstance: undefined,

        /**
         * Cсылка на инстанс esri.layer.WebTiledLayer
         * @member {esri.layer.WebTiledLayer} _layer
         * @default undefined
         * @memberof gcapi.jams.Tool
         * @instance
         * @private
         */
        _layer: undefined,

        /**
         * Имя сервера с информацией о пробках
         * Заполняется из объекта [gc.Config.jams]{@link gc.Config}
         * @member {String} _server
         * @default undefined
         * @memberof gcapi.jams.Tool
         * @instance
         * @private
         */
        _server: undefined,

        /**
         * Url слоя с пробками.
         * Например: "CODD_Jams/${level}/${col}/${row}.png"
         * Заполняется из объекта [gc.Config.jams]{@link gc.Config} 
         * @member {String} _url
         * @default undefined
         * @memberof gcapi.jams.Tool
         * @instance
         * @private
         */
        _url: undefined,

        /**
         * Интервал обновления слоя с пробками (в милисекундах)
         * Заполняется из объекта [gc.Config.jams]{@link gc.Config}
         * @member {Number} _updateInterval
         * @default undefined
         * @memberof gcapi.jams.Tool
         * @instance
         * @private
         */
        _updateInterval: undefined,

        /**
         * Системный идентификатор таймера обновления слоя
         * @member {Object} _timerId
         * @default undefined
         * @memberof gcapi.jams.Tool
         * @instance
         * @private
         */
        _timerId: undefined,

        constructor: function () {
            var me = this;
            
            me._server = gcapi.Config.jams.server;
            me._url = gcapi.Config.jams.url;
            me._updateInterval = gcapi.Config.jams.updateInterval * 1000;
            
            me._layer = new WebTiledLayer(me._server + me._url, {
                id: gcapi.Config.jams.id
            });
            
            me._layer.hide()
        },

        /**
         * Будет вызвана автоматически при добавлении модуля на карту
         * @memberof gcapi.jams.Tool
         * @function _bind 
         * @param {gcapi.Map} map Инстанс gcapi.Map
         * @instance
         * @private
         */
        _bind: function (map) {
            var me = this;
            me._mapInstance = map;

            me._mapInstance.addLayer(me._layer);
            me._runUpdating()
        },

        /**
         * Отображает слой с пробками на карте
         * @memberof gcapi.jams.Tool
         * @function show
         * @instance
         */
        show: function(){
            var me = this;
            
            me._layer && !me._layer.visible && me._layer.show() && me._runUpdating();
        },

        /**
         * Скрывает слой с пробками с карты
         * @memberof gcapi.jams.Tool
         * @function hide
         * @instance
         */
        hide: function(){
            var me = this;
            me._layer && me._layer.visible && me._layer.hide() && me._stopUpdating();
        },

        /**
         * Запускает таймер обновления слоя с пробками
         * @memberof gcapi.jams.Tool
         * @function _runUpdating
         * @instance
         * @private
         */
        _runUpdating: function () {
            var me = this;
            me._timerId && clearInterval(me._timerId);

            me._timerId = setInterval(function () {
                var d = new Date();

                var noCache = "?nocache=" + String(d.getTime());
                // здесь добавляется параметр для обхода кеширования
                me._layer.urlPath = me._url + noCache;
                //после refresh слой запросит новые данные с сервера
                me._layer.refresh();

            }, me._updateInterval);
        },

        /**
         * Останавливает таймер обновления слоя с пробками
         * @memberof gcapi.jams.Tool
         * @function _stopUpdating 
         * @instance
         * @private
         */
        _stopUpdating: function () {
            var me = this;
            me._timerId && clearInterval(me._timerId);
        }

    });
});