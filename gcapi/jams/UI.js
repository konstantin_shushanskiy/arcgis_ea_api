/**
 * Модуль содержащий класс кнопки включения/выключения пробок на карте
 * @module gcapi/jams/UI
 * @see gcapi.jams.UI
 * @author Шушанский К.П.
 */
define("gcapi/jams/UI", [
        "dojo/_base/declare",
        "dijit/form/ToggleButton",
        "dijit/Tooltip",
        "dojo/i18n",
        "dojo/i18n!../nls/jams"
    ],
    function (declare, ToggleButton, Tooltip, i18n, locale) {

        /**
         * Класс кнопки включения/выключения пробок.
         * Создаёт графический элемент кнопки.
         * Основан на dijit.form.ToggleButton
         * Может работать в связке с инстансом [gcapi.jams.Tool]{@link gcapi.jams.Tool}
         *
         * @class gcapi.jams.UI
         * @alias gcapi.jams.UI
         * @memberof! <global>
         */
        return declare("gcapi.jams.UI", [ToggleButton], {

            baseClass: 'gcApi_JamsControl',
            
            class: 'gcApi_CommonButton',

            tooltipWidget: undefined,
            
            postCreate: function(){
                var me = this;

                me.tooltipWidget = new Tooltip({
                    connectId: me.domNode,
                    label: locale.tooltip
                });
            },

            /**
             * Изменяет содержимое tooltip согласно заданой локали
             * @function changeLang
             * @param {String} lang - зык локализации
             * @memberof gcapi.jams.UI
             * @instance
             */
            changeLang: function(lang){
                var me = this;

                var locale = i18n.getLocalization("gcapi", "jams", lang);
                
                me.tooltipWidget.label = locale.tooltip;
            }
            
        });
    }
);