/**
 * Модуль содержащий класс для работы с сервисом общественного транспорта
 *
 * @module gcapi/publicTransport/Tool
 * @see gcapi.publicTransport.Tool
 * @author Шушанский К.П.
 */
define("gcapi/publicTransport/Tool", [
        "dojo/_base/declare"
    ], function (declare) {

        /**
         * Класс для работы с сервисом общественного транспорта
         * Может работать в связке с графическим элементом [gcapi.publicTransport.UI]{@link gcapi.publicTransport.UI}
         *
         * @class gcapi.publicTransport.Tool
         * @alias gcapi.publicTransport.Tool
         * @memberof! <global>
         */
        return declare("gcapi.publicTransport.Tool", [], {

            /**
             * Cсылка на инстанс esri.Map
             * @member {esri.Map} _mapInstance
             * @default undefined
             * @memberof gcapi.publicTransport.Tool
             * @instance
             * @private
             */
            _mapInstance: undefined,

            /**
             * Будет вызвана автоматически при добавлении модуля на карту
             * @memberof gcapi.publicTransport.Tool
             * @function _bind
             * @param {gcapi.Map} map Инстанс gcapi.Map
             * @instance
             * @private
             */
            _bind: function (map) {
                var me = this;
                me._mapInstance = map;
            }

        });
    }
);