/**
 * Модуль содержащий класс HTML контрола для работы с сервисом общественного транспорта
 *
 * @module gcapi/publicTransport/UI
 * @see gcapi.publicTransport.UI
 * @author Шушанский К.П.
 */
define("gcapi/publicTransport/UI", [
        "dojo/_base/declare"
    ],
    function (declare) {
        /**
         * Класс HTML контрола для работы с сервисом общественного транспорта
         *
         * Может работать в связке с инстансом [gcapi.publicTransport.Tool]{@link gcapi.publicTransport.Tool}
         * @class gcapi.publicTransport.UI
         * @alias gcapi.publicTransport.UI
         * @memberof! <global>
         */
        return declare("gcapi.publicTransport.UI", [], {


        });
    }
);
