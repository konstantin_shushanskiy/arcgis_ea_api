/**
 * Модуль содержащий класс связывающий кнопку включения/выключения инструмента измерения площади на карте с её контроллером
 *
 * @module gcapi/MeasureArea
 * @see gcapi.MeasureArea
 * @author Шушанский К.П.
 */
define("gcapi/MeasureArea", [
        "dojo/_base/declare",
        "gcapi/measureArea/Tool",
        "gcapi/measureArea/UI"
    ],
    function (declare, toolMeasureArea, uiMeasureArea) {

        /**
         * Класс связывающий кнопку включения/выключения инструмента измерения площади на карте с её контроллером
         * @class gcapi.MeasureArea
         * @alias gcapi.MeasureArea
         * @param {gcapi.Map} map Инстанс gcapi.Map
         * @memberof! <global>
         */
        return declare("gcapi.MeasureLine", [], {

            constructor: function(instance){
                var ui = new uiMeasureArea();
                var tool = new toolMeasureArea();
            }

        });
    }
);