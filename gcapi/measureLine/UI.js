/**
 * Модуль содержащий класс HTML кнопки включения/выключения инструмента измерения расстояния на карте
 *
 * @module gcapi/measureLine/UI
 * @see gcapi.measureLine.UI
 * @author Шушанский К.П.
 */
define("gcapi/measureLine/UI", [
        "dojo/_base/declare"
    ],
    function (declare) {
        /**
         * Класс кнопки включения/выключения инструмента измерения расстояния на карте
         *
         * Может работать в связке с инстансом [gcapi.measureLine.Tool]{@link gcapi.measureLine.Tool}
         * @class gcapi.measureLine.UI
         * @alias gcapi.measureLine.UI
         * @memberof! <global>
         */
        return declare("gcapi.measureLine.UI", [], {


        });
    }
);
