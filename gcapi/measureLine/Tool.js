/**
 * Модуль содержащий класс измерения расстояния на карте
 *
 * @module gcapi/measureLine/Tool
 * @see gcapi.measureLine.Tool
 * @author Шушанский К.П.
 */
define("gcapi/measureLine/Tool", [
        "dojo/_base/declare"
    ], function (declare) {

        /**
         * Класс инструмента измерения расстояния на карте
         * Может работать в связке с графическим элементом [gcapi.measureLine.UI]{@link gcapi.measureLine.UI}
         *
         * @class gcapi.measureLine.Tool
         * @alias gcapi.measureLine.Tool
         * @memberof! <global>
         */
        return declare("gcapi.measureLine.Tool", [], {

            /**
             * Cсылка на инстанс esri.Map
             * @member {esri.Map} _mapInstance
             * @default undefined
             * @memberof gcapi.measureLine.Tool
             * @instance
             * @private
             */
            _mapInstance: undefined,

            /**
             * Будет вызвана автоматически при добавлении модуля на карту
             * @memberof gcapi.measureLine.Tool
             * @function _bind
             * @param {gcapi.Map} map Инстанс gcapi.Map
             * @instance
             * @private
             */
            _bind: function (map) {
                var me = this;
                me._mapInstance = map;
            }

        });
    }
);