/**
 * Модуль содержащий класс HTML контрола переключения базовых слоёв карты
 *
 * @module gcapi/layerSwitcher/UI
 * @see gcapi.layerSwitcher.UI
 * @author Шушанский К.П.
 */
define("gcapi/layerSwitcher/UI", [
        "dojo/_base/declare",
        "dijit/_WidgetBase",
        "dijit/form/DropDownButton",
        "dijit/DropDownMenu",
        "dijit/MenuItem"
    ],
    function (declare, _WidgetBase, DropDownButton, DropDownMenu, MenuItem) {
        /**
         * Класс HTML контрола переключения базовых слоёв карты
         *
         * Может работать в связке с инстансом [gcapi.layerSwitcher.Tool]{@link gcapi.layerSwitcher.Tool}
         * @class gcapi.layerSwitcher.UI
         * @alias gcapi.layerSwitcher.UI
         * @memberof! <global>
         */
        return declare("gcapi.layerSwitcher.UI", [_WidgetBase], {
            baseClass: 'gcApi_LayerSwitcherControl',

            button: undefined,

            constructor: function(){
                
                var me = this;
                
                var menu = new DropDownMenu({
                    baseClass: 'gcApi_LayerSwitcherMenu'
                });
                
                var menuItem1 = new MenuItem({
                    baseClass: 'gcApi_LayerSwitcherItem',
                    label: "Спутник"
                });
                menu.addChild(menuItem1);

                var menuItem2 = new MenuItem({
                    baseClass: 'gcApi_LayerSwitcherItem',
                    label: "Гибрид"
                });
                menu.addChild(menuItem2);
                
                var menuItem3 = new MenuItem({
                    baseClass: 'gcApi_LayerSwitcherItem',
                    label: "Карта"
                });
                menu.addChild(menuItem3);

                me.button = new DropDownButton({
                    label: "Карта",
                    dropDown: menu,
                    baseClass: "gcApi_LayerSwitcherButtonClass",
                    iconClass: "gcApi_LayerSwitcherIcon"
                });

            },
            
            postCreate: function(){
                var me = this;
                
                me.button.placeAt(me.domNode);
            }
            
        });
    }
);
