/**
 * Модуль содержащий класс управления переключения базовых слоёв карты
 *  
 * @module gcapi/layerSwitcher/Tool
 * @see gcapi.layerSwitcher.Tool
 * @author Шушанский К.П.
 */
define("gcapi/layerSwitcher/Tool", [
        "dojo/_base/declare"
    ], function (declare) {

        /**
         * Класс управления переключения базовых слоёв карты
         * Может работать в связке с графическим элементом [gcapi.layerSwitcher.UI]{@link gcapi.layerSwitcher.UI}
         *  
         * @class gcapi.layerSwitcher.Tool
         * @alias gcapi.layerSwitcher.Tool
         * @memberof! <global>
         */
        return declare("gcapi.layerSwitcher.Tool", [], {

            /**
             * Cсылка на инстанс esri.Map
             * @member {esri.Map} _mapInstance
             * @default undefined
             * @memberof gcapi.layerSwitcher.Tool
             * @instance
             * @private
             */
            _mapInstance: undefined,

            /**
             * Будет вызвана автоматически при добавлении модуля на карту
             * @memberof gcapi.layerSwitcher.Tool
             * @function _bind
             * @param {gcapi.Map} map Инстанс gcapi.Map
             * @instance
             * @private
             */
            _bind: function (map) {
                var me = this;
                me._mapInstance = map;
            }
            
        });
    }
);