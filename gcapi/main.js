/**
 * Модуль содержащий неймспейс gcapi
 * @module gcapi/main
 * @namespace gcapi
 * @author Шушанский К.П.
 */
define('gcapi/main', [
    'dojo', 'dijit', 'dojox', 'esri'
], function () {

    dojo.provide('gcapi');
    
    gcapi.version = {
        major: '2',
        minor: '0',
        patch: '0',
        tag: 'prototype'
    };
    
    require([
        'gcapi/Config',
        'gcapi/Map',
        "gcapi/Jams",
        "gcapi/Zoom",
        "gcapi/LayerSwitcher",
        "gcapi/MeasureLine",
        "gcapi/MeasureArea",
        "gcapi/Search",
        "gcapi/Panorams",
        "gcapi/TraceRoute",
        "gcapi/PublicTransport",
        "gcapi/Catalog",
        "gcapi/LanguageSwitcher"
    ]);

    /**
     * Регестрирует функцию, которая будет вызвана, когда будет готова DOM структура документа и
     * будут завершены все вызовы функции require.
     * Cсылка на функцию dojo.ready
     *
     * @example <caption>Пример использования фунции gcapi.ready</caption>
     * gcapi.ready(function () {
     *     console.log('gcapi is ready') //выведет в консоль 'gcapi is ready'
     * }
     * @param {Function} callback Функция обратного вызова
     * @function ready
     * @memberof gcapi
     * @static
     */
    gcapi.ready = dojo.ready;

});
