/**
 * Модуль содержащий класс карты
 * @module gcapi/Map
 * @see gcapi.Map
 * @author Шушанский К.П.
 */
define("gcapi/Map", [
    "dojo/_base/declare",
    "dojo/_base/xhr",
    "dojo/Evented",
    "dojo/_base/lang",
    "dijit/layout/_LayoutWidget",
    "dojo/i18n",
    "dojo/i18n!./nls/map",
    "dojo/ready",
    "esri/map",
    "esri/dijit/Attribution"
], function (declare, xhr, Evented, lang, LayoutWidget, i18n) {

    /**
     * Класс карты
     * @class gcapi.Map
     * @alias gcapi.Map
     * @memberof! <global>
     * @property {String} container id div'а в который будет встроена карта
     * @property {Object} params параметры инициализации карты
     * @property {Number} params.latitude широта центра карты при старте
     * @property {Number} params.longitude долгота центра карты при старте
     * @property {Number} params.zoom начальный уровень зума карты
     * @emits gcapi.Map#mapReady
     *
     * @example <caption>Пример работы с классом gcapi.Map</caption>
     * gcapi.ready(function () { //регестрируем callback функцию
     *      //создание инстанса карты с задаными координатами центра и начальным уровнем зума
     *      var map = new gcapi.Map({latitude: 37.616837652505, longitude: 55.748880176022, zoom: 2}, 'map');
     *      map.on('mapReady', function () { // вещаем обработчик на событие 'mapReady'
     *          map.showDefaultButton(); //включаем дефолтные кнопки
     *      });
     * });
     */
    return declare("gcapi.Map", [LayoutWidget, Evented], {

        /**
         * Id div'а с картой
         * @member {gcapi.Map} container
         * @default undefined
         * @instance
         */
        container: undefined,

        /**
         * Cсылка на инстанс esri.Map
         * @member {esri.Map} map
         * @default undefined
         * @memberof gcapi.Map
         * @instance
         */
        map: undefined,

        _oldBaseLayer: undefined,

        /**
         * Текущий набор базовых слоёв карты
         * @member {String} baseLayer
         * @default undefined
         * @memberof gcapi.Map
         * @instance
         */
        baseLayer: undefined,

        _setBaseLayerAttr: function (baseLayer) {
            var me = this;

            me._set('baseLayer', baseLayer);

            /**
             * Событие пробрасывается при набора базовых слоёв карты
             * @event gcapi.Map#baseLayerChange
             * @type {gcapi.Map}
             * @property {baseLayer} название набора базовых слоёв карты
             */
            me.emit('baseLayerChange', baseLayer);

        },

        _oldLang: undefined,
        /**
         * Текущий язык инстанса gcapi
         * @member {String} lang
         * @default undefined
         * @memberof gcapi.Map
         * @instance
         */
        lang: undefined,

        _setLangAttr: function (language) {
            var me = this;

            me._set('lang', language);

            /**
             * Событие пробрасывается при смене языка инстанса gcapi
             * @event gcapi.Map#langChange
             * @type {gcapi.Map}
             * @property {lang} новый язык карты
             */
            me.emit('langChange', language);
            
            me._oldLang = me.lang;
        },

        /**
         * Ссылка на конфиг карты
         * @member {String} _config
         * @default undefined
         * @memberof gcapi.Map
         * @instance
         * @private
         */
        _config: undefined,

        _loaded: false,

        constructor: function (params, container) {
            var me = this;

            me.on('langChange', me.changeLang);

            //какую подложку и какаую аннотацию грузить по-умолчанию
            var configDefault = {
                lang: (( params.language ) ? params.language : 'ru'),
                baseLayer: (( params.baseLayer ) ? params.baseLayer : 'scheme')
            };

            var locale = i18n.getLocalization("gcapi", "map", configDefault.lang);

            //конфиг карты
            me._config = gcapi.Config.map;

            if (dojo.query('#' + container).length == 0) {
                console.error('Div for init map not exist');
                return false;
            }

            me.params = params;

            me.container = container;

            var width = dojo.query('#' + me.container).style("width")[0];
            var height = dojo.query('#' + me.container).style("height")[0];

            var getLods = new dojo.Deferred();

            xhr.get({
                url: me._config.base[locale[configDefault.baseLayer].base.url],
                content: {
                    f: 'pjson'
                },
                handleAs: 'json',
                load: function (result) {
                    getLods.resolve(result.tileInfo.lods);
                }
            });

            getLods.then(function (lods) {
                var spatialReference = new esri.SpatialReference({
                    wkid: 102100
                });

                var mapCenter = esri.geometry.geographicToWebMercator(
                    new esri.geometry.Point(me.params.latitude, me.params.longitude)
                );

                var resolution = lods[me.params.zoom].resolution;

                var startExtent = new esri.geometry.Extent(
                    mapCenter.x - width / 2 * resolution,
                    mapCenter.y - height / 2 * resolution,
                    mapCenter.x + width / 2 * resolution,
                    mapCenter.y + height / 2 * resolution,
                    spatialReference
                );

                me.map = new esri.Map(container, {
                    logo: false,
                    nav: false,
                    slider: false,
                    smartNavigation: false,
                    extent: startExtent
                });

                //сохраним полученные lods
                //они нам понядобятся в классе изменения зума
                me.map.lods = lods;

                //текущая локализация
                var locale = i18n.getLocalization("gcapi", "map", configDefault.lang);

                //основа
                var layer = new esri.layers.ArcGISTiledMapServiceLayer(me._config.base[locale[configDefault.baseLayer].base.url], {
                    id: locale[configDefault.baseLayer].base.id
                });
                me.map.addLayer(layer);

                //аннотация
                if (locale[configDefault.baseLayer].annotation !== null) {

                    layer = new esri.layers.ArcGISTiledMapServiceLayer(me._config[configDefault.lang][locale[configDefault.baseLayer].annotation.url], {
                        id: locale[configDefault.baseLayer].annotation.id
                    });

                    me.map.addLayer(layer);
                }

                /**
                 * Событие пробрасывается после инициалиазации объекта gcapi.Map
                 * @event gcapi.Map#mapReady
                 * @type {gcapi.Map}
                 * @property {gcapi.Map} gcapi.Map объект карты
                 */
                me.emit('mapReady', me);

                me._oldLang = configDefault.lang;
                
                me.set('lang', configDefault.lang);

                me.baseLayer = configDefault.baseLayer;

                me._loaded = true;
                
            });

        },

        changeLang: function (lang) {
            var me = this;

            if(me._loaded) {
                //если локализация не первая, то
                //скроем стырые слои

                //старая локализация

                var oldLocale = i18n.getLocalization("gcapi", "map", me._oldLang);

                //текущая локализация
                var locale = i18n.getLocalization("gcapi", "map", lang);

                //старая подложка
                var base = me.map.getLayer(oldLocale[me.baseLayer].base.id);
                //спрячим её
                base.hide();

                //старая аннотация
                var annotation = me.map.getLayer(oldLocale[me.baseLayer].annotation.id);
                //и её тоже спрячим
                annotation.hide();

                //теперь нужно включить новую локализацию
                //подложка, сначала проверим есть ли такой слой
                if (me.map.layerIds.indexOf(locale[me.baseLayer].base.id) == -1) {

                    //если такого слоя нет, то создаим его
                    layer = new esri.layers.ArcGISTiledMapServiceLayer(me._config.base[locale[me.baseLayer].base.url], {
                        id: locale[me.baseLayer].base.id
                    });

                    //и добавиим на карту
                    me.map.addLayer(layer);
                } else {
                    //если такой слой есть - включим его
                    layer = me.map.getLayer(locale[me.baseLayer].base.id);
                    layer.show();
                }

                //аннотация,
                //сначала проверим, есть ли аннотация для этого базового слоя
                if (locale[me.baseLayer].annotation) {
                    //если аннотация для этого базового слоя предусмотрена, то
                    //проверим есть ли такой слой
                    if (me.map.layerIds.indexOf(locale[me.baseLayer].annotation.id) == -1) {

                        //если такого слоя нет, то создаим его
                        layer = new esri.layers.ArcGISTiledMapServiceLayer(me._config[lang][locale[me.baseLayer].annotation.url], {
                            id: locale[me.baseLayer].annotation.id
                        });
                        //и добавим на карту
                        me.map.addLayer(layer);

                    } else {
                        //если такой слой есть - включим его
                        layer = me.map.getLayer(locale[me.baseLayer].annotation.id);
                        layer.show();
                    }
                }
            }
        },

        /**
         * Расширяет функционал класса дополнительными модулями
         * @function add
         * @param {Object} module Модуль api
         * @returns {gcapi.Map}
         * @memberof gcapi.Map
         * @instance
         */
        add: function (module) {
            var me = this;

            var name = module.declaredClass.replace('gcapi.', '');

            //console.log(me.addChild);

            lang.setObject(name, module, me);

            if (module instanceof dijit._WidgetBase) {
                me.addChild(module);
            }

            if (typeof(module._bind) === "function") {
                module._bind(me.map);
            }

            return me;
        },

        /**
         * Расширяет функционал карты модулями [gcapi.Jams]{@link gcapi.Jams}, [gcapi.Zoom]{@link gcapi.Zoom}
         * @function showDefaultButton
         * @returns {gcapi.Map}
         * @memberof gcapi.Map
         * @instance
         */
        showDefaultButton: function () {
            var me = this;

            if (me.map !== undefined) {

                new gcapi.LayerSwitcher(me);

                new gcapi.LanguageSwitcher(me);

                new gcapi.Zoom(me);

                new gcapi.Jams(me);
            }
            return me;
        }
    });
});