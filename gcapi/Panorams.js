/**
 * Модуль содержащий класс связывающий кнопку включения/выключения панорам с её контроллером
 *
 * @module gcapi/Panorams
 * @see gcapi.Panorams
 * @author Шушанский К.П.
 */
define("gcapi/Panorams", [
        "dojo/_base/declare",
        "gcapi/panorams/Tool",
        "gcapi/panorams/UI"
    ],
    function (declare, toolPanorams, uiPanorams) {

        /**
         * Класс связывающий кнопку включения/выключения панорам с её контроллером
         *
         * @class gcapi.Panorams
         * @alias gcapi.Panorams
         * @param {gcapi.Map} map Инстанс gcapi.Map
         * @memberof! <global>
         */
        return declare("gcapi.Panorams", [], {

            constructor: function(instance){
                var ui = new uiPanorams();
                var tool = new toolPanorams();
            }

        });
    }
);