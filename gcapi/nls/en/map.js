define(
    {
        scheme: {
            base: { 
                url: 'scheme',
                id: 'scheme' 
            },
            annotation: { 
                url: 'annotation',
                id: 'annotation_en' 
            }
        },
        satellite: {
            base: { 
                url: 'satellite',
                id: 'satellite' 
            },
            annotation: null
        },
        hybrid: {
            base: { 
                url: 'satellite',
                id: 'satellite' 
            },
            annotation: { 
                url: 'hybrid',
                id: 'hybrid_en' 
            }
        }
    }
);
