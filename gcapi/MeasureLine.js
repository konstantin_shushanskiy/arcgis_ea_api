/**
 * Модуль содержащий класс связывающий кнопку включения/выключения инструмента измерения расстояния на карте с её контроллером
 *
 * @module gcapi/MeasureLine
 * @see gcapi.MeasureLine
 * @author Шушанский К.П.
 */
define("gcapi/MeasureLine", [
        "dojo/_base/declare",
        "gcapi/measureLine/Tool",
        "gcapi/measureLine/UI"
    ],
    function (declare, toolMeasureLine, uiMeasureLine) {

        /**
         * Класс связывающий кнопку включения/выключения инструмента измерения расстояния на карте с её контроллером
         * @class gcapi.MeasureLine
         * @alias gcapi.MeasureLine
         * @param {gcapi.Map} map Инстанс gcapi.Map
         * @memberof! <global>
         */
        return declare("gcapi.MeasureLine", [], {

            constructor: function(instance){
                var ui = new uiMeasureLine();
                var tool = new toolMeasureLine();
            }

        });
    }
);