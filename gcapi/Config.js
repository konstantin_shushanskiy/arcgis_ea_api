/**
 * Created by legodemon on 14.07.15.
 */
define("gcapi/Config", [], function () {
    gcapi.Config = {
        map: {
            base: {
                scheme: "http://apiatlas.mos.ru/arcgis/rest/services/Basemaps/egko_gc_graphics/MapServer",
                satellite: "http://apiatlas.mos.ru/arcgis/rest/services/Basemaps/egip_sat/MapServer"
            },
            ru: {
                annotation: "http://apiatlas.mos.ru/arcgis/rest/services/Basemaps/egko_gc_anno_rus/MapServer",
                hybrid: "http://apiatlas.mos.ru/arcgis/rest/services/Basemaps/egko_gc_hyb_rus/MapServer"
            },
            en: {
                annotation: "http://apiatlas.mos.ru/arcgis/rest/services/Basemaps/egko_gc_anno_rus_eng/MapServer",
                hybrid: "http://apiatlas.mos.ru/arcgis/rest/services/Basemaps/egko_gc_hyb_rus_eng/MapServer"
            }

        },
        jams: {
            id: "roadJams",
            server: "http://gis03.rumap.ru/",
            url: "CODD_Jams/${level}/${col}/${row}.png",
            updateInterval: 5
        }
    }
});