/**
 * Модуль содержащий класс связывающий HTML контрол каталога с его контроллером
 *
 * @module gcapi/Catalog
 * @see gcapi.Catalog
 * @author Шушанский К.П.
 */
define("gcapi/Catalog", [
        "dojo/_base/declare",
        "gcapi/catalog/Tool",
        "gcapi/catalog/UI"
    ],
    function (declare, toolCatalog, uiCatalog) {

        /**
         * Класс связывающий HTML контрол каталога с его контроллером
         * @class gcapi.Catalog
         * @alias gcapi.Catalog
         * @param {gcapi.Map} map Инстанс gcapi.Map
         * @memberof! <global>
         */
        return declare("gcapi.Catalog", [], {

            constructor: function(instance){
                var ui = new uiCatalog();
                var tool = new toolCatalog();
            }

        });
    }
);