/**
 * Модуль содержащий класс связывающий кнопку переключения языков и её контроллер
 * @module gcapi/LanguageSwitcher
 * @see gcapi.LanguageSwitcher
 * @author Шушанский К.П.
 */
define("gcapi/LanguageSwitcher", [
        "dojo/_base/declare",
        "gcapi/languageSwitcher/UI",
        "gcapi/languageSwitcher/Tool",
        "dojo/i18n",
        "dojo/i18n!./nls/map"
    ],
    function (declare, ui, tool, i18n, locale) {

        /**
         * Класс связывающий связывающий кнопку переключения языков и её контроллер
         * @class gcapi.LanguageSwitcher
         * @alias gcapi.LanguageSwitcher
         * @param {gcapi.Map} map Инстанс gcapi.Map
         * @memberof! <global>
         */
        return declare("gcapi.LanguageSwitcher", [], {

            indexCurrentLocale: 0,

            constructor: function (instance) {
                var me = this;

                var button = new ui();
                
                instance.add(button);

                var locale = djConfig.extraLocale.filter(function (item, pos) {
                    return djConfig.extraLocale.indexOf(item) == pos;
                });
                
                button.on('click', function () {


                    me.indexCurrentLocale = locale.indexOf(instance.lang);
                    
                    
                    if (me.indexCurrentLocale < locale.length-1) {
                        me.indexCurrentLocale++;
                    } else {
                        me.indexCurrentLocale = 0;
                    }

                    instance.set('lang', locale[me.indexCurrentLocale]);


                });

                instance.on('langChange', function(lang){
                    var me = this;

                    //поросим кнопку изменить язык
                    button.changeLang(lang);
                    
                });

            }

        });
    }
);