/**
 * Модуль содержащий класс управлением уровнем зума карты
 *
 * @module gcapi/zoom/Tool
 * @see gcapi.zoom.Tool
 * @author Шушанский К.П.
 */
define("gcapi/zoom/Tool", [
        "dojo/_base/declare"
    ], function (declare) {

        /**
         * Класс содержащий функции увеличения/уменьшения зума карты
         * Может работать в связке с графическим элементом [gcapi.zoom.UI]{@link gcapi.zoom.UI}
         * @class gcapi.zoom.Tool
         * @alias gcapi.zoom.Tool
         * @memberof! <global>
         */
        return declare("gcapi.zoom.Tool", [], {

            /**
             * Cсылка на инстанс esri.Map
             * @member {esri.Map} _mapInstance
             * @default undefined
             * @memberof gcapi.zoom.Tool
             * @instance
             * @private
             */
            _mapInstance: undefined,

            /**
             * Будет вызвана автоматически при добавлении модуля на карту
             * @memberof gcapi.zoom.Tool
             * @function _bind
             * @param {gcapi.Map} map Инстанс gcapi.Map
             * @instance
             * @private
             */
            _bind: function (map) {
                var me = this;
                me._mapInstance = map;
            },

            /**
             * Увеличивает зум карты на еденицу
             * @function zoomIn
             * @memberof gcapi.zoom.Tool
             * @instance
             */
            zoomIn: function () {
                var me = this;

                var level = me._mapInstance.getLevel();

                if (level < me._mapInstance.lods.length - 1) {
                    me._mapInstance.setLevel(++level);
                }
            },

            /**
             * Уменьшает зум карты на еденицу
             * @function zoomOut
             * @memberof gcapi.zoom.Tool
             * @instance
             */
            zoomOut: function () {
                var me = this;

                var level = me._mapInstance.getLevel();

                if (level > 0) {
                    me._mapInstance.setLevel(--level);
                }
            }
        });
    }
);