/**
 * Модуль содержащий класс кнопок увеличения/уменьшения зума карты
 * @module gcapi/zoom/UI
 * @see gcapi.zoom.UI
 * @author Шушанский К.П.
 */
define("gcapi/zoom/UI", [
        "dojo/_base/declare",
        "dijit/_WidgetBase",
        "dijit/_TemplatedMixin",
        "dojo/text!./template/Zoom.html",
        "dojo/on",
        "dojo/Evented"
    ],
    function (declare, WidgetBase, TemplatedMixin, template, on, Evented) {
        /**
         * Класс кнопок увеличения/уменьшения зума карты.
         * Создаёт графический элемент управления.
         * Основан на dijit._WidgetBase, dijit._TemplatedMixin
         * Может работать в связке с инстансом [gcapi.zoom.Tool]{@link gcapi.zoom.Tool}
         * @class gcapi.zoom.UI
         * @alias gcapi.zoom.UI
         * @memberof! <global>
         */
        return declare("gcapi.zoom.UI", [WidgetBase, TemplatedMixin, Evented], {

            templateString: template,
            

            postCreate: function(){
                var me = this;
                
                /**
                 * Событие пробрасывается при клике на кнопку увеличения зума карты
                 * @event gcapi.ui.Zoom#zoomInClick
                 */
                on(me.zoomInButton, 'click', function(){
                    me.emit('zoomInClick');
                });
                
                /**
                 * Событие пробрасывается при клике на кнопку уменьшения зума карты
                 * @event gcapi.ui.Zoom#zoomOutClick
                 */
                on(me.zoomOutButton, 'click', function(){
                    me.emit('zoomOutClick');
                });
            }
        });
    }
);
