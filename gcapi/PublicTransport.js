/**
 * Модуль содержащий класс связывающий HTML контрол сервиса общественного транспорта с его контроллером
 *
 * @module gcapi/PublicTransport
 * @see gcapi.PublicTransport
 * @author Шушанский К.П.
 */
define("gcapi/PublicTransport", [
        "dojo/_base/declare",
        "gcapi/publicTransport/Tool",
        "gcapi/publicTransport/UI"
    ],
    function (declare, toolPublicTransport, uiPublicTransport) {

        /**
         * Класс связывающий HTML контрол сервиса общественного транспорта с его контроллером
         *
         * @class gcapi.PublicTransport
         * @alias gcapi.PublicTransport
         * @param {gcapi.Map} map Инстанс gcapi.Map
         * @memberof! <global>
         */
        return declare("gcapi.PublicTransport", [], {

            constructor: function(instance){
                var ui = new uiPublicTransport();
                var tool = new toolPublicTransport();
            }

        });
    }
);