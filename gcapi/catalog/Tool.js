/**
 * Модуль содержащий класс контроллера каталога
 *
 * @module gcapi/catalog/Tool
 * @see gcapi.catalog.Tool
 * @author Шушанский К.П.
 */
define("gcapi/catalog/Tool", [
        "dojo/_base/declare"
    ], function (declare) {

        /**
         * Класс контроллера каталога
         * Может работать в связке с графическим элементом [gcapi.catalog.UI]{@link gcapi.catalog.UI}
         *
         * @class gcapi.catalog.Tool
         * @alias gcapi.catalog.Tool
         * @memberof! <global>
         */
        return declare("gcapi.catalog.Tool", [], {

            /**
             * Cсылка на инстанс esri.Map
             * @member {esri.Map} _mapInstance
             * @default undefined
             * @memberof gcapi.catalog.Tool
             * @instance
             * @private
             */
            _mapInstance: undefined,

            /**
             * Будет вызвана автоматически при добавлении модуля на карту
             * @memberof gcapi.catalog.Tool
             * @function _bind
             * @param {gcapi.Map} map Инстанс gcapi.Map
             * @instance
             * @private
             */
            _bind: function (map) {
                var me = this;
                me._mapInstance = map;
            }

        });
    }
);