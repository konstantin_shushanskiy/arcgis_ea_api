/**
 * Модуль содержащий класс HTML контрола каталога
 *
 * @module gcapi/catalog/UI
 * @see gcapi.catalog.UI
 * @author Шушанский К.П.
 */
define("gcapi/catalog/UI", [
        "dojo/_base/declare"
    ],
    function (declare) {
        /**
         * Класс HTML контрола каталога
         *
         * Может работать в связке с инстансом [gcapi.catalog.Tool]{@link gcapi.catalog.Tool}
         * @class gcapi.catalog.UI
         * @alias gcapi.catalog.UI
         * @memberof! <global>
         */
        return declare("gcapi.catalog.UI", [], {


        });
    }
);
