/**
 * Модуль содержащий класс связывающий HTML контрол инструмента прокладки маршрута с его контроллером
 *
 * @module gcapi/TraceRoute
 * @see gcapi.TraceRoute
 * @author Шушанский К.П.
 */
define("gcapi/TraceRoute", [
        "dojo/_base/declare",
        "gcapi/traceRoute/Tool",
        "gcapi/traceRoute/UI"
    ],
    function (declare, toolTraceRoute, uiTraceRoute) {

        /**
         * Класс связывающий HTML контрол инструмента прокладки маршрута с его контроллером
         *
         * @class gcapi.TraceRoute
         * @alias gcapi.TraceRoute
         * @param {gcapi.Map} map Инстанс gcapi.Map
         * @memberof! <global>
         */
        return declare("gcapi.TraceRoute", [], {

            constructor: function(instance){
                var ui = new uiTraceRoute();
                var tool = new toolTraceRoute();
            }

        });
    }
);