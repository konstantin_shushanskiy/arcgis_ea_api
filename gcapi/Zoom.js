/**
 * Модуль содержащий класс связывающий кнопки увеличения/уменьшения зума карты с классом содержащим функции увеличения/уменьшения зума карты
 * @module gcapi/Zoom
 * @see gcapi.Zoom
 * @author Шушанский К.П.
 */
define("gcapi/Zoom", [
        "dojo/_base/declare",
        "gcapi/zoom/Tool",
        "gcapi/zoom/UI"
    ],
    function (declare, toolZoom, uiZoom) {
        return declare("gcapi.Zoom", [], {
            
            /**
             * Класс связывающий кнопку увеличения/уменьшения зума карты с классом содержащим функции увеличения/уменьшения зума карты
             * @class gcapi.Zoom
             * @alias gcapi.Zoom
             * @param {gcapi.Map} map Инстанс gcapi.Map
             * @memberof! <global>
             */
            constructor: function(instance){
                var zoomButtons = new uiZoom();
                
                var zoomTools = new toolZoom();

                instance.add(zoomButtons);

                instance.add(zoomTools);

                zoomButtons.on('zoomInClick', function () {
                    zoomTools.zoomIn();
                });

                zoomButtons.on('zoomOutClick', function () {
                    zoomTools.zoomOut();
                });
            }

        });
    }
);