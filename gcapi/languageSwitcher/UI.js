define("gcapi/languageSwitcher/UI", [
        "dojo/_base/declare",
        "dijit/form/Button",
        "dijit/Tooltip",
        "dojo/dom-style",
        "dojo/i18n",
        "dojo/i18n!../nls/languageSwitcher"
    ],
    function (declare, Button, Tooltip, domStyle, i18n) {


        return declare("gcapi.languageSwitcher.UI", [Button], {

            baseClass: 'gcApi_LanguageSwitcherControl',

            class: 'gcApi_CommonButton',
            
            postCreate: function(){
                var me = this;

                me.tooltipWidget = new Tooltip({
                    connectId: me.domNode
                });
            },

            changeLang: function(lang){
                var me = this;
                
                var locale = i18n.getLocalization("gcapi", "languageSwitcher", lang);

                me.tooltipWidget.close();
                
                me.tooltipWidget.label = locale.tooltip;

                me.set('label', locale.label);
            }

        });
    }
);