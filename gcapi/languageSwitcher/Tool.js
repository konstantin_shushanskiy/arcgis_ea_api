define("gcapi/languageSwitcher/Tool", [
        "dojo/_base/declare"
    ], function (declare) {

        return declare("gcapi.languageSwitcher.Tool", [], {

            _mapInstance: undefined,

            _bind: function (map) {
                var me = this;
                me._mapInstance = map;
            }

        });
    }
);