/**
 * Модуль содержащий класс связывающий контрол переключения базовых слоёв карты с
 * модулем переключения базовых слоёв карты
 *
 * @module gcapi/LayerSwitcher
 * @see gcapi.LayerSwitcher
 * @author Шушанский К.П.
 */
define("gcapi/LayerSwitcher", [
        "dojo/_base/declare",
        "gcapi/layerSwitcher/Tool",
        "gcapi/layerSwitcher/UI"
    ],
    function (declare, Tool, Ui) {

        /**
         * Класс связывающий связывающий контрол переключения базовых слоёв карты с
         * модулем переключения базовых слоёв карты
         *
         * @class gcapi.LayerSwitcher
         * @alias gcapi.LayerSwitcher
         * @param {gcapi.Map} map Инстанс gcapi.Map
         * @memberof! <global>
         */
        return declare("gcapi.LayerSwitcher", [], {

            constructor: function (instance) {
                var ui = new Ui();
                var tool = new Tool();
                
                instance.add(ui);
                
            }

        });
    }
);