/**
 * Модуль содержащий класс HTML контрола инструмента прокладки маршрута
 *
 * @module gcapi/traceRoute/UI
 * @see gcapi.traceRoute.UI
 * @author Шушанский К.П.
 */
define("gcapi/traceRoute/UI", [
        "dojo/_base/declare"
    ],
    function (declare) {
        /**
         * Класс HTML контрола инструмента прокладки маршрута
         *
         * Может работать в связке с инстансом [gcapi.traceRoute.Tool]{@link gcapi.traceRoute.Tool}
         * @class gcapi.traceRoute.UI
         * @alias gcapi.traceRoute.UI
         * @memberof! <global>
         */
        return declare("gcapi.traceRoute.UI", [], {


        });
    }
);
