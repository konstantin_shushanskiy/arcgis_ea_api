/**
 * Модуль содержащий класс инструмента прокладки маршрутов
 *
 * @module gcapi/traceRoute/Tool
 * @see gcapi.traceRoute.Tool
 * @author Шушанский К.П.
 */
define("gcapi/traceRoute/Tool", [
        "dojo/_base/declare"
    ], function (declare) {

        /**
         * Класс инструмента прокладки маршрутов
         * Может работать в связке с графическим элементом [gcapi.traceRoute.UI]{@link gcapi.traceRoute.UI}
         *
         * @class gcapi.traceRoute.Tool
         * @alias gcapi.traceRoute.Tool
         * @memberof! <global>
         */
        return declare("gcapi.traceRoute.Tool", [], {

            /**
             * Cсылка на инстанс esri.Map
             * @member {esri.Map} _mapInstance
             * @default undefined
             * @memberof gcapi.traceRoute.Tool
             * @instance
             * @private
             */
            _mapInstance: undefined,

            /**
             * Будет вызвана автоматически при добавлении модуля на карту
             * @memberof gcapi.traceRoute.Tool
             * @function _bind
             * @param {gcapi.Map} map Инстанс gcapi.Map
             * @instance
             * @private
             */
            _bind: function (map) {
                var me = this;
                me._mapInstance = map;
            }

        });
    }
);