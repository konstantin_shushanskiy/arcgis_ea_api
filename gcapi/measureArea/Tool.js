/**
 * Модуль содержащий класс измерения площади на карте
 *
 * @module gcapi/measureArea/Tool
 * @see gcapi.measureArea.Tool
 * @author Шушанский К.П.
 */
define("gcapi/measureArea/Tool", [
        "dojo/_base/declare"
    ], function (declare) {

        /**
         * Класс инструмента измерения расстояния на карте
         * Может работать в связке с графическим элементом [gcapi.measureArea.UI]{@link gcapi.measureArea.UI}
         *
         * @class gcapi.measureArea.Tool
         * @alias gcapi.measureArea.Tool
         * @memberof! <global>
         */
        return declare("gcapi.measureArea.Tool", [], {

            /**
             * Cсылка на инстанс esri.Map
             * @member {esri.Map} _mapInstance
             * @default undefined
             * @memberof gcapi.measureArea.Tool
             * @instance
             * @private
             */
            _mapInstance: undefined,

            /**
             * Будет вызвана автоматически при добавлении модуля на карту
             * @memberof gcapi.measureArea.Tool
             * @function _bind
             * @param {gcapi.Map} map Инстанс gcapi.Map
             * @instance
             * @private
             */
            _bind: function (map) {
                var me = this;
                me._mapInstance = map;
            }

        });
    }
);