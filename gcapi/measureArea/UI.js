/**
 * Модуль содержащий класс HTML кнопки включения/выключения инструмента измерения площади на карте
 *
 * @module gcapi/measureArea/UI
 * @see gcapi.measureArea.UI
 * @author Шушанский К.П.
 */
define("gcapi/measureArea/UI", [
        "dojo/_base/declare"
    ],
    function (declare) {
        /**
         * Класс кнопки включения/выключения инструмента измерения площади на карте
         *
         * Может работать в связке с инстансом [gcapi.measureArea.Tool]{@link gcapi.measureArea.Tool}
         * @class gcapi.measureArea.UI
         * @alias gcapi.measureArea.UI
         * @memberof! <global>
         */
        return declare("gcapi.measureArea.UI", [], {


        });
    }
);
