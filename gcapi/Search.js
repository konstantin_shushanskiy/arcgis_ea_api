/**
 * Модуль содержащий класс связывающий HTML контрол инструмента поиска с его контроллером
 *
 * @module gcapi/Search
 * @see gcapi.Search
 * @author Шушанский К.П.
 */
define("gcapi/Search", [
        "dojo/_base/declare",
        "gcapi/search/Tool",
        "gcapi/search/UI"
    ],
    function (declare, toolSearch, uiSearch) {

        /**
         * Класс связывающий HTML контрол инструмента поиска с его контроллером
         * @class gcapi.Search
         * @alias gcapi.Search
         * @param {gcapi.Map} map Инстанс gcapi.Map
         * @memberof! <global>
         */
        return declare("gcapi.Search", [], {

            constructor: function(instance){
                var ui = new uiSearch();
                var tool = new toolSearch();
            }

        });
    }
);