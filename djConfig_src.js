djConfig = {
    locale: 'ru',
    extraLocale: ['en'],
    baseUrl: window.location.protocol+'//{host}/{release}',
    parseOnLoad: true,
    isDebug: false,
    has: {
        'dojo-firebug': false,
        'dojo-debug-messages': false,
        'dojo-preload-i18n-Api': true
    },
    async: false,
    tlmSiblingOfDojo: true,
    packages:[
        { name: 'dojo',  location: 'dojo'},
        { name: 'dojox', location: 'dojox'},
        { name: 'dijit', location: 'dijit'},
        { name: 'esri',  location: 'esri'},
        { name: 'gcapi', location: 'gcapi'}
    ]
};