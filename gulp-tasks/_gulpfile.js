//подключение плагинов
var gulp = require('gulp');                         //gulp - сборщик
var concat = require('gulp-concat');                //конкатенация нескольких файлов в один
var runSequence = require('run-sequence').use(gulp);//плагин запускающий таски поочереди
var clean = require('gulp-clean');                  //удаляет директорию/файл
var minimist = require('minimist');                 //упрощает получение аргументов и строки вызова
var watch = require('gulp-watch');                  //наблюдает за изменениями в целевых файлаих и запускает callback
var compass = require('gulp-compass');              //compass транслятор
var spritesmith = require('gulp.spritesmith');      //сборщик спрайта
var jsdoc = require("gulp-jsdoc");                  //сборщик документации
var run = require('gulp-run');                      //запускает shell команды
var uglify = require('gulp-uglifyjs');              //минификатор js
var rename = require("gulp-rename");                //переименовывает фаилы и директории
var zip = require('gulp-zip');                      //архиватор
var minifyCSS = require('gulp-minify-css');         //минификация css

//получение опций запуска из командной строки
var options = minimist(process.argv.slice(2));
var mode = options.mode;        //mode - production или development
var watches = options.watch;    //watch  - если задан, то будут запущены watch процессы
var docs = options.docs;        //docs - если задан, то будет собрана пользовательская документация

//проверка опции mode
if (!mode) {
    mode = 'development';
} else if (mode != 'development' && mode != 'production') {
    console.log('\n\x1b[31mНеверное значение параметра\x1b[36m mode \x1b[0m');
    process.exit();
}

//перечень путей, для удобства объденены в объект
var paths = {
    build: {
        builds: 'builds',
        documentation: paths.build.builds + '/documentation',
        development: paths.build.builds + '/development',
        production: paths.build.builds + '/production',
        out: paths.build.builds + '/out'
    },
    gcapi: {
        js: [
            "gcapi/**"
        ],
        scss: [
            'resources/sass/gcapi.scss'
        ],
        css: [
            'resources/css/**'
        ],
        sprite: {
            icons: 'resources/sass/sprite/*.png',       //где храняться иконки из которых будет собран спрайт
            destImgPath: 'resources/images/',           //куда положить спрайт
            destImgName: 'sprite.png',                  //как спрайт будет называться
            destCssPath: 'resources/sass/mixins/',      //куда положить файл sass с описанием спрайта
            destCssName: 'sprite.scss',                 //как будет называть этот файл
            cssTemplate: 'resources/sass/sprite/style'  //шаблон описания спрайта
        }
    },
    esri: {
        js: [
            'vendor/arcgis_js_v32_api/arcgis_js_api/library/3.2/jsapi/js/esri/**'           //где лежит esri
        ],
        css: [
            'vendor/arcgis_js_v32_api/arcgis_js_api/library/3.2/jsapi/js/esri/css/esri.css' //css esri необходимый для отображения тайлов
        ]
    },
    dojo: {
        dojo: [
            'vendor/dojo-release-1.7.3-src/dojo/**'                 //исходники dojo
        ],
        dijit: [
            'vendor/dojo-release-1.7.3-src/dijit/**'                //исходники dijit
        ],
        dojox: [
            'vendor/dojo-release-1.7.3-src/dojox/**'                //исходники dojox
        ],
        bundle: [
            'tmp_release/**'                                        //куда компилится gcapi
        ],
        css: [
            'vendor/dojo-release-1.7.3-src/dijit/themes/dijit.css' //сss фреймворка dijit нужна для каректного отображения виджетов
        ]
    }
};

//получение информации для билда спрайта
var spriteData = gulp.src(paths.gcapi.sprite.icons).pipe(spritesmith({
    imgName: paths.gcapi.sprite.destImgName,
    cssName: paths.gcapi.sprite.destCssName,
    cssTemplate: paths.gcapi.sprite.cssTemplate
}));

//--- таски для сборки css и sprite.png ---//
gulp.task('pre-build-gcapi-css', function () {
    return gulp.src(paths.gcapi.scss)
        .pipe(compass({
            sass: 'resources/sass',
            css: 'resources/css',
            style: 'expanded',
            comments: true
        }))
});

gulp.task('build-sprite-img', function () {
    return spriteData.img.pipe(gulp.dest(paths.gcapi.sprite.destImgPath));
});

gulp.task('build-sprite-css', function () {
    return spriteData.css.pipe(gulp.dest(paths.gcapi.sprite.destCssPath));
});

gulp.task('copy-gcapi-css', function () {
    var css = paths.esri.css.concat(paths.dojo.css).concat(paths.gcapi.css);
    return gulp.src(css)
        .pipe(concat('gcapi.css'))
        .pipe(gulp.dest('build/development'))
        .pipe(minifyCSS())
        .pipe(gulp.dest('build/production'));
});

gulp.task('copy-gcapi-sprite', function () {
    var spritePath = paths.gcapi.sprite.destImgPath + paths.gcapi.sprite.destImgName;
    return gulp.src(spritePath)
        .pipe(gulp.dest('build/development/images'))
        .pipe(gulp.dest('build/production/images'));
});

gulp.task('copy-gcapi-fonts', function () {
    return gulp.src('resources/fonts/**')
        .pipe(gulp.dest('build/development/fonts'))
        .pipe(gulp.dest('build/production/fonts'));
});
//---

//запускает dojo builder
gulp.task('pre-build-gcapi', function () {
    return run('./vendor/dojo-release-1.7.3-src/util/buildscripts/build.sh profile=./gcapi/gcapi.profile.js -r').exec();
});
//-----------------------------------------------

gulp.task('clean-production', function () {
    return gulp.src(paths.build.production, {read: false})
        .pipe(clean());
});

gulp.task('copy-gcapi-to-production', function () {
    return gulp.src('tmp_release/gcapi/**')
        .pipe(gulp.dest('build/production/gcapi'))
});

gulp.task('copy-dojo-to-production', function () {
    return gulp.src('tmp_release/dojo/**')
        .pipe(gulp.dest('build/production/dojo'))
});

gulp.task('copy-dijit-to-production', function () {
    return gulp.src('tmp_release/dijit/**')
        .pipe(gulp.dest('build/production/dijit'))
});

gulp.task('copy-esri-to-production', function () {
    return gulp.src('tmp_release/esri/**')
        .pipe(gulp.dest('build/production/esri'))
});

gulp.task('copy-dojox-to-production', function () {
    return gulp.src('tmp_release/dojox/**')
        .pipe(gulp.dest('build/production/dojox'))
});

gulp.task('rename-gcapi.js.uncompressed.js', function () {
    return gulp.src('build/production/gcapi/gcapi.js.uncompressed.js')
        .pipe(rename('gcapi.js'))
        .pipe(gulp.dest('build/production/gcapi'))
});

gulp.task('copy-production-to-out', function () {
    return gulp.src('build/production/**')
        .pipe(gulp.dest('build/out'))
});

gulp.task('rename-dojo.js.uncompressed.js', function () {
    return gulp.src('build/production/dojo/dojo.js.uncompressed.js')
        .pipe(rename('dojo.js'))
        .pipe(gulp.dest('build/production/dojo'))
});

gulp.task('clean-tmp_release', function () {
    return gulp.src('tmp_release')
        .pipe(clean())
});

gulp.task('build-copy-app.js-to-production', function () {
    return gulp.src([
        'gcapi.js',
        'build/production/dojo/dojo.js',
        'build/production/dijit/main.js',
        'build/production/dojox/main.js',
        'build/production/gcapi/gcapi.js'
    ]).pipe(concat('gcapi.js'))
        .pipe(gulp.dest('build/production'));
});

gulp.task('copy-web.config', function () {
    return gulp.src('web.config')
        .pipe(gulp.dest('build/out'))
});

gulp.task('production', function () {
    runSequence(
        'clean-production',
        'pre-build-gcapi-css',
        'build-sprite-img',
        'build-sprite-css',
        'copy-gcapi-css',
        'copy-gcapi-sprite',
        'copy-gcapi-fonts',
        'pre-build-gcapi',
        ///
        'copy-gcapi-to-production',
        'copy-dojo-to-production',
        'copy-dijit-to-production',
        'copy-esri-to-production',
        'copy-dojox-to-production',
        'rename-gcapi.js.uncompressed.js',
        'rename-dojo.js.uncompressed.js',
        'build-copy-app.js-to-production',
        'copy-production-to-out',
        'clean-tmp_release',
        'copy-web.config'
    )
});
//------------------------------------------------


//таск по-умолчанию
//собирает всё ;) development, production, documentation и укладывает всё в архив
//аргумент mode - влияет на то, какая версия (development или production) окажется в директории out
//веб-сервер (iis 7) смотрит на директорию out
//пример: gulp --mode production
//пример: gulp --mode development
gulp.task('default', function () {
    runSequence(
        'clean-build',
        //
        'copy-builder.php',
        //
        'dev-copy-dojo',
        'dev-copy-dijit',
        'dev-copy-dojox',
        'dev-copy-esri',
        'dev-copy-gcapi',
        //build gcapi.css
        'pre-build-gcapi-css',
        'build-sprite-img',
        'build-sprite-css',
        'copy-gcapi-css',
        'copy-gcapi-sprite',
        'copy-gcapi-fonts',
        //build gcapi/gcapi.js
        'pre-build-gcapi',
        'post-build-gcapi',
        'clean-tmp_release',
        //
        'prod-copy-dojo',
        'prod-copy-esri',
        //
        'jsdoc-build',
        'esri-doc-copy',
        'jsdoc-copy',
        //
        'copy-output',
        'distrib'
    )
});

//-- таск для работы над исходным кодом
//собирает development версию api, кладёт её в директорию out,
//запускает два watch процесса: наблюдение за папкой gcapi и resources/sass
gulp.task('dev-source', function () {
    runSequence(
        'clean-build',
        'copy-builder.php',
        'dev-copy-dojo',
        'dev-copy-dijit',
        'dev-copy-dojox',
        'dev-copy-esri',
        'dev-copy-gcapi',
        'pre-build-gcapi-css',
        'build-sprite-img',
        'build-sprite-css',
        'copy-gcapi-css',
        'copy-gcapi-sprite',
        'copy-gcapi-fonts',
        function () {
            gulp.src('build/development/**').pipe(gulp.dest('build/out'));

            watch(paths.gcapi.js, {name: 'gcapi js changed...'}, function () {
                console.log('Copy gcapi directory');
                runSequence('dev-copy-gcapi', function () {
                    gulp.src('build/development/**').pipe(gulp.dest('build/out'));
                });
            });

            watch('gcapi/builder.php', {name: 'builder.php copy'}, function () {
                runSequence('copy-builder.php', function () {
                    gulp.src('build/development/builder.php').pipe(gulp.dest('build/out'));
                });
            });

            watch('resources/sass/**', {name: 'gcapi css changed...'}, function () {
                runSequence(
                    'pre-build-gcapi-css',
                    'build-sprite-img',
                    'build-sprite-css',
                    'copy-gcapi-css',
                    'copy-gcapi-sprite',
                    function () {
                        gulp.src('build/development/**').pipe(gulp.dest('build/out'));
                    }
                )
            })
        }
    );
});

//-- таск для работы над документированием кода
gulp.task('dev-doc', function () {

});

gulp.task('clean-build', function () {
    return gulp.src('build', {read: false})
        .pipe(clean());
});

gulp.task('copy-builder.php', function () {
    return gulp.src('builder.php')
        .pipe(gulp.dest('build/development'))
        .pipe(gulp.dest('build/production'))
});

//---
gulp.task('dev-copy-dojo', function () {
    return gulp.src(paths.dojo.dojo).pipe(gulp.dest('build/development/vendor/dojo'));
});

gulp.task('dev-copy-dijit', function () {
    return gulp.src(paths.dojo.dijit).pipe(gulp.dest('build/development/vendor/dijit'));
});

gulp.task('dev-copy-dojox', function () {
    return gulp.src(paths.dojo.dojox).pipe(gulp.dest('build/development/vendor/dojox'));
});

gulp.task('dev-copy-esri', function () {
    return gulp.src(paths.esri.js)
        .pipe(gulp.dest('build/development/vendor/esri'));
});

gulp.task('dev-copy-gcapi', function () {
    return gulp.src(paths.gcapi.js)
        .pipe(gulp.dest('build/development/gcapi'));
});
//---

//--- таски для сборки gcapi


gulp.task('post-build-gcapi', function () {
    return gulp.src('tmp_release/gcapi/gcapi.js.uncompressed.js')
        .pipe(uglify({mangle: false}))
        .pipe(rename("gcapi.js"))
        .pipe(gulp.dest('build/production/gcapi'));
});

gulp.task('clean-tmp_release', function () {
    return gulp.src('tmp_release', {read: false})
        .pipe(clean());
});
//---

gulp.task('prod-copy-dojo', function () {
    return gulp.src(paths.dojo.bundle)
        .pipe(gulp.dest('build/production/vendor'))
});

gulp.task('prod-copy-esri', function () {
    return gulp.src(paths.esri.js)
        .pipe(gulp.dest('build/production/vendor/esri'));
});

gulp.task('copy-output', function () {
    return gulp.src('build/' + mode + '/**')
        .pipe(gulp.dest('build/out'));
});

//-- сборка документации
gulp.task('jsdoc-build', function () {
    return gulp.src([
        'gcapi/*.js',
        'gcapi/**/*.js',
        '!gcapi/zoom/template/Zoom.html',
        'readme.md'
    ])
        .pipe(jsdoc.parser())
        .pipe(jsdoc.generator('build/documentation', {}, {
            plugins: ["plugins/markdown"],
            showPrivate: true,
            monospaceLinks: false,
            cleverLinks: true,
            outputSourceFiles: true
        }));
});

gulp.task('esri-doc-copy', function () {
    return gulp.src("vendor/arcgis_js_v32_sdk/arcgis_js_api/sdk/**").pipe(gulp.dest('build/documentation/sdk'));
});

gulp.task('jsdoc-copy', function () {
    return gulp.src("build/documentation/**").pipe(gulp.dest('build/out/documentation'));
});
//--

//-- таск для сбоки дистрибутива
//TODO научить таск понимать что нужно укладывать в дистрибутив
gulp.task('distrib', function () {

    function getDateTime() {
        var date = new Date();
        var hour = date.getHours();
        hour = (hour < 10 ? "0" : "") + hour;
        var min = date.getMinutes();
        min = (min < 10 ? "0" : "") + min;
        //var sec = date.getSeconds();
        //sec = (sec < 10 ? "0" : "") + sec;
        var year = date.getFullYear();
        var month = date.getMonth() + 1;
        month = (month < 10 ? "0" : "") + month;
        var day = date.getDate();
        day = (day < 10 ? "0" : "") + day;
        return year + "-" + month + "-" + day + "-" + hour + "-" + min;
    }

    var zipName = 'gcapi-' + getDateTime() + '.zip';

    return gulp.src(['build/development/**', 'build/production/**', 'build/documentation/**'], {base: "."})
        .pipe(zip(zipName))
        .pipe(gulp.dest('distribution'));
});
