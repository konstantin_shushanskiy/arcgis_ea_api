module.exports = function(gulp, plugins) {
    return function () {
        return gulp.src('build/documentation', {read: false})
            .pipe(plugins.clean());
    }
}