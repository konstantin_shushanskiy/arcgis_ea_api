//-- сборка документации
module.exports = function(gulp,plugins){
    return function() {
        return gulp.src([
            'gcapi/*.js',
            'gcapi/**/*.js',
            '!gcapi/zoom/template/Zoom.html',
            'readme.md'
        ])
            .pipe(plugins.jsdoc.parser())
            .pipe(plugins.jsdoc.generator('build/documentation', {}, {
                plugins: ["plugins/markdown"],
                showPrivate: true,
                monospaceLinks: false,
                cleverLinks: true,
                outputSourceFiles: true
            }));
    }
};