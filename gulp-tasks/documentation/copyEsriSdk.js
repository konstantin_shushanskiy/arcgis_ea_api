module.exports = function(gulp,plugins) {
    return function () {
        return gulp.src("vendor/arcgis_js_v32_sdk/arcgis_js_api/sdk/**").pipe(gulp.dest('build/documentation/sdk'));
    }
}