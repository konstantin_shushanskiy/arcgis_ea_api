module.exports = function (gulp, plugins) {
    return function () {
        return gulp.src('resources/images/sprite.png')
            .pipe(gulp.dest('build/production/resources/images'))
    }
};