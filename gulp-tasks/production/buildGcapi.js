//запускает dojo builder
module.exports = function (gulp, plugins) {
    return function () {
        return plugins.run('./vendor/dojo-release-1.7.3-src/util/buildscripts/build.sh profile=./gcapi/gcapi.profile.js -r').exec();
    }
};