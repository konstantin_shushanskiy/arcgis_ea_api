module.exports = function (gulp, plugins) {
    return function() {
        return gulp.src([
            'vendor/dojo-release-1.7.3-src/dijit/themes/dijit.css', //сss фреймворка dijit нужна для каректного отображения виджетов
            'vendor/arcgis_js_v32_api/arcgis_js_api/library/3.2/jsapi/js/esri/css/esri.css', //css esri необходимый для отображения тайлов
            'resources/css/gcapi.css'
        ])
            .pipe(plugins.concat('js.css'))
            .pipe(plugins.minifyCss())
            .pipe(gulp.dest('build/production'));
    }
};