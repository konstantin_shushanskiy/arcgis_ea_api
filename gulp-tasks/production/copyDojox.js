module.exports = function (gulp, plugins) {
    return function () {
        return gulp.src('tmp_release/dojox/**')
            .pipe(gulp.dest('build/production/dojox'))
    }
};