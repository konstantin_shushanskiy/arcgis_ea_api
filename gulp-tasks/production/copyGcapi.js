module.exports = function (gulp, plugins) {
    return function () {
        return gulp.src('tmp_release/gcapi/**')
            .pipe(gulp.dest('build/production/gcapi'))
    }
};