module.exports = function (gulp, plugins) {
    return function() {
        return gulp.src([
            'djConfig.js',
            'tmp_release/dojo/dojo.js.uncompressed.js',
            'tmp_release/gcapi/gcapi.js.uncompressed.js'
        ]).pipe(plugins.concat('js.js'))
            .pipe(gulp.dest('build/production'));
    }
};