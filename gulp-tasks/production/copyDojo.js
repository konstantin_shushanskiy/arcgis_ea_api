module.exports = function (gulp, plugins) {
    return function () {
        return gulp.src('tmp_release/dojo/**')
            .pipe(gulp.dest('build/production/dojo'))
    }
};