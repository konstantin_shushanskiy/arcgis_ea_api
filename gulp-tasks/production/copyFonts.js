module.exports = function (gulp, plugins) {
    return function(){
        return gulp.src('resources/fonts/**')
            .pipe(gulp.dest('build/production/resources/fonts'));
    }
};