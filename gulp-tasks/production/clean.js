module.exports = function(gulp, plugins) {
    return function () {
        return gulp.src('build/production', {read: false})
            .pipe(plugins.clean());
    }
}