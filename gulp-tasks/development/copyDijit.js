module.exports = function (gulp, plugins) {
    return function() {
        return gulp.src('vendor/dojo-release-1.7.3-src/dijit/**').pipe(gulp.dest('build/development/dijit'));
    }
};