module.exports = function(gulp, plugins) {
    return function () {
        return gulp.src('build/development', {read: false})
            .pipe(plugins.clean());
    }
}