module.exports = function (gulp, plugins) {
    return function() {
        return gulp.src([
            'djConfig.js',
            'build/development/dojo/dojo.js'
            //'build/development/dijit/main.js',
            //'build/development/dojox/main.js',
            //'build/development/gcapi/main.js'
        ]).pipe(plugins.concat('js.js'))
            .pipe(gulp.dest('build/development'));
    }
};