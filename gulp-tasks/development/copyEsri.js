module.exports = function (gulp, plugins) {
    return function() {
        return gulp.src('vendor/arcgis_js_v32_api/arcgis_js_api/library/3.2/jsapi/js/esri/**').pipe(gulp.dest('build/development/esri'));
    }
};