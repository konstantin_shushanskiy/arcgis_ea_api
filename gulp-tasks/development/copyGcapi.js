module.exports = function (gulp, plugins) {
    return function() {
        return gulp.src('gcapi/**').pipe(gulp.dest('build/development/gcapi'));
    }
};