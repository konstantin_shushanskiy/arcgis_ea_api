module.exports = function (gulp, plugins) {
    return function() {
        return gulp.src('resources/sass/gcapi.scss')
            .pipe(plugins.compass({
                sass: 'resources/sass',
                css: 'resources/css',
                style: 'expanded',
                comments: true
            }))
    }
};