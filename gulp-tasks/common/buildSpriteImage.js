module.exports = function (gulp, plugins) {

    var spritePaths = require('./spritePaths');

    var spriteData = require('./spriteData')(gulp, plugins);

    return function() {
        return spriteData.img.pipe(gulp.dest(spritePaths.destImgPath));
    }
};