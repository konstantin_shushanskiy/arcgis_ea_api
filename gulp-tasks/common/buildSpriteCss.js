module.exports = function (gulp, plugins) {

    var spritePaths = require('./spritePaths');

    var spriteData = require('./spriteData')(gulp, plugins);

    return function() {
        return spriteData.css.pipe(gulp.dest(spritePaths.destCssPath));
    }
};