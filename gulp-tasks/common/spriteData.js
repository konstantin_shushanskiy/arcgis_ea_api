/**
 * получение информации для билда спрайта
 */

var spritePaths = require('./spritePaths');

module.exports =  function(gulp, plugins){
    
    var spriteData = gulp.src(spritePaths.icons).pipe(plugins.spritesmith({
        imgName: spritePaths.destImgName,
        cssName: spritePaths.destCssName,
        cssTemplate: spritePaths.cssTemplate
    }));

    return spriteData;
};