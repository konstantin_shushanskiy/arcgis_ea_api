module.exports = {
    icons: 'resources/sass/sprite/*.png',       //где храняться иконки из которых будет собран спрайт
    destImgPath: 'resources/images/',           //куда положить спрайт
    destImgName: 'sprite.png',                  //как спрайт будет называться
    destCssPath: 'resources/sass/mixins/',      //куда положить файл sass с описанием спрайта
    destCssName: 'sprite.scss',                 //как будет называть этот файл
    cssTemplate: 'resources/sass/sprite/style'  //шаблон описания спрайта
};