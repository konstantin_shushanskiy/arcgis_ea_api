module.exports = function (gulp, plugins, host, release) {
    return function () {

        var fs = require('fs');

        var fileContent = fs.readFileSync('djConfig_src.js', {encoding: 'utf8'});

        var result = fileContent.replace(/\{host}/g, host).replace(/\{release}/g, release);

        fs.writeFileSync('djConfig.js', result, 'utf8');

        return gulp;
    }
};