var gulp = require('gulp');                                                                      //gulp - сборщик
var plugins = require('gulp-load-plugins')();                                                    //динамическая подгрузка плагинов

var runSequence = require('run-sequence').use(gulp);                                             //плагин запускающий таски по очереди

gulp.task('build-sprite-image', require('./gulp-tasks/common/buildSpriteImage')(gulp, plugins)); //билд sprite.png
gulp.task('build-sprite-css', require('./gulp-tasks/common/buildSpriteCss')(gulp, plugins));     //билд scss
gulp.task('build-compass', require('./gulp-tasks/common/buildCompass')(gulp, plugins));          //билд gcapi.css

gulp.task('dev-concat-css', require('./gulp-tasks/development/concatCss')(gulp, plugins));       //конкатенация dijit.css, esri.css, gcapi.css в gcapi.css
gulp.task('prod-concat-css', require('./gulp-tasks/production/concatCss')(gulp, plugins));       //конкатенация dijit.css, esri.css, gcapi.css в gcapi.css (минификация)

gulp.task('dev-copy-sprite.png', require('./gulp-tasks/development/copySprite')(gulp, plugins));
gulp.task('prod-copy-sprite.png', require('./gulp-tasks/production/copySprite')(gulp, plugins));

gulp.task('dev-copy-fonts', require('./gulp-tasks/development/copyFonts')(gulp, plugins));
gulp.task('prod-copy-fonts', require('./gulp-tasks/production/copyFonts')(gulp, plugins));

gulp.task('dev-copy-dojo', require('./gulp-tasks/development/copyDojo')(gulp, plugins));
gulp.task('dev-copy-dijit', require('./gulp-tasks/development/copyDijit')(gulp, plugins));
gulp.task('dev-copy-dojox', require('./gulp-tasks/development/copyDojox')(gulp, plugins));
gulp.task('dev-copy-esri', require('./gulp-tasks/development/copyEsri')(gulp, plugins));
gulp.task('dev-copy-gcapi', require('./gulp-tasks/development/copyGcapi')(gulp, plugins));
gulp.task('dev-make-djConfig', require('./gulp-tasks/common/makeDjConfig')(gulp, plugins, 'gcapi.local', 'development'));
gulp.task('dev-concat-gcapi.js', require('./gulp-tasks/development/concatGcapi')(gulp, plugins));

gulp.task('build-gcapi', require('./gulp-tasks/production/buildGcapi')(gulp, plugins));

gulp.task('prod-copy-dojo', require('./gulp-tasks/production/copyDojo')(gulp, plugins));
gulp.task('prod-copy-dijit', require('./gulp-tasks/production/copyDijit')(gulp, plugins));
gulp.task('prod-copy-dojox', require('./gulp-tasks/production/copyDojox')(gulp, plugins));
gulp.task('prod-copy-esri', require('./gulp-tasks/production/copyEsri')(gulp, plugins));
gulp.task('prod-copy-gcapi', require('./gulp-tasks/production/copyGcapi')(gulp, plugins));
gulp.task('prod-make-djConfig', require('./gulp-tasks/common/makeDjConfig')(gulp, plugins, 'gcapi.local', 'production'));
gulp.task('prod-concat-gcapi.js', require('./gulp-tasks/production/concatGcapi')(gulp, plugins));

gulp.task('dev-clean', require('./gulp-tasks/development/clean')(gulp, plugins));
gulp.task('prod-clean', require('./gulp-tasks/production/clean')(gulp, plugins));

gulp.task('doc-clean', require('./gulp-tasks/documentation/clean')(gulp, plugins));
gulp.task('doc-build', require('./gulp-tasks/documentation/jsdock')(gulp, plugins));
gulp.task('doc-sdk', require('./gulp-tasks/documentation/copyEsriSdk')(gulp, plugins));

gulp.task('development', function () {
    runSequence(
        'dev-clean',
        'build-sprite-image',
        'build-sprite-css',
        'build-compass',
        'dev-concat-css',
        'dev-concat-css',
        'dev-copy-sprite.png',
        'dev-copy-fonts',
        'dev-copy-dojo',
        'dev-copy-dijit',
        'dev-copy-dojox',
        'dev-copy-esri',
        'dev-copy-gcapi',
        'dev-make-djConfig',
        'dev-concat-gcapi.js',
        function () {
            gulp.src('djConfig.js').pipe(plugins.clean());
        }
    );
});

gulp.task('production', function () {
    runSequence(
        'prod-clean',
        'build-sprite-image',
        'build-sprite-css',
        'build-compass',
        'prod-concat-css',
        'prod-concat-css',
        'prod-copy-sprite.png',
        'prod-copy-fonts',
        'build-gcapi',
        'prod-copy-dojo',
        'prod-copy-dijit',
        'prod-copy-dojox',
        'prod-copy-esri',
        'prod-copy-gcapi',
        'prod-make-djConfig',
        'prod-concat-gcapi.js',
        function () {
            gulp.src('djConfig.js').pipe(plugins.clean());
            gulp.src('tmp_release').pipe(plugins.clean());
        }
    );
});

gulp.task('documentation', function () {
    runSequence(
        'doc-clean',
        'doc-build',
        'doc-sdk'
    )
});

