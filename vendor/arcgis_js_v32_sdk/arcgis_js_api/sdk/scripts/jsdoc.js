function prettify(){
  ifWebPrint();
  prettyPrint();
  
  //if(window !== window.top){

  //}
}
function genEmailLink (o) {
    var subjTxt = "subject=" + encodeURIComponent ("ESRI Help Topic")
    var bodyTxt = "body=" + 
              encodeURIComponent ("Here is a useful topic I found on esri.com: ") + 
              encodeURIComponent (window.location.href)                
    o.href = "mailto:?"+ subjTxt + "&" + bodyTxt                
}

function printTopic () {
    var path = document.location.pathname
    path = path.substring (path.lastIndexOf ("/")+1) + "#webprint"
    return window.open (path)
}

function syncToc (pathname, hash) {
	if (!hash) {
		hash = ""
	}
	
	
	paths = pathname.split ("/")
	//IE 6 hack, IE6 uses \ and other browser use / when loading local files
	if (paths.length <= 2) {
		paths = pathname.split ("\\")
	}
   
  console.log(pathname);
  if(pathname.indexOf('/jshelp/') !== -1){
    window.location = "../jshelp_start.htm#jshelp/" +paths[paths.length-1] + hash
  }
  if(pathname.indexOf('/jssamples/') !== -1){
    window.location = "../jssamples_start.htm#jssamples/" +paths[paths.length-1] + hash
  }
  if(pathname.indexOf('/jsapi/') !== -1){
    window.location = "../jsapi_start.htm#jsapi/" +paths[paths.length-1] + hash
  }
  

}

//		 this is the event handler for page load event
//		it does 2 thing
//		1. redirect page to sync with toc
//      or
//		2. exec print function - mod css 
function ifWebPrint () {
    var hash = document.location.hash 

	//if url has a hash
    if (hash) {
		//if url is encode as http://xyz#webprint
		if (hash == "#webprint") {
			var wparts = ["wpart0", "wpart1", "wpart2", "wh_footer"]

			for (var i=0; i<wparts.length; i++) {	
				var ele = document.getElementById (wparts[i])
				if (ele) {
					ele.style.display = "none"
				}
			}
		} else {
			//if top-level page, no frame
			
			if (parent == self) {
				syncToc (self.location.pathname, hash)
			}
			
			
		} 
		
    } else {
	//if url does not have a hash	
			//if top-level page, no frame
			
			if (parent == self) {
				syncToc (self.location.pathname, hash)
			}
			
	}
}