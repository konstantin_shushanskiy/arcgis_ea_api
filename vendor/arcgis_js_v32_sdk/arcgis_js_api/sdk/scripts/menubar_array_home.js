/*	Current Tab Names:
	homeTab - productsTab - servicesTab - industriesTab -	trainingTab - supportTab -  newsTab - eventsTab - companyTab 
*/

/*
	addTabURL syntax:
	addTabURL(  "/directory_path" , "tabName"  );
*/
addTabURL( "home", 				           "homeTab"	);
addTabURL( "library", 		   				"LibraryTab"	);
addTabURL( "content_maps", 		   			"onlineTab"	);
addTabURL( "services", 		   				"servicesTab"	);
addTabURL( "blog", 		   					"blogTab"	);
addTabURL( "support", 		   				"supportTab"	);
addTabURL( "geodatabase", 		   		"datamanageTab"	);
addTabURL( "gisenterprise", 		   		"datamanageTab"	);
addTabURL( "webgeodata", 		   		"datamanageTab"	);
addTabURL( "datamanagement", 		   		"datamanageTab"	);
addTabURL( "arcgismobile", 		   			"arcgismobTab"	);
addTabURL( "applications", 		   			"appTab"	);

// ======================================================================
//  MENUS

// addMainItem(description, URL, _target, tabName);
addMainItem("Home", "index.html", "_top", "homeTab");
//addMainItem("Content", "http://arcgisonline.esri.com/index.cfm?fa=catalog.services", "_blank", "onlineTab");
addMainItem("Concepts", "help/jshelp_start.htm", "", "conceptTab");
addMainItem("Samples", "help/jssamples_start.htm", "", "samplesTab");
addMainItem("API Reference", "help/jsapi_start.htm", "", "apiTab");
addMainItem("Blog", "http://blogs.esri.com/Dev/blogs/arcgisserver/default.aspx", "_blank", "blogTab");	
addMainItem("Forum", "http://forums.arcgis.com/forums/15-ArcGIS-API-for-JavaScript", "_blank", "forumTab");
addMainItem("Code Gallery","http://resources.arcgis.com/gallery/file/arcgis-api-for-javascript","_blank","galleryTab");
	
generateMenus();

