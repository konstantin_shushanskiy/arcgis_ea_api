({
  io: {
    proxyNotSet:"esri.config.defaults.io.proxyUrl is not set."
  },
  
  map: {
    deprecateReorderLayerString: "Map.reorderLayer(/*String*/ id, /*Number*/ index) deprecated. Use Map.reorderLayer(/*Layer*/ layer, /*Number*/ index).",
    deprecateShiftDblClickZoom: "Map.(enable/disable)ShiftDoubleClickZoom deprecated. Shift-Double-Click zoom behavior will not be supported."
  },

  geometry: {
    deprecateToScreenPoint:"esri.geometry.toScreenPoint deprecated. Use esri.geometry.toScreenGeometry.",
    deprecateToMapPoint:"esri.geometry.toMapPoint deprecated. Use esri.geometry.toMapGeometry."
  },

  layers: {
    tiled: {
      tileError:"Unable to load tile"
    },
    
    dynamic: {
      imageError:"Unable to load image"
    },
    
    graphics: {
      drawingError:"Unable to draw graphic "
    },

    agstiled: {
      deprecateRoundrobin:"Constructor option 'roundrobin' deprecated. Use option 'tileServers'."
    },

    imageParameters: {
      deprecateBBox:"Property 'bbox' deprecated. Use property 'extent'."
    },
    
    FeatureLayer: {
      noOIDField: "objectIdField is not set [url: ${url}]",
      fieldNotFound: "unable to find '${field}' field in the layer 'fields' information [url: ${url}]",
      noGeometryField: "unable to find a field of type 'esriFieldTypeGeometry' in the layer 'fields' information. If you are using a map service layer, features will not have geometry [url: ${url}]",
      invalidParams: "query contains one or more unsupported parameters",
      updateError: "an error occurred while updating the layer"
    }
  },

  tasks: {
    gp: {
      gpDataTypeNotHandled:"GP Data type not handled."
    },
        
    na: {
      route: {
        routeNameNotSpecified: "'RouteName' not specified for atleast 1 stop in stops FeatureSet."
      }
    },
    
    query: {
      invalid: "Unable to perform query. Please check your parameters."
    }
  },

  toolbars: {
    draw: {
      convertAntiClockwisePolygon: "Polygons drawn in anti-clockwise direction will be reversed to be clockwise.",
      addPoint: "This is using my localization file.",
      addShape: "This is using my localization file.",
      addMultipoint: "Click to start adding points",
      freehand: "Press down to start and let go to finish",
      start: "This is using my localization file.",
      resume: "This is using my localization file.",
      complete: "This is using my localization file.",
      finish: "This is using my localization file.",
      invalidType: "Unsupported geometry type"
    },
    edit: {
      invalidType: "Unable to activate the tool. Check if the tool is valid for the given geometry type.",
      deleteLabel: "Delete"
    }
  },
  
  virtualearth: {
    // minMaxTokenDuration:"Token duration must be greater than 15 minutes and lesser than 480 minutes (8 hours).",
    
    vetiledlayer: {
      //tokensNotSpecified:"Either clientToken & serverToken must be provided or tokenUrl must be specified."
      bingMapsKeyNotSpecified: "BingMapsKey must be provided."
    },
    
    vegeocode: {
      //tokensNotSpecified:"Either serverToken must be provided or tokenUrl must be specified.",
      bingMapsKeyNotSpecified: "BingMapsKey must be provided.",
      requestQueued: "Server token not retrieved. Queing request to be executed after server token retrieved."
    }
  },
  widgets: {
    attributeInspector: {
      NLS_first: "First",
      NLS_previous: "Previous",
      NLS_next: "Next",
      NLS_last: "Last",
      NLS_deleteFeature: "Delete",
      NLS_title: "Edit Attributes",
      NLS_errorInvalid: "Invalid",
      NLS_validationInt: "Value must be an integer.",
      NLS_validationFlt: "Value must be a float.",
      NLS_of: "of",
      NLS_noFeaturesSelected: "No features selected"
    },
    overviewMap: {
      NLS_drag: "Drag To Change The Map Extent",
      NLS_show: "Show Map Overview",
      NLS_hide: "Hide Map Overview",
      NLS_maximize: "Maximize",
      NLS_restore: "Restore",
      NLS_noMap: "'map' not found in input parameters",
      NLS_noLayer: "main map does not have a base layer",
      NLS_invalidSR: "spatial reference of the given layer is not compatible with the main map",
      NLS_invalidType: "unsupported layer type. Valid types are 'TiledMapServiceLayer' and 'DynamicMapServiceLayer'"
    },
    timeSlider: {
      NLS_first: "First",
      NLS_previous: "Previous",
      NLS_next: "Next",
      NLS_play: "Play/Pause",
      NLS_invalidTimeExtent: "TimeExtent not specified, or in incorrect format."
    },
    attachmentEditor: {
      NLS_attachments: "Attachments:",
      NLS_add: "Add",
      NLS_none: "None"
    },
    editor: {
      tools: {
        NLS_attributesLbl: "Attributes",
        NLS_cutLbl: "Cut",
        NLS_deleteLbl: "Delete",
        NLS_extentLbl: "Extent",
        NLS_freehandPolygonLbl: "Freehand Polygon",
        NLS_freehandPolylineLbl: "Freehand Polyline",
        NLS_pointLbl: "Point",
        NLS_polygonLbl: "Polygon",
        NLS_polylineLbl: "Polyline",
        NLS_reshapeLbl: "Reshape",
        NLS_selectionNewLbl: "New selection",
        NLS_selectionAddLbl: "Add to selection",
        NLS_selectionClearLbl: "Clear selection",
        NLS_selectionRemoveLbl: "Subtract from selection",
        NLS_selectionUnionLbl: "Union",
        NLS_autoCompleteLbl: "Auto Complete",
        NLS_unionLbl: "Union",
        NLS_rectangleLbl: "Rectangle",
        NLS_circleLbl: "Circle",
        NLS_ellipseLbl: "Ellipse",
        NLS_triangleLbl: "Triangle",
        NLS_arrowLbl: "Arrow",
        NLS_arrowLeftLbl: "Left Arrow",
        NLS_arrowUpLbl: "Up Arrow",
        NLS_arrowDownLbl: "Down Arrow",
        NLS_arrowRightLbl: "Right Arrow",
        NLS_undoLbl: "Undo",
        NLS_redoLbl: "Redo"
      }
    },
    legend: {
      NLS_creatingLegend: "Creating legend",
      NLS_noLegend: "No legend"
    },
    popup: {
      NLS_searching: "Searching",
      NLS_prevFeature: "Previous feature",
      NLS_nextFeature: "Next feature",
      NLS_close: "Close",
      NLS_prevMedia: "Previous media",
      NLS_nextMedia: "Next media",
      NLS_noInfo: "No information available",
      NLS_noAttach: "No attachments found",
      NLS_maximize: "Maximize",
      NLS_restore: "Restore"
    }
  },
  arcgis: {
    utils: {
      baseLayerError: "Unable to load the base map layer",
      geometryServiceError: "Provide a geometry service to open Web Map."
    }
  }
})