<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<meta name="keywords" content="ArcGIS JavaScript API" />
	<link href="jsdoc.css" rel="stylesheet" type="text/css"/>
  <link href="../jssamples/prettify/prettify.css" rel="stylesheet" type="text/css"/>
  <script src="../jssamples/prettify/prettify.js" type="text/javascript"></script>
  <script src="../../scripts/jsdoc.js" type="text/javascript"></script>
	<title>Inside esri.request</title>
</head>

<body onload="prettify();">

<div id="pageBanner">Inside esri.request<span class="bannerLinks"><a id="wpart0" href="#" onclick="javascript:genEmailLink(this)"><img border="0" src="../../graphics/mail.png" title="E-mail This Topic" alt="E-mail This Topic"/></a><a id="wpart1" href="#" onclick="javascript:printTopic();"><img border="0" src="../../graphics/print.png" title="Printable Version" alt="Printable Version"/></a><a id="wpart2" href="http://support.esri.com/en/feedback" target="_blank"><img border="0" src="../../graphics/feedback.png" title="Give Us Feedback" alt="Give Us Feedback"/></a></span></div>

<div id="nstext">

<!--***START TEXT - DO NOT WRITE ABOVE THIS LINE***-->

<p><a href="../jsapi/namespace_esri.htm" target="_blank">esri.request</a> is a utility method to retrieve data from a web server. Data can be static (stored in a file on the web server), or it can be dynamic (generated on-demand by a web service). esri.request can handle the following formats:  </p>
<ul>
  <li>plain text
  <li><a href="http://en.wikipedia.org/wiki/XML" target="_blank">XML</a>
  <li><a href="http://en.wikipedia.org/wiki/JSON" target="_blank">JSON</a>
  <li><a href="http://en.wikipedia.org/wiki/JSONP" target="_blank">JSONP</a> or "JSON with padding"
</ul>

<h2>Prerequisites</h2>
<p>Before a request can be made, some information is required:
  <ol>
    <li>Location of the data i.e., URL
    <li>For web services, any required parameters and appropriate values
    <li>Data format
  </ol>
</p>

<p>Once those are known, there are two requirements to use esri.request.</p>
<p>1. Import the module that has the source code for esri.request function:</p>
<div class="detailSample">
<pre class="prettyprint" style="border:none;">
dojo.require("esri.utils");
</pre>
</div>
<p><strong>Note</strong>:  esri.request is used extensively throughout the API and is loaded by other modules. Depending on which modules are loaded by an app, it may not be necessary to explicitly require esri.utils.</p>
<p>2. Set a proxy for the application:</p>
<div class="detailSample">
<pre class="prettyprint" style="border:none;">
esri.config.defaults.io.proxyUrl = "http://apps.myorganization.com/proxy.ashx";
</pre>
</div>
<p>A proxy is a simple script that runs on a web server. It is automatically used by esri.request for certain types of requests. More information is available on the <a href="ags_proxy.htm" target="_blank">proxy set up page</a>.</p>

<h2>Syntax</h2>
<div class="detailSample">
<pre class="prettyprint" style="border:none;">
var request = esri.request({
  // Location of the data
  url: "&lt;String>",
  // Service parameters if required, sent with URL as key/value pairs
  content: {
    parameter1: &lt;value>,
    parameter2: &lt;value>
  },
  // Data format
  handleAs: "&lt;String>"
});
</pre>
</div>
<p>In the code above, esri.request is not returning the actual data. If it were to return the actual data, it would have to wait for the data to be downloaded - which typically takes some time depending on network traffic, available bandwidth and the load on the remote server. Until then, the application would be frozen - not allowing you to interact with it. This is called <code>synchronous</code> behavior. Instead, to keep the application responsive, esri.request always returns a <code>request</code> object that represents the request you've just made and makes the data available to you later when it is downloaded. This is called <code>asynchronous</code> behavior.</p>
<p>To access the data after it is downloaded, two functions are registered with the request object by calling its method named <code>then</code>. The first function (named requestSucceeded in the example code below, the function name is up to the developer) will be executed if the data retrieved without error. The second function (named requestFailed) will be executed if data download failed for some reason. These functions are also known as <code>callbacks.</code></p>
<div class="detailSample">
<pre class="prettyprint" style="border:none;">
function requestSucceeded(data) {
  console.log("Data: ", data); // print the data to browser's console
}

function requestFailed(error) {
  console.log("Error: ", error.message);
}

request.then(requestSucceeded, requestFailed);
</pre>
</div>

<p>Callbacks can also be defined <code>inline</code>:</p>
<div class="detailSample">
<pre class="prettyprint" style="border:none;">
request.then(
  function (data) {
    console.log("Data: ", data);
  },
  function (error) {
    console.log("Error: ", error.message);
  }
);
</pre>
</div>

<h2>Examples</h2>
<p><strong>Plain text:</strong></p>
<div class="detailSample">
<pre class="prettyprint" style="border:none;">
var earthquakes = esri.request({
  url: "http://earthquake.usgs.gov/earthquakes/catalogs/eqs1day-M1.txt",
  handleAs: "text"
});
earthquakes.then(requestSucceeded, requestFailed);
</pre>
</div>
<a href="../jssamples/data_requestPlainText.html" target="_blank">Sample that requests <strong>plain text</strong> using esri.request</a>

<p><strong>XML</strong></p>
<div class="detailSample">
<pre class="prettyprint" style="border:none;">
var earthquakes = esri.request({
  url: "http://earthquake.usgs.gov/earthquakes/catalogs/7day-M5.xml",
  handleAs: "xml"
});
earthquakes.then(requestSucceeded, requestFailed);
</pre>
</div>
<a href="../jssamples/data_requestXML.html" target="_blank">Sample that requests <strong>XML</strong> using esri.request</a>

<p><strong>JSON</strong></p>
<div class="detailSample">
<pre class="prettyprint" style="border:none;">
var earthquakes = esri.request({
  url: "http://earthquake.usgs.gov/earthquakes/feed/geojson/4.5/week",
  handleAs: "json"
});
earthquakes.then(requestSucceeded, requestFailed);
</pre>
</div>
<a href="../jssamples/data_requestJson.html" target="_blank">Sample that requests <strong>JSON</strong> using esri.request</a>

<p><strong>JSONP</strong></p>
<div class="detailSample">
<pre class="prettyprint" style="border:none;">
var flickrPhotos = esri.request({
  url: "http://api.flickr.com/services/feeds/photos_public.gne",
  content: {
    tags:    "earthquakes,us",
    tagmode: "all",
    format:  "json"
  },
  handleAs: "json",
  callbackParamName: "jsoncallback"
});
flickrPhotos.then(requestSucceeded, requestFailed);
</pre>
</div>
<a href="../jssamples/data_requestJsonp.html" target="_blank">Sample that requests <strong>JSONP</strong> using esri.request</a>

<p><a href="http://www.flickr.com/services/feeds/docs/photos_public/" target="_blank">Flickr's Public Feed</a> is an example of a web service that dynamically generates data (list of photos) based on some search criteria. The <a href="http://links.esri.com/server/rest-api" target="_blank">ArcGIS REST API</a> and <a href="http://links.esri.com/api/portal/rest" target="_blank">ArcGIS Portal API</a> are other examples where data is made available on-demand through web services.</p>
<p>The example above uses a special property called <code>callbackParamName</code>. You need to set this property when requesting for data available in JSONP format. The value of this property depends on the owner of the web service, which is usually available from the service documentation. For the flickr service, the value should be <code>"jsoncallback"</code>, as specified in <a href="http://www.flickr.com/services/api/response.json.html" target="_blank">Flickr's documentation</a>. For ArcGIS services, callbackParamName is always <code>"callback"</code>.</p>

<p>The information and examples above provide the background necessary to write applications that work with remote data. For additional information on the inner-workings of esri.request, and additional advanced use cases, please read on.</p>



<h2>Additional Information and Use Cases</h2>
<p><strong>Upload a File</strong></p>
<p>esri.request is primarily used to download data. However, it can also be used to upload a file from user's computer to a web server. The file can be of any type: Word Document, Excel Spreadsheet, CSV, PDF, JPEG, PNG etc.</p>
<p>To build an application capable of uploading files, the following is required:</p>
<p>
  <ol>
    <li>Import relevant source code.
<div class="detailSample">
<pre class="prettyprint" style="border:none;">
dojo.require("esri.utils");   // this module contains implementation for esri.request
dojo.require("dojo.io.iframe"); // this module adds upload capability to older browsers
</pre>
</div>
    <li>Set a proxy for the application.
<div class="detailSample">
<pre class="prettyprint" style="border:none;">
esri.config.defaults.io.proxyUrl = "proxy.ashx";
</pre>
</div>
    <li>Create a HTML Form with File Input control. Users can click on this file input control and select a file from their computer.
<div class="detailSample">
<pre class="prettyprint" style="border:none;">
&lt;html>
 &lt;head>
   ...
 &lt;/head>
 &lt;body>

   &lt;form id="uploadForm" method="post" enctype="multipart/form-data">
     &lt;input type="file" name="attachment" />
   &lt;/form>
   
 &lt;/body>
&lt;/html>
</pre>
</div>
The <code>form</code> element has three attributes: id, method and enctype (stands for encoding type). <code>id</code> assigns a unique name to the form so that you can refer to it from the JavaScript code below. <code>method</code> and <code>enctype</code> attributes indicate how the file should be uploaded and should always be "post" and "multipart/form-data", respectively.
<br><br>
The <code>input</code> element has two attributes: <code>type</code> and <code>name</code>. type should always be "file" since you want to present a file control to the user. name attribute uniquely identifies this input element within the form and its value depends on the web service to which you're uploading the file. For example: if you're uploading an attachment to <a href="http://sampleserver3.arcgisonline.com/ArcGIS/SDK/REST/fsaddattachment.html" target="_blank">ArcGIS Server Feature Service</a>, the name would be "attachment".
  <li>Call esri.request to upload the selected file.
<div class="detailSample">
<pre class="prettyprint" style="border:none;">
var upload = esri.request({
  url: "<type-web-service-url-here>",
  form: document.getElementById("uploadForm"),
  handleAs: "json"
});
upload.then(requestSucceeded, requestFailed);
</pre>
</div>
The <a href="../jssamples/exp_dragdrop.html" target="_blank">File Access with Drag and Drop sample</a> shows a working example of using this method to upload a file. <strong>Note</strong>: This file upload feature was added in version <strong>2.7</strong> of the API.
  </ol>
</p>

<p><strong>Using a Proxy</strong></p>
<p>A proxy is a simple script that runs on your web server. It relays a request made by your web application running on a user's web browser to a remote web server and relays the response returned by the remote server back to the application. This is necessary because web browsers allow applications to make requests and retrieve data only from the web server where they are hosted. In other words, without a proxy, an application can only request information from the web server where it is running. This is called <a href="http://en.wikipedia.org/wiki/Same_origin_policy" target="_blank">same origin policy</a> (additional information on <a href="https://developer.mozilla.org/en/Same_origin_policy_for_JavaScript" target="_blank">same origin policy info on the Mozilla Developer Network</a>). JSONP is a technique originally developed to circumvent same origin policy.</p>

<p>If you're building an application that requires data from a variety of services that run on different domains, you need a proxy to get around the same origin restriction. The proxy will act as an intermediary between the browser and remote server(s). The diagram below illustrates how this works.<br>
<img src="images/esri-request-proxy.png" alt="esri.request using a proxy" title="esri.request using a proxy" /></p>
<p><strong>Note</strong>:  A proxy is <em>not</em> required if your application and all of the data/web services used by it are on the same domain.</p>
<p>For more information on configuring a proxy, and example proxy pages for various server environments, refer to the <a href="ags_proxy.htm" target="_blank">Using the Proxy Page</a> conceptual help topic.</p>

<p><strong>CORS</strong></p>
<p>CORS stands for Cross Origin Resource Sharing. It is a specification that allows a web server and web browser to interact and determine if a cross-origin request should be allowed. The specification stipulates that the interaction should happen through a set of HTTP headers, which means that both the browser and server should implement the CORS specification. The <a href="https://developer.mozilla.org/en/http_access_control" target="_blank">Mozilla Developer Network</a> has additional information on the technical details of how CORS works.</p>

<p>CORS provides a better way to execute cross origin requests relative to using a proxy. esri.request will avoid using a proxy if the following are true:</p>
<ol>
  <li>The <a href="http://caniuse.com/#search=cors" target="_blank">browser supports CORS</a>. All modern browsers support CORS.
  <li>The target web server supports CORS. ArcGIS Server 10.1 supports CORS out of the box. Versions 9.3 and 10.0 do not have support for CORS but can be easily added by an administrator. <a href="http://enable-cors.org/" target="_blank">enable-cors.org</a> has detailed instructions on how to do this.
</ol>
<p>If an application makes requests to an ArcGIS Server, esri.request will automatically detect if that server supports CORS. However, if a server is known to support CORS, it is recommended that you configure your application manually by telling the API that a server supports CORS:
<div class="detailSample">
<pre class="prettyprint" style="border:none;">
esri.config.defaults.io.corsEnabledServers.push("services.myorganization.com");
</pre>
</div>
<p>The diagram below illustrates how an application talks to a remote server that supports CORS.<br>
<img src="images/esri-request-cors.png" alt="esri.request using CORS" title="esri.request using CORS" /></p>
<strong>Note</strong>:  Initial CORS support was added at version 2.5. Automatic CORS detection for ArcGIS server was added at version 3.0

<p><strong>Security</strong></p>
<p>While much of the data and many of the services on the web today are freely accessible to everyone, some are protected and accessible only to some users as designated by the owner/publisher/server administrator. ArcGIS Server provides administrators the ability to control access to their GIS web services using a token-based authentication mechanism. esri.request has integrated support for ArcGIS token-based authentication.</p>
<p>When developing an application that uses secure ArcGIS services, import the <a href="../jsapi/identitymanager.htm" target="_blank">identity manager</a> module and esri.request will automatically use it to manage the authentication process:</p>
<div class="detailSample">
<pre class="prettyprint" style="border:none;">
dojo.require("esri.utils");
dojo.require("esri.IdentityManager");
</pre>
</div>
<p><strong>Note</strong>:  The identity manager was added to the API at version <strong>2.5</strong>.</p>



<h2>Under the Hood:  Determining Which Dojo Method is Used</h2>
<p>esri.request uses the appropriate method to execute a request based on the target of the request and what the browser supports. The native method to do AJAX requests is to use XMLHttpRequest(XHR). Because there are subtle differences in how XHR is implemented across browsers, Dojo abstracts away and addresses cross-browser inconsistencies via dojo.xhrGet and dojo.xhrPost. Dojo also provides methods in the dojo.io namespace to do AJAX-style requests with JSONP and to use an iframe to upload files.</p>
<p>esri.request sits on top of the methods defined by Dojo to provide a clean, simple way to execute AJAX-style requests. Depending on the arguments passed to esri.request, different Dojo methods are used. The following pseudocode is a rough outline of how esri.request works.</p>
<br>
<strong>JSONP:</strong>
<pre>
If JSONP request
  If target on same domain
    Convert to JSON request
  Else If CORS available
    Convert to JSON request
  Else
    If request size is 2000 characters or less
      Call dojo.io.script.get
    Else
      Convert to JSON request
</pre >
<br>
<strong>JSON / XML / plain text:</strong>
<pre>
If JSON / XML / PlainText request
  If target not on same domain && CORS not available
    Rewrite request url to use proxy
  If request size is 2000 characters or less
    Call dojo.xhrGet
  Else
    Call dojo.xhrPost
</pre>
<br>
<strong>File Uploads:</strong>
<pre>
If File Upload request
  If browser supports native file upload
    If target not on same domain && CORS not available
      Rewrite request url to use proxy
    Call dojo.xhrPost
  Else
    If target not on same domain
      Rewrite request url to use proxy
    Call dojo.io.iframe.send
</pre>
<br>



<h2>Additions to esri.request at Various Releases</h2>
<p>Changes to esri.request have been made at various API releases. Below is a summary of those changes:</p>
<ul>
  <li>1.3:  Setting useProxy to true will force use of a proxy
  <li>2.1:  useProxy was added to an options argument that allows you to specify the useProxy and usePost options. This method includes a default error handler in cases when the server response is an error.  The returned object is dojo.Deferred.
  <li>2.5:  disableIdentityLookup parameter added to options
  <li>2.7: Added support for uploading files from a user's computer to a server. This can be useful if you want to build an application that allows users to upload files to the server. See the <a href="../jssamples_start.htm#jssamples/exp_dragdrop.html" target="_blank">Drag Drop Sample</a> for an example that uses this to upload csv files.
 <li>3.0:  Automatic detection of CORS support and use JSON instead of JSONP where possible
</ul>

<!--***END TEXT - DO NOT WRITE BELOW THIS LINE***-->
</div>
<div id="wh_footer" class="footnote" align="center" >
   Please visit the <a href="http://support.esri.com/en/feedback" target="_blank">Feedback</a> page to comment or give suggestions.<br>
   Copyright &copy; Environmental Systems Research Institute, Inc.
</div>
</body>
</html>
