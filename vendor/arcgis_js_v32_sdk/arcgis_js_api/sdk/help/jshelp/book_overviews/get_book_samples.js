dojo.require("dojo.string")
dojo.require("dojo.io.script");

var book, bookAbbrLookup, baseUrl, content, samples;

bookAbbrLookup = {
  "geocoding": "geocoding",
  "arcgis_server_10": "arcgis_server_ten_one",
  "bing_maps": "bing",
  "data_access": "data",
  "editing": "editing",
  "feature_layers": "feature_layer",
  "find_and_identify": "find",
  "geometry_service": "geometry_service",
  "geoprocessing": "geoprocessing",
  "graphics": "graphics",
  "html5": "html5",
  "image_services": "image",
  "javascript_frameworks": "javascript_framework",
  "layers": "layers",
  "layouts": "layout",
  "map_configuration": "mapconfig",
  "map": "map",
  "mobile": "mobile",
  "network_analyst": "network_analyst",
  "portal": "arcgis_portal_api",
  "query_task": "query",
  "renderers": "renderer",
  "time": "time",
  "toolbars": "toolbar",
  "web_maps": "arcgis_online",
  "widgets": "widget"
}
baseUrl = "http://www.arcgis.com/sharing/rest/search";
content = {
  //"q": "code sample AND typekeywords:javascript NOT Bing NOT Google AND owner:esri_webapi",
  //"q": "typekeywords:javascript NOT Google AND owner:esri_webapi",
  "q": "group:b99ada9698614e97a4859e9fc160169d",
  "start": "1",
  "num": "100",
  "sortField": "uploaded",
  "sortOrder": "desc",
  "f": "json"
}
samples = [];

function init() {
  // figure out which book we're in
  var pieces, lastPiece;
  pieces = document.location.href.split("overview");
  lastPiece = pieces[pieces.length - 1];
  if ( bookAbbrLookup.hasOwnProperty(lastPiece.split(".")[0].slice(1)) ) {
    book = bookAbbrLookup[lastPiece.split(".")[0].slice(1)];
    content.q += " jssample_" + book;
    console.log("just set book to: ", book);
  } else {
    // console.log("Couldn't find a book for: ", lastPiece.split(".")[0].slice(1));
    oops();
    return;
  }
  // get samples from arcgis.com
  querySamples(content);
}

function querySamples(content) {
  // go get sample info and add a loading icon
  dojo.byId("directions").innerHTML = "Getting sample thumbnails and descriptions from ArcGIS.com.";
  dojo.create("img", { "src": "spinner.gif" }, dojo.byId("directions"));
  dojo.io.script.get({
    "url": baseUrl, // global
    "content": content, // passed in as an argument
    "callbackParamName": "callback",
    "load": handleSamples,
    "error": errHandler
  });
}

function handleSamples(resp) {
  // merge results
  if ( samples.length == 0 ) {
    samples = resp.results;
  } else {
    dojo.forEach(resp.results, function(r) {
      samples.push(r);
    });
  }

  // check if there are more samples
  // nextStart is -1 when there are no more samples
  if ( resp.nextStart > 0 ) {
    // query to get the rest of the samples
    // update the start property of content
    content.start = resp.nextStart;
    querySamples(content);
  } else {
    //filterItems();
    renderSamples(samples);
  }
}

function renderSamples(items) {
  // build markup for the samples that match the selected book
  var samplesLeft, samplesRight, sampleString;
  
  items.reverse();
  samplesLeft = samplesRight = "";
  sampleString = "<div class=\"sample\" id=\"sample${0}" + 
    "\"><a href=\"../../jssamples_start.htm#jssamples/${1}\"" +
    "target=\"_blank\"><img class=\"sampleThumb\"" + 
    " src=\"http://www.arcgis.com/sharing/content/items/${2}" + 
    "/info/${3}\"  /></a><span class=\"sampleSnippet\">${4}</span></div>";

  dojo.forEach(items, function(item, idx) {
    // handle null item snippets...
    console.log("render samples, url: ", item.url);
    item.snippet = item.snippet || "";
    var file = item.url.split("/")[item.url.split("/").length - 1];
    var s = dojo.string.substitute(sampleString, [idx, file, item.id, item.thumbnail, item.snippet]);
    idx % 2 ? samplesRight += s : samplesLeft += s;
  });
  // add the markup for the samples to the page
  dojo.byId("samplesLeft").innerHTML = samplesLeft;
  dojo.byId("samplesRight").innerHTML = samplesRight;

  if ( items.length > 0 ) {
    dojo.byId("directions").innerHTML = "Click a thumbnail for more information about a sample.";
  } else {
    oops();
  }
}

function oops() {
    dojo.byId("directions").innerHTML = "Oops, there was a problem getting sample info from ArcGIS.com. Please use the tree to the left to navigate."
}

function errHandler(err) {
  console.log("Error getting samples: ", err);
}

dojo.ready(init);
