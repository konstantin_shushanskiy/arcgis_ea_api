<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
  
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <meta name="keywords" content="ArcGIS JavaScript API" />
    <link href="jsdoc.css" rel="stylesheet" type="text/css" />
    <link href="../jssamples/prettify/prettify.css" rel="stylesheet" type="text/css"/>
    <script src="../jssamples/prettify/prettify.js" type="text/javascript"></script>
    <script src="../../scripts/jsdoc.js" type="text/javascript"></script>
    <title>Using the proxy page</title>
  </head>

  <body onload="prettify();">
    <div id="pageBanner"> Using the proxy page<span class="bannerLinks"><a id="wpart0" href="#" onclick="javascript:genEmailLink(this)"><img border="0" src="../../graphics/mail.png" title="E-mail This Topic" alt="E-mail This Topic"/></a><a id="wpart1" href="#" onclick="javascript:printTopic();"><img border="0" src="../../graphics/print.png" title="Printable Version" alt="Printable Version"/></a><a id="wpart2" href="http://support.esri.com/en/feedback" target="_blank"><img border="0" src="../../graphics/feedback.png" title="Give Us Feedback" alt="Give Us Feedback"/></a></span></div>
    <div id="nstext">
      <!--***START TEXT - DO NOT WRITE ABOVE THIS LINE***-->
      <p>
        The ArcGIS JavaScript API provides a proxy page that handles communication
        with the ArcGIS Server services you use in your application. The proxy
        page consists of server-side code that you set up to run on your Web server.
        The browser sends a request to the proxy and the proxy forwards the request
        to the ArcGIS Server service.
      </p>
      <p>
        A proxy page is necessary in the following situations:
        <ul>
          <li>
            The application creates requests that exceed 2048 characters. While there is
            no official maximum length for a URL some modern browsers have
            imposed <a target="_blank" href="http://www.boutell.com/newfaq/misc/urllength.html">limits</a>. 
            Using a proxy works around this issue by performing a POST request rather than a GET
            request. POST requests are not limited by the 2048 character maximum because the information is 
            transferred in the header instead of the URL. Common situations where you may exceed the URL length are:
            <ul>
              <li>Buffer complex polygons then use the buffered geometry with a query task.</li>
              <li>Specify a spatial reference using well-known text (wkt). </li>
            </ul>
          </li>
          <li>
            The application uses a service that is
            <a href="ags_security.htm">secured with
            token-based authentication</a>
            , and you do not wish to allow users to view the token, or you do not
            want to transmit the token over the network between your Web server and
            your users.
          </li>
          <li>
            The application edits features in a feature service, unless the application
            is on the same domain as the GIS Server.
          </li>
        </ul>
      </p>
     <h3>Install and Configure the proxy page</h3>
      <p>
        To use the proxy page, you need to perform the following steps. These
        are described in detail below.
      </p>
      <ol>
        <li>
          <a href="#Download_Configure">Download and configure</a>
          the proxy page appropriate for your server
        </li>
        <li>
          <a href="#Add_code">Add code</a>
          to your application page to enable use of the proxy
        </li>
        <li>
          <a href="#Secure_web_app">Secure the Web application</a>
           This step is critical for applications that use tokens. If the application is not
           secured correctly, the ArcGIS Server services used by the application are vulnerable to 
           unlimited use by unauthorized persons. See the
        <a href="#Secure_web_app">
        section on securing the application</a>
        below for further information.
        </li>
        <li>
          <a href="#Test">Test</a>
          to ensure the proxy is working correctly
        </li>
      </ol>
      <p>
      </p>

      <h4 id="Download_Configure">
        1. Download and configure the proxy page
      </h4>
      <p>
        In this step, you'll learn how to download and install the proxy page on your Web server. 
        The proxy page runs on your local web server, not on an ESRI server or
        on the ArcGIS Server computer (unless your web server also hosts the ArcGIS
        Server instance). ESRI provides proxy pages for three languages (ASP.NET, Java/JSP, PHP), but you can create a proxy page in 
        another language as well. Download and configure the appropriate proxy for your 
        platform.
      </p>
     <p>
        For services with token-based authentication, the URL of the
        service should use HTTPS, for example, https://www.example.com/arcgis/rest/services/MyMapService/MapServer.
        This ensures that the token appended to the query string of the request
        is encrypted and cannot be intercepted and used by unauthorized parties.
        This would be especially important when the communication between your
        Web server and the ArcGIS Server service travels over the Internet, and
        not just on the local network.
      </p>
       <p><b>ASP.NET</b></p>
       <p>
       Before proceeding ensure, that you have ASP.NET 2.0 or higher installed and registered with Internet Information Services (IIS). 
       If ASP.NET is not configured with IIS, follow the instructions for the version of IIS you are running in the <a target="_blank" href="http://msdn.microsoft.com/en-us/library/ms178477.aspx">ASP.NET and IIS Configuration</a>
       help topic on MSDN. 
       <ol>
           <li>Download <a href="proxypage_net.zip">proxypage_net.zip</a>, then unzip and save the contents to a folder on your web server's root directory. In IIS, the default location for the Web server's root folder is <code>c:\inetpub\wwwroot</code>. If you plan on 
            using the proxy for multiple sites you can create a folder in the web root directory called proxy and place the contents there.</li>
           <li>Unzip and save then contents to your Web server.</li>
           <li>Open IIS Manager. This <a target="_blank" href="http://msdn.microsoft.com/en-us/library/bb763170.aspx">Microsoft Knowledge Base</a> article provides instructions for opening IIS Manager depending on the version of IIS you are using.</li>
           <li>Next, create a Web Application for the proxy folder, these steps also differ depending on the version of IIS you are using. For IIS 5 or 6 use the following steps:
            <ol>
              <li>In the Default Web Site, right-click the proxy directory and select Properties.</li>
              <li>On the Directory tab, in the Application Settings section click the Create button then click OK</li><br />
                <img src="images/createWebApp.png" width="300" height="300"/><br /> 
            </ol>
          <li>If you will be using the proxy page for services with token-based authentication,
          obtain a token for the service. See instructions at
          <a href="ags_security.htm">Working with secure ArcGIS services</a>
          . If your application uses multiple ArcGIS Server systems that require
          tokens, obtain a token for each server.</li>
         <li>Open the configuration proxy page (proxy.config for ASP.NET) in a text or XML editor. </li>
         <li>For each ArcGIS Server that will use the proxy page, add a &lt;serverUrl&gt;
          entry to the configuration XML file within the &lt;serverUrls&gt; section.
          See the proxy configuration file for examples. The serverUrl element can
          have these attributes:
          <ul>
            <li>
              <strong>
                url
              </strong>
              : the URL of the ArcGIS Server machine or the service. If multiple services
              in the same server are used in the application, then the url can point
              to the services root. If only a single service on the server is used, then
              the url can be set to the full service URL.
            </li>
            <li>
              <strong>
                matchAll
              </strong>
              : whether to use the token for all requests with this URL stem. If this
              attribute is true and the url attribute is set to the services root, then
              the entry can be used for multiple services in the application.
            </li>
            <li>
              <strong>
                token
              </strong>
              : the authentication token obtained in step 1. Optional - used only for
              services secured with token-based authentication.
            </li>
          </ul>
          <p>If multiple services on the same server are used in the application, the
          URL may point to the service root (e.g., http://www.example.com/arcgis/rest/services),
          and the matchAll parameter set to true. See commented examples included
          in the downloaded file. Multiple server entries may be added if more than
          one ArcGIS Server computer is used in the application.</p>
          <p>The  mustMatch  attribute in the containing <ProxyConfig> element controls whether only specified sites may be proxied. This attribute should generally be set to true. If set to false, then the proxy page will forward any request to any server. This could potentially allow your proxy page to be used to send requests to third-party servers without your permission. </p>
        </li>
        <li>
          Save the configuration file.
        </li>
       </ol>
       
       </p>
       <p><b>Java/JSP</b>
        <ol>
          <li>Download <a href="proxypage_java.zip">proxypage_java.zip</a>, then unzip and save the contents to your web server's web applications directory. For Tomcat,
          this is the <code>webapps</code> directory under the base Tomcat folder.</li>
          <li>
          If you will be using the proxy page for services with token-based authentication,
          obtain a token for the service. See instructions at
          <a href="ags_security.htm">Working with secure ArcGIS services</a>
          . If your application uses multiple ArcGIS Server systems that require
          tokens, obtain a token for each server.
        </li>
        <li>
          Open the configuration proxy page (proxy.jsp) in your favorite editor.
        </li>
        <li>
          The JSP code defines a
          <code>
            serverUrls
          </code>
          variable. This represents the servers for which you want this JSP to act
          as a proxy. It is a String array where each String has the following syntax:
          <code>
            "&lt;url&gt;,&lt;token&gt;"
          </code>
          . The token is optional. Add URLs to the services you want to access over
          here.
          <br/>
          See commented examples included in the JSP. Multiple server entries may
          be added if more than one ArcGIS Server computer is used in the application.
          <br/>
          <i>
            Note that while this sample proxy includes the configuration in the JSP
            itself, you may want to configure the URLs in a separate resource file
            and have your application reference that file.
          </i>
          <li>
            Save the JSP.
          </li>
        </ol></p>
       <p><b>PHP</b>
        <ol>
          <li>Download <a href="proxypage_php.zip">proxypage_php.zip</a>, then unzip and save the contents to your web server's web applications directory. For 
          Apache installs of PHP, this is a folder under <code>htdocs</code> in the Apache install location. For IIS installs, this is a folder under <code>wwwroot</code>.</li>
          <li>
          If you are using the proxy page for services with token-based authentication,
          obtain a token for the service. See instructions at
          <a href="ags_security.htm">Working with secure ArcGIS services</a>
          . If your application uses multiple ArcGIS Server systems that require
          tokens, obtain a token for each server.
        </li>
        <li>
          Open the proxy page (proxy.php) in your favorite editor.
        </li>
        <li>
          Using the "mustMatch" variable, specify whether the proxy should forward
          requests only to specified servers. If this value is true, the proxy will
          only forward requests to the servers specified in the "serverUrls" variable
          (see below). If this value is false, then the proxy will forward requests
          to any server - be warned that this could potentially allow your proxy
          page to be used to send requests to third-party servers without your permission.
        </li>
        <li>
          Using the "serverUrls" variable, specify the servers that this proxy will
          forward requests to if "mustMatch" is true. It is an array where each element
          configures an ArcGIS server or service using an array with keys named "url",
          "matchAll" and "token". The sematics of these keys are similar to the correspondingly
          named attributes used in the ASP.NET proxy.
        </li>
        <li>
          Save the PHP file.
        </li>
        </ol></p>

      <h4 id="Add_code">
        2. Add code to use the proxy page
      </h4>
      <p>
        In order for your application's page to send requests through the proxy,
        you must add code to the page as follows. In the startup function (e.g.,
        OnPageLoad) in the JavaScript code of your page, add a line to set the
        proxy page. The following example is for the ASP.NET version; substitute
        proxy.jsp or proxy.php as appropriate. If your proxy page is not in the
        same directory as the Web application, adjust the path appropriately. The
        path should use a relative URL (e.g., ../proxy.ashx), or a URL relative
        to the root (e.g., /Proxy/proxy.ashx).
      </p>
      <p><b>Note:</b>
          If you are using version 1.2 or lower of the ArcGIS API for JavaScript, replace "esri.config" in the examples
          below with "esriConfig".
      </p>
      <p>
        For ASP.NET:
      </p>
       <pre class="prettyprint" style="border:none;">
        esri.config.defaults.io.proxyUrl = "proxy.ashx";
      </pre>
      <b>Note:</b> if you are using version 1.2 or lower of the ArcGIS API for JavaScript, the syntax would be
      <code>
        esriConfig.defaults.io.proxyUrl = "proxy.ashx";
      </code>
      <p>
        For Java/JSP:
      </p>
       <pre class="prettyprint" style="border:none;">
        esri.config.defaults.io.proxyUrl = "proxy.jsp"; 
        //relative - if proxy.jsp is in the same application as the web page
      </pre>
      Or
       <pre class="prettyprint" style="border:none;">
        esri.config.defaults.io.proxyUrl = "http://samedomain:sameport/proxy/proxy.jsp";
        //absolute - if proxy.jsp is in another application in the same domain
      </pre>
      <p>
        For PHP:
      </p>
       <pre class="prettyprint" style="border:none;">
        esri.config.defaults.io.proxyUrl = "proxy.php"; 
        //relative - if proxy.php is in the same application as the web page
      </pre>
      Or
       <pre class="prettyprint" style="border:none;">
        esri.config.defaults.io.proxyUrl = "http://samedomain:sameport/proxy/proxy.php";
        //absolute - if proxy.php is in another application in the same domain
      </pre>
      <p>
        <em>
          Only if you are using the page with a service secured with tokens
        </em>
        : add a statement to use the proxy for every request. Do not add this
        line if you only need to use the proxy when requests exceed the browser's
        limit on URL size (see above). If you add this line, the esriConfig.defaults.io.proxyUrl
        property must also be set.
      </p>
       <pre class="prettyprint" style="border:none;">
        esri.config.defaults.io.alwaysUseProxy = true;
      </pre>
      <p>
        If the alwaysUseProxy property is false (the default), then the proxy page
        will be used automatically if the proxy property is set and the request
        exceeds 2000 characters.
      </p>
      <h4 id="Secure_web_app">
        3. Secure the Web application
      </h4>
      <p>
        This step is essential if the application uses services with token-based
        security, and the proxy page is configured with the token.
        <em>
          If you do not secure the Web application, then anyone can send any request
          through your proxy page to the ArcGIS Server service, without using your
          application page.
        </em>
        Your proxy page could be used for unauthorized disclosure of data from
        the service, or for inappropriate or high volume of use of the service.
      </p>
      <p>
        To secure the Web application, you need do
        <strong>
          both
        </strong>
        of the following:
      </p>
      <ol>
        <li>
          Require users of the application to login (authenticate) in order to use
          the application. This ensures that only authorized users can use the application,
          including the proxy page. How you require authentication depends on the
          type of server software you are using. For IIS/ASP.NET servers, you may
          use Windows authentication or ASP.NET forms; see
          <a href="http://msdn2.microsoft.com/en-us/library/aa292471(VS.71).aspx"
          target="_blank">this page at Microsoft</a>
          for information and instructions. For other platforms, consult documentation
          on implementing authentication for your platform.
          <br />
          -- and --
        </li>
        <li>
          Require use of HTTPS via Secure Sockets Layer (SSL). At minimum the login
          page should be secured, especially if using forms or Basic authentication.
          For maximum security, you may require HTTPS for all communication in the
          application. Note that in order to require HTTPS for an application, you
          must obtain and install a SSL certificate for your Web server. If using
          IIS, the
          <a href="http://www.microsoft.com/technet/prodtechnol/WindowsServer2003/Library/IIS/56bdf977-14f8-4867-9c51-34c346d48b04.mspx?mfr=true"
          target="_blank">Microsoft documentation for IIS</a>
          describes how to obtain and install SSL certificates.
        </li>
      </ol>
      <h4 id="Test">
        4. Test and deploy the application
      </h4>
      <p>
        Once you have configured the proxy page with the application, test the
        application to ensure that requests are processed correctly. The application
        should function as it did before the proxy page was implemented.
        If not, you may need to troubleshoot the proxy. If your application environment
        supports debugging mode, you may be able to set breakpoint in the proxy
        page and detect whether it is operating correctly.
      </p>
      <p>
        In an IIS/ASP.NET environment, you can open the application
        in Visual Studio or Visual Web Developer Express, open the proxy.ashx page,
        and set a break point in the ProcessRequest method. Then run the application
        with Debug-Start. The execution should halt at the break point, and you
        should be able to detect where the problem lies. You may also be able to
        set break points in the JavaScript functions of your application, or insert
        alert() statements to display values during execution. As a last resort,
        remove the esriConfig.defaults.io.proxyUrl property value and start over.
      </p>
      <p>One common issue is that the path to the proxy file is entered into the application incorrectly. You can use <a href="http://getfirebug.com/">Firebug</a> or another browser debugging tool to determine if the proxy is located.  
      Open Firebug and activate the NET tab. Then run your application and watch the requests and look for the request that POSTs to the proxy. In the image below
      the request is red and a 404 error is returned which means that the proxy page cannot be found. If you hover your mouse over the request in the NET tab you'll 
      see the location where the application is looking for the proxy.</p>
      <p><img src="images/firebugdebug.png" /></p>
  
      <br />
      <br />
      <!--***END TEXT - DO NOT WRITE BELOW THIS LINE***-->
    </div>
    <div id="wh_footer" class="footnote" align="center" >
   Please visit the <a href="http://support.esri.com/en/feedback" target="_blank">Feedback</a> page to comment or give suggestions.<br>
   Copyright &copy; Environmental Systems Research Institute, Inc.
</div>
  </body>

</html>