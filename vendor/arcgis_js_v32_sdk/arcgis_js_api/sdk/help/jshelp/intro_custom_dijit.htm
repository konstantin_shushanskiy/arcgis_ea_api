<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<meta name="keywords" content="ArcGIS JavaScript API" />
	<link href="jsdoc.css" rel="stylesheet" type="text/css"/>
	<link href="../jssamples/prettify/prettify.css" rel="stylesheet" type="text/css"/>
	<script src="../jssamples/prettify/prettify.js" type="text/javascript"></script>
	<script src="../../scripts/jsdoc.js" type="text/javascript"></script>
	<title>Writing a Custom Dijit</title>
</head>

<body onload="prettify();">

<div id="pageBanner">Writing a Custom Widget<span class="bannerLinks"><a id="wpart0" href="#" onclick="javascript:genEmailLink(this)"><img border="0" src="../../graphics/mail.png" title="E-mail This Topic" alt="E-mail This Topic"/></a><a id="wpart1" href="#" onclick="javascript:printTopic();"><img border="0" src="../../graphics/print.png" title="Printable Version" alt="Printable Version"/></a><a id="wpart2" href="http://support.esri.com/en/feedback" target="_blank"><img border="0" src="../../graphics/feedback.png" title="Give Us Feedback" alt="Give Us Feedback"/></a></span></div>

<div id="nstext">

<!--***START TEXT - DO NOT WRITE ABOVE THIS LINE***-->
<p>Dijit is the user interface library of the Dojo Toolkit that provides not only an <a href="http://archive.dojotoolkit.org/nightly/dojotoolkit/dijit/themes/themeTester.html" target="_blank">extensive library of pre-built and tested widgets</a>, but also a system for building your own custom widgets. This document will walk through creating a custom widget using Dijit and show how to use a custom dijit in an ArcGIS API for JavaScript application.</p>

<h4>Dijit Resources</h4>
<p>There are quite a few resources already available for getting to know Dijit. To gain a general understanding of how Dijit works, it is recommended to read through the material listed below. For instance, it is beneficial to know the difference between declarative and programmatic widget creation, the difference between dojo.byId and dijit.byId and the purpose dijit.registry (hint:  all answers are in the first link in the list).</p>
<ul>
  <li><a href="http://dojotoolkit.org/reference-guide/1.7/dijit/info.html#dijit-info" target="_blank">Dijit Introduction:  The Basics</a>
  <li><a href="http://dojotoolkit.org/reference-guide/1.7/dijit/index.html" target="_blank">Dijit Overview:  List of all Classes in the Dijit Namespace</a>
  <li><a href="http://dojotoolkit.org/documentation/tutorials/1.7/understanding_widgetbase/" target="_blank">Dojo Tutorial:  Understanding _WidgetBase</a>
  <li><a href="http://dojotoolkit.org/documentation/tutorials/1.7/templated/" target="_blank">Dojo Tutorial:  Creating Template-based Widgets</a></a>
  <li><a href="http://dojotoolkit.org/documentation/tutorials/1.7/recipes/custom_widget/" target="_blank">Dojo Tutorial:  Creating a Custom Widget</a>
</ul>

<h4>Why Dijit?</h4>
<p>The links above do a great job of describing why Dijit exists and many of the benefits of using Dijit. To re-cap, below is a list of why buidling custom dijits is a good idea when developing web applications:  </p>
<ul>
  <li>Provides a widget lifecycle (via "_Widget" or "_WidgetBase") to manage creation, interaction with and teardown of widgets
  <li>Built-in HTML templating system (using the "_Templated" mixin) that includes template loading, variable substitution, a convenient way to attach DOM nodes to widget properties and a convenient way to set up event listeners
  <li>Standard file structure to organize your widget's resources
</ul>

<h4>Creating the esri.dijit.Gauge Widget</h4>
<p>To illustrate the process of building a custom widget, the rest of this document will walk through how the Gauge dijit (released with version 3.0 of the API) was built. To see a working example, head over to the <a href="../jssamples/widget_gauge.html" target="_blank">Gauge sample</a>. The <a href="http://www.arcgis.com/home/item.html?id=8551c1bbcbec4b0e8a0415c08d053dfb" target="_blank">source and associated resources for the Gauge widget</a> are also available on ArcGIS.com as a zip file.</p>

<p>As mentioned previously, one of the recommended practices of custom dijit development is to use a standard file structure. This file structure consists of a JavaScript file at the root of a directory that is named to match the name of the custom dijit. In the case of the Gauge, this JavaScript file is named Gauge.js and lives in esri/dijit/. At the same level as the JavaScript file are folders that contain additional pieces of the widget:  css, images, templates and sometimes others, such as data. The Gauge uses css and templates for its stylesheet and HTML template.</p>

<p>Once the standard file structure is in place, the next step is to start building the JavaScript behind the widget. Since the widget is a class, this starts with dojo.provide, dojo.require and dojo.declare. For more on these, please refer to <a href="intro_javascript_classes.htm" target="_blank">Writing a Class</a>.</p>

<p>Here's the beginning of Gauge.js:  </p>
<div class="detailSample">
<pre class="prettyprint" style="border:none;">
dojo.provide("esri.dijit.Gauge");

dojo.require("dojo.cache");
dojo.require("dijit._Widget");
dojo.require("dijit._Templated");
dojo.require("dojox.widget.AnalogGauge");
dojo.require("dojox.widget.gauge.AnalogArcIndicator");

dojo.declare("esri.dijit.Gauge", [dijit._Widget, dijit._Templated], { 
</pre>
</div>

<p>The class name is defined, dependencies are required and a call to dojo.declare gets things under way. As shown here, the standard _Widget and _Templated classes are brought in and three other classes are loaded:  dojo.cache is used to load the widget's HTML template while AnalogGauge and AnalogArcIndicator are included to build the actual esri.dijit.Gauge UI.</p>

<p>The first thing that happens inside the Gauge declaration is to define a couple of custom properties:  </p>
<div class="detailSample">
<pre class="prettyprint" style="border:none;">
dojo.declare("esri.dijit.Gauge", [dijit._Widget, dijit._Templated], {
  templateString: dojo.cache("esri.dijit", "templates/Gauge.html"),
  widgetsInTemplate: false,
</pre>
</div>
<p>templateString specifies where the widget's template file lives and widgetsInTemplate indicates whether or not the template includes additional widgets, and if so create them automatically when the widget is created. For the Gauge, there are no additional widgets in the template. Please refer to the <a href="http://dojotoolkit.org/documentation/tutorials/1.7/templated/" target="_blank">Dojo tutorial title "Creating Template-based Widgets"</a> for more information.</p>

<p>After properties are defined, the <a href="http://dojotoolkit.org/reference-guide/1.7/dijit/_WidgetBase.html#dijit-widgetbase" target="_blank">dijit lifecycle</a> begins. Up first is constructor:  </p>
<div class="detailSample">
<pre class="prettyprint" style="border:none;">
  constructor: function(options, srcRefNode) {
    // expected properties and values for the options argument:
    //  caption:  text to display below the gauge
    //  color:  color for the gauge indicator
    //  dataField:  attribute field used to drive the gauge
    //  dataFormat:  whether to display an actual value or a percentage, valid values are "value" or "percentage", tick marks are only added when this is "percentage"
    //  dataLabelField:  attribute field to use display a feature's name, displayed below gauge title
    //  maxDataValue:  maximum value for the gauge
    //  noDataLabel:  string to use when a feature doesn't have a value for dataLabelField
    //  numberFormat:  object passed to dojo.number.format, see dojo documentation for details:  http://dojotoolkit.org/api/1.6/dojo/number, most common options:  specify a number of decimal places, for instance: { "places": 2 }
    //  title:  title for the gauge
    //  unitLabel:  label attribute field being displayed, use "" for no label
    //  fromWebmap:  boolean, if true, all options listed above are ignored and the JSON from a webmap is used to create the gauge
    //
    // srcRefNode is the DOM node where the gauge will be created

    // set up widget defaults
    this.caption = "&nbsp;";
    this.color = "#000";
    this.dataFormat = "value";
    this.maxDataValue = 100;
    this.title = "&nbsp;";
    this.unitLabel = "";
    this.fromWebmap = false;

    // used to keep a reference to the current graphic
    this.feature = null;

    // start with no feature name
    this.dataLabel = "&nbsp;";

    // mixin constructor options 
    dojo.safeMixin(this, options);

    // default is to not show ticks on the gauge
    this._majorTicks = "";
    
    // initialize value to zero
    this.value = 0;

    // default to zero decimal places if numberFormat is not provided
    this.numberFormat = this.numberFormat || { "places": 0 };

    if ( this.fromWebmap ) {
      // map properties from webmap JSON gadget to names that the gauge widget expected
      this.dataField = this.field;
      this.dataFormat = this.valueLabel;
      this.dataLabelField = this.displayField;
      this.maxDataValue = this.target;
      this.unitLabel = "";
    }

    if ( this.dataFormat == "percentage" ) {
      this.unitLabel = "%";
      this._majorTicks = { offset: 90, interval: 25, length: 3, color: "black" };
    }

    // watch updates of public properties and update the widget accordingly
    this.watch("caption", this._updateCaption);
    this.watch("dataLabel", this._updateDataLabel); 
    this.watch("title", this._updateTitle);
    this.watch("value", this._updateValue); 
    this.watch("feature", this._updateFeature); 
  },
</pre>
</div>

<p>The constructor function of a widget should accept two arguments:  an object containing options supported by the widget and a reference to the DOM element where the widget's user interface will be rendered. The main thing that happens in constructor is that widget properties are initialized. Also, because the Gauge widget can be created from a regular JSON object with specific properties and values, or from JSON stored in an <a href="http://help.arcgis.com/en/arcgisexplorer/help/index.html#//01560000004z000000" target="_blank">ArcGIS Online Webmap that has gauge gadget definitions</a>, checking is done to handle both cases. Finally, some watches are set up to monitor and react when certain attributes are set.</p>

<p>The Gauge widget does not do anything with the next five stages of the widget lifecycle (postScript, create, postMixinProperties, buildRenderering and postCreate) but it's worth mentioning that postCreate is the next stage at which developers usually interact with custom widgets. The main reason postCreate is important is because it is the stage where a widget's DOM structure is built and available to manipulate but the widget is not yet in the page yet.</p>

<p>The next step in the lifecycle, startup, is used by the Gauge: </p>
<div class="detailSample">
<pre class="prettyprint" style="border:none;">
  startup: function() {
    this.inherited(arguments);

    // create gauge now that the template has been inserted into the DOM
    // using startup instead of postCreate because some element
    // dimensions are needed
    var gaugeBackground = new dojox.widget.gauge.AnalogArcIndicator({
      interactionMode: "gauge",
      noChange: true,
      value: this.maxDataValue,
      width: 20,
      offset: 65,
      color: [204, 204, 204, 1], 
      title: "value",
      hideValue: true,
      duration: 100 // default in dojo is 1000
    });
    
    var indicator = new dojox.widget.gauge.AnalogArcIndicator({
      interactionMode: "gauge",
      noChange: false,
      value: this.value,
      width: 20,
      offset: 65,
      color: this.color,
      title: "value",
      hideValue: true,
      duration: 100 // default in dojo is 1000
    });

    this.gaugeWidget = new dojox.widget.AnalogGauge({
      background: [204, 204, 204, 0.0],
      width: parseInt(this.gaugeNode.style.width),
      height: parseInt(this.gaugeNode.style.height) + 10, // add 10 px so ticks show
      cx: parseInt(this.gaugeNode.style.width) / 2, 
      cy: parseInt(this.gaugeNode.style.height),
      style: "position: absolute;",
      radius: parseInt(this.gaugeNode.style.width) / 2, 
      useTooltip: false,
      ranges: [{ low: 0, high: this.maxDataValue, color: "rgba(255,0,0,0)" }],
      majorTicks: this._majorTicks, 
      indicators: [ gaugeBackground, indicator ]
    }, dojo.create("div", null, this.gaugeNode));
    this.gaugeWidget.startup();

    // add percent label
    this.valueNode = dojo.create("div",{
      "innerHTML": "0" + this.unitLabel,
      "style": {
        "bottom": parseInt(this.gaugeNode.style.height) - (this.gaugeWidget.cy - 20) + "px",
        "color": "#000",
        "font-family": "arial",
        "font-size": "1em",
        "left": "-1000px",
        "position": "absolute"
      }
    }, this.gaugeWidget.domNode);
    
    // put the percent label in the middle of the gauge
    var contentBox = dojo.contentBox(this.valueNode);
    dojo.style(this.valueNode, "left", this.gaugeWidget.cx + "px");
    dojo.style(this.valueNode, "marginLeft", (-contentBox.w/2) + "px");
    if( this.gaugeWidget.cx ) {
      dojo.style(this.valueNode, "marginBottom", (-contentBox.h/2) + "px");
    }

    // only do this if a layer is passed in
    if ( this.layer ) {
      this._connectMouseOver();
    }
  },
</pre>
</div>

<p>startup is used by the Gauge (as opposed to postCreate) because the Gauge needs the widget's markup to be in the page so that all DOM nodes will have all measurements and positions available. Startup is where other dojo widgets are created and added to the Gauge widget. If the Gauge widget is passed a reference to a layer as part of the options supplied to the Gauge constructor then a mouse over listener is set up to update the Gauge as the mouse moves over features in the layer.</p>

<p>At the top of the startup method, <code>this.inherited(arguments)</code> is called. When overriding a method defined by a parent class, especially one defined by the Dijit lifecycle, such as startup, it is important to call <code>this.inherited(arguments)</code>. this.inherited calls the parent class's method with the arguments supplied to the current class's method. This is done so that _Widget's (the Gauge class's parent class) startup method is called and all of the necessary _Widget code is executed.</p>

<p>After startup is defined, the rest of the Gauge's methods are defined:  </p>
<div class="detailSample">
<pre class="prettyprint" style="border:none;">
  destroy: function() {
    if ( this._mouseOverHandler ) {
      dojo.disconnect(this._mouseOverHandler);
    }
    this.gaugeWidget.destroy();
    dojo.empty(this.domNode);
  },

  _connectMouseOver: function() {
    this._mouseOverHandler = dojo.connect(this.layer, "onMouseOver", dojo.hitch(this, function(e) {
      this.set("feature", e);
    }));
  },

  _formatValue: function(val) {
    if ( this.dataFormat == "percentage" ) {
      // calculate the percentage
      val = Math.round(( val / this.maxDataValue ) * 100);
    }
    return dojo.number.format(val, this.numberFormat);
  },

  _updateCaption: function(attr, oldVal, newVal) {
    this.captionNode.innerHTML = newVal;
  },

  _updateTitle: function(attr, oldVal, newVal) {
    this.titleNode.innerHTML = newVal;
  }, 

  _updateValue: function(attr, oldVal, newVal) {
    var val = this._formatValue(newVal);
    this.valueNode.innerHTML = val + this.unitLabel;
    this.gaugeWidget.indicators[1].update(parseInt(val));
  },

  _updateDataLabel: function(attr, oldVal, newVal) {
    this.dataLabelNode.innerHTML = newVal;
  },

  _updateFeature: function(attr, oldVal, newVal) {
    // check that this is either a graphic or
    // or an event object with a graphic as a property
    if ( ! newVal || 
         ( newVal.hasOwnPropety && // this is for IE8, using hasOwnProperty was causing an error
           ! newVal.hasOwnProperty("graphic") && 
           ! newVal.declaredClass == "esri.Graphic" )
       ) {
      console.log("Gauge Dijit:  a graphic is required to update the gauge.");
      return;
    }

    this.feature = newVal.graphic || newVal;
    
    // update the widget's percent value
    // use zero when feature's don't have a valid value
    this.set("value", this.feature.attributes[this.dataField] || 0);
    this.set("dataLabel", this.feature.attributes[this.dataLabelField] || this.noDataLabel);
  }

});
</pre>
</div>

<p>That completes the declaration of the JavaScript class for the widget.</p>

<p>The template file and the CSS are simple and straightforward. The template file:  </p>
<div class="detailSample">
<pre class="prettyprint" style="border:none;">
&lt;div class="gaugeContainer"&gt;
  &lt;div data-dojo-attach-point="titleNode" class="gaugeTitle"&gt;${title}&lt;/div&gt;
  &lt;div data-dojo-attach-point="dataLabelWrapperNode"&gt;
    &lt;div data-dojo-attach-point="dataLabelNode"&gt;${dataLabel}&lt;/div&gt;
  &lt;/div&gt;
  &lt;div data-dojo-attach-point="gaugeNode" style="margin: -10px auto 10px auto; width: 240px; height: 120px;"&gt;&lt;/div&gt;
  &lt;div data-dojo-attach-point="captionNode" class="gaugeCaption"&gt;${caption}&lt;/div&gt;
&lt;/div&gt;
</pre>
</div>
<p>In the markup above, there are two things to note. First, in addition to the typical HTML attributes such as class and style, special attributes are used for some of the elements. Using the data-dojo-attach-point attribute allows assignment of a unique name for an element that can be used to reference to an element from the widget's JavaScript class. For example, the _updateTitle function refers to the div element that displays the title as "this.titleNode" where "titleNode" is the unique name assigned to it in the template. Second, substitution variables happens via Dojo's string substitution syntax. ${title}, ${dataLabel} and ${caption} are example of this. Note that title, dataLabel and caption are properties of the widget (see constructor function above).  When the widget is created, these variables will be replaced with the values of corresponding properties of the widget.</p>
<p>And the stylesheet:  </p>
<div class="detailSample">
<pre class="prettyprint" style="border:none;">
.gaugeContainer {
  background: #fff;
  border-top: 1px solid #ccc; 
  color: #444;
  font-family: arial;
  font-size: 0.8em;
  height: 170px;
  padding: 5px 5px 5px 5px;
  width: 280px;
}

.gaugeCaption {
  height: 1em;
  margin: 0;
}
</pre>
</div>

<h4>get() and set()</h4>
<p>One of the advantages to using _Widget is that it includes all of the components for simple getters and setters. When using a custom widget, simply call get() or set() to retrieve or set a widget attribute value. For instance, to update the Gauge's title, use <code>gauge.set("title", "New Title");</code>.</p>

<h4>Using a Custom Dijit</h4>
<p>Custom dijits are used in the same way as the Dijit classes that ship with Dojo. In this case, the esri.dijit.Gauge dijit is recommended to be used programmatically. This means creating the dijit with JavaScript (as opposed to markup) and manually calling startup(). To see example of how to use this widget refer to the samples in the ArcGIS API for JavaScript SDK that use the Gauge widget:  </p>
<ul>
  <li><a href="../jssamples/widget_gauge.html" target="_blank">Gauge</a>
  <li><a href="../jssamples/widget_gauge_webmap.html" target="_blank">Gauge from a Webmap</a>
</ul>

<!--***END TEXT - DO NOT WRITE BELOW THIS LINE***-->
</div>
<div id="wh_footer" class="footnote" align="center" >
   Please visit the <a href="http://support.esri.com/en/feedback" target="_blank">Feedback</a> page to comment or give suggestions.<br>
   Copyright &copy; Environmental Systems Research Institute, Inc.
</div>

</body>
</html>
