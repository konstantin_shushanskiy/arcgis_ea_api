<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<meta name="keywords" content="ArcGIS JavaScript API" />
	<link href="jsdoc.css" rel="stylesheet" type="text/css"/>
  <link href="../jssamples/prettify/prettify.css" rel="stylesheet" type="text/css"/>
  <script src="../jssamples/prettify/prettify.js" type="text/javascript"></script>
  <script src="../../scripts/jsdoc.js" type="text/javascript"></script>
	<title>Feature Layer Best Practices</title>
</head>

<body onload="prettify();">

<div id="pageBanner">Feature Layer Best Practices<span class="bannerLinks"><a id="wpart0" href="#" onclick="javascript:genEmailLink(this)"><img border="0" src="../../graphics/mail.png" title="E-mail This Topic" alt="E-mail This Topic"/></a><a id="wpart1" href="#" onclick="javascript:printTopic();"><img border="0" src="../../graphics/print.png" title="Printable Version" alt="Printable Version"/></a><a id="wpart2" href="http://support.esri.com/en/feedback" target="_blank"><img border="0" src="../../graphics/feedback.png" title="Give Us Feedback" alt="Give Us Feedback"/></a></span></div>

<div id="nstext">

<!--***START TEXT - DO NOT WRITE ABOVE THIS LINE***-->
<p>When using a Feature Layer in ONDEMAND mode, the ArcGIS API for JavaScript intelligently handles retrieval of features using two techniques:  vector tiling and feature generalization. The purpose and use of both are outlined below.</p>

<h3>Vector Tiling</h3>
<p>
  Vector tiling is a relatively new topic in the realm of web mapping applications. You are probably familiar with the raster tiles that you can create en masse with ArcGIS Server to speed up your web app. Vector tiles are a little different. The underlying idea is that a static grid is used to issue a series of requests to retrieve vector features. The features are added to the map, but they are also stored in your browser cache.
</p>

<p>
  What is the motivation behind vector tiling? Some benefits include:
    <ul>
      <li>Dynamic interaction with individual features to change feature symbols, make selections, or work with feature attributes without going back to the server</li>
      <li>Features are retrieved from the browser cache when the same request (URL) is issued more than once (think <a href="http://support.google.com/webmasters/bin/answer.py?hl=en&answer=40132">HTTP status code 304</a>)</li>
      <li>Only features that intersect the map's current extent are retrieved</li>
      <li>Improved user experience and performance by using multiple queries to retrieve features in reasonable numbers rather than a single query to retrieve all features</li>
    </ul>
</p>

<p>
One thing to note, especially for veteran users of ArcGIS Server that are familiar with traditional map caches, is that vector tiles are generated on the fly and they are not persisted on the server. For vector tiles, there is no cache building and no additional server maintenance required. Vector tiles are only cached on the client and are used on a session-by-session basis.
</p>

<p>
To demonstrate vector tiling, and the associated browser caching, open the <a href="http://help.arcgis.com/en/webapi/javascript/arcgis/help/jssamples_start.htm#jssamples/fl_ondemand.html">Feature Layer with ONDEMAND mode</a> sample. As the hydro features load, notice that they are displayed in chunks. This is because features are queried from the server using the virtual grid mentioned previously. As queries complete, features are added to the map. To explore further, open Firebug or the Chrome Developer Tools and select the Net tab or the Network pane. Pan to get the feature layer to make some requests back to the server. Initially, requests are sent to the server to retrieve features. If the map is panned to somewhere it has been before, you'll start seeing HTTP 304s being returned:
<br />
<img src="images/feature_layer_304s.jpeg" />
</p>

<p>
What does status 304 mean? Basically, it is the server's way of saying it has already responded to this request (URL) and that the browser should use the previous response. When this happens, the server does not send any features over the wire and the browser uses the previous response by pulling it out of the browser cache. Because the Feature Layer uses a grid to generate query URLs, and the browser caches responses to these URLs, the server does not need to send the same features over the wire multiple times.
</p>

<p>
Vector tiling does not address generalizing features. As a map scale changes, the amount of detail required for each feature changes. If the map is at a small scale, more generalized features are appropriate. At larger scales, features need to show more detail. Feature Layers handle this via a parameter called <a href="http://help.arcgis.com/en/webapi/javascript/arcgis/help/jsapi_start.htm#jsapi/featurelayer.htm">maxAllowableOffset</a>.
</p>

<h3>Feature Generalization</h3>
<p>
  Feature generalization is a common enough scenario that it is built into Feature Layers and the ArcGIS Server REST API. ArcGIS Online generalizes features by default and, starting at version 2.7 of the ArcGIS API for JavaScript, Feature Layers generalize features by default as well. For more information on how this process works, and how to use it with pre-2.7 applications, see below.
</p>

<p>
  When a Feature Layer is created, one the options is maxAllowableOffset. This parameter name has its roots in the ArcGIS Desktop generalization tools. For more technical details, please refer to the <a href="http://en.wikipedia.org/wiki/Ramer%E2%80%93Douglas%E2%80%93Peucker_algorithm">Wikipedia page for the Douglas-Peucker algorithm</a>. If this is new terminology, refer to the ArcGIS Desktop Help for the <a href="http://help.arcgis.com/en/arcgisdesktop/10.0/help/index.html#//001v00000006000000.htm">Generalize tool</a>.
</p>

<p>
  Feature Layers come with a setter method <a href="http://help.arcgis.com/en/webapi/javascript/arcgis/help/jsapi/featurelayer.htm#setMaxAllowableOffset">setMaxAllowableOffset</a> that is used to simplify features on the fly. Since the web APIs fire an event when the map's extent/zoom level changes, you can listen for this event and use setMaxAllowableOffset to indicate an appropriate value.
</p>

<p>
  The recommended approach for generalizing features is that a feature's geometry should not display more than one vertex per pixel. The reasoning is that a pixel is the smallest unit for displays so displaying more than one vertex per pixel is wasted effort.
</p>

<p>
  To calculate an appropiate value for maxAllowableOffset, calculate the width of a pixel in map coordinates and pass it to the Feature Layer's constructor or setMaxAllowableOffset(). The easiest way to calculate the width of a pixel is to  divide map.extent.getWidth() by map.width. The code below shows how to create a Feature Layer specifying a value for maxAllowableOffset, how to set up a listener to update maxAllowableOffset when the map level changes and how to calculate maxAllowableOffset.
</p>

<pre class="prettyprint" style="border:none;">
app.fl = new esri.layers.FeatureLayer( "http://layer/url/0", {
    mode: esri.layers.FeatureLayer.MODE_ONDEMAND,
    maxAllowableOffset: calcOffset()
  }
);

dojo.connect(app.map, 'onZoomEnd', function() {
  app.maxOffset = calcOffset();
  app.fl.setMaxAllowableOffset(app.maxOffset);
});

function calcOffset() {
  return (app.map.extent.getWidth() / app.map.width);
}
</pre>

<p>Below are two graphics that show the benefits of using maxAllowableOffset. Notice that the features look nearly identical with and without maxAllowableOffset but the difference in response sizes is sigificant. In the first graphic, the features genereated using maxAllowableOffset result in a response size less than one quarter the size of features without specifying maxAllowableOffset. In the second graphic, using maxAllowableOffset reduces the response by over 95%.
<br />
<img src="images/graphics_NE_states.png" />
<br />
Using maxAllowableOffset reduces feature size by more than <strong>75%</strong> with no discernable difference in feature appearence.
<br /><br />
<img src="images/graphics_MS_counties.png" />
<br />
Using maxAllowableOffset reduces feature size by more than <strong>95%</strong> with no discernable difference in feature appearence.
</p>

<p> 
  Because this is such a useful idea, it is done automatically for Feature Layers used in <a href="http://arcgis.com/">ArcGIS.com</a>.
</p>

<p>
  As stated above at the beginning of this section, ONDEMAND mode Feature Layers created when using version 2.7 or greater of the API auto-generalize features using the method outlined above. The advantage of this is that developers do not have to set up an event listener for map level changes, calculate a value for maxAllowableOffset and set the value on Feature Layers- it is done automatically.
</p>

<p>
  One caveat:  maxAllowableOffset is not applicable on layers that are editable. If editing a feature, the true geometry of that feature should be displayed, including all its participating vertices. Editing a generalized feature could inadvertently wipe out detailed information that was meticulously created.
</p>

<h3>Related Samples</h3>
<p>The samples below showcase the Feature Layer capabilities outlined above in action.</p>
<a href="../jssamples_start.htm#jssamples/fl_ondemand.html" target="_blank">Feature Layer On Demand Mode</a><br />
<a href="../jssamples_start.htm#jssamples/fl_generalize.html" target="_blank">Generalize Features</a><br />
<a href="../jssamples_start.htm#jssamples/fl_performance.html" target="_blank">Feature Layer Performance</a> <br />
<a href="http://servicesbeta.esri.com/demos/graphics-performance/polys.html" target="_blank">Incrementally Add U.S. County Polygons To a Map</a> <br />
<!--***END TEXT - DO NOT WRITE BELOW THIS LINE***-->
</div>
<div id="wh_footer" class="footnote" align="center" >
   Please visit the <a href="http://support.esri.com/en/feedback" target="_blank">Feedback</a> page to comment or give suggestions.<br>
   Copyright &copy; Environmental Systems Research Institute, Inc.
</div>
</body>
</html>

