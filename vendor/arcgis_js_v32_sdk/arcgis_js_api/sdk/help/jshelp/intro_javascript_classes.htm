<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
  <meta name="keywords" content="ArcGIS JavaScript API" />
  <link href="jsdoc.css" rel="stylesheet" type="text/css"/>
  <link href="../jssamples/prettify/prettify.css" rel="stylesheet" type="text/css"/>
  <script src="../jssamples/prettify/prettify.js" type="text/javascript"></script>
  <script src="../../scripts/jsdoc.js" type="text/javascript"></script>
  <title>Writing a Class</title>
</head>

<body onload="prettify();">

<div id="pageBanner">Writing a Class<span class="bannerLinks"><a id="wpart0" href="#" onclick="javascript:genEmailLink(this)"><img border="0" src="../../graphics/mail.png" title="E-mail This Topic" alt="E-mail This Topic"/></a><a id="wpart1" href="#" onclick="javascript:printTopic();"><img border="0" src="../../graphics/print.png" title="Printable Version" alt="Printable Version"/></a><a id="wpart2" href="http://support.esri.com/en/feedback" target="_blank"><img border="0" src="../../graphics/feedback.png" title="Give Us Feedback" alt="Give Us Feedback"/></a></span></div>

<div id="nstext">

<!--***START TEXT - DO NOT WRITE ABOVE THIS LINE***-->

<p>Most developers who have worked on the web for a significant amount of time have likely evolved in how they manage their JavaScript code. When first starting out, it is common to put everything (HTML, CSS and JavaScript) in a single file. Eventually, this becomes cumbersome and JavaScript (as well as CSS) is moved to a separate file or files. As the amount of JS code continues to grow, so do the number of JS files. Over time, managing several different files that reference variables and objects defined in other places becomes hard to maintain.</p> 

<p>One solution to this problem is to use patterns from object-oriented(OO) programming to handle large JavaScript code bases. By using an object-oriented style, you can avoid spaghetti-code in your apps as well as increase the likelihood for code to be re-used. Application maintenance is also simplified allowing for bugs to be fixed faster and new features to be implemented in less time.</p>

<p>The primary goal of this tutorial is to save developers some of the headache of figuring this out on their own, provide some examples of how to write classes with the tools provided by Dojo and how to use classes provided by both Dojo and the ArcGIS API for JavaScript in your own classes.</p>

<p>Before working with the various Dojo components that help make OO JS possible, developers need to spend a little time reading through a couple of Dojo tutorials:</p> 
<ul>
  <li><a href="http://dojotoolkit.org/documentation/tutorials/1.7/modules/" target="_blank">Defining Modules</a>
  <li><a href="http://dojotoolkit.org/documentation/tutorials/1.7/declare/" target="_blank">Classy JavaScript with dojo/_base/declare</a>
</ul>
<p>Those tutorials provide the basic concepts that will be used throughout this tutorial to illustate best practices in organizing JavaScript code.</p>

<h4>Writing a Class</h4>
<p>Creating Dojo classes follows a common pattern:  </p>
<ul>
  <li>Use <a href="http://dojotoolkit.org/reference-guide/1.7/dojo/provide.html" target="_blank">dojo.provide()</a> to name your module and tell the loader about a module
  <li>Use <a href="http://dojotoolkit.org/reference-guide/1.7/dojo/require.html" target="_blank">dojo.require()</a> to load dependencies
  <li>Use <a href="http://dojotoolkit.org/reference-guide/1.7/dojo/declare.html" target="_blank">dojo.declare()</a> to create a new class
</ul>
<p>Note that in Dojo, a module and a class are not the same thing. In the examples shown here, each module contains a single class for simplicity. But it is possible for a module to contain multiple classes. When loading dependencies using dojo.require, module names are used. When using a class, the class name is used.</p>
<p>As an example, this tutorial will walk through writing a class to search <a href="http://seatgeek.com/" target="_blank">Seat Geek</a> via their <a href="http://platform.seatgeek.com/" target="_blank">API</a> which accepts a latitude, longitude coordinate pair and a search radius. The Seat Geek API returns information for events (concerts, baseball games, etc.) that fall within the specified area of interest.</p>

<p>The class to search Seat Geek will be called "SeatGeekSearch" and will live in the "extras" namespace. The module name where this class will live is "extras.SeatGeekUtils". Here's the dojo.provide that goes at the top of the file:  </p>
<div class="detailSample">
<pre class="prettyprint" style="border:none;">
dojo.provide("extras.SeatGeekUtils");
</pre>
</div>

<p>There is a single dojo.require for this class:</p>
<div class="detailSample">
<pre class="prettyprint" style="border:none;">
dojo.require("esri.utils");
</pre>
</div>
<p>That code loads the module used to retrieve data from the Seat Geek API. The actual function used to retrieve data is <a href="../jsapi/namespace_esri.htm" target="_blank">esri.request</a>.</p>

<p>Next up is the actual class declaration:  </p>
<div class="detailSample">
<pre class="prettyprint" style="border:none;">
dojo.declare("extras.SeatGeekSearch", null, { ... });
</pre>
</div>

<p>Declare takes three parameters:  class name, superclass(es) and an object with properties and methods. Please refer to <a href="http://dojotoolkit.org/documentation/tutorials/1.7/declare/" target="_blank">Dojo's "Classy JavaScript" tutorial</a> for more information on each of the three parameters. All code for a custom class will live inside the object that is passed to declare as the third argument. If migrating from global functions to classes, this third parameter will likely include code that was previously stored in global functions.</p>

<p>Below is the object that is passed to declare to implement the SeatGeekSearch class's custom functionality:</p>

<div class="detailSample">
<pre class="prettyprint" style="border:none;">
{
  distance: null,
  lastSearchResult: null,
  perPage: null,
  queryParams: null,
  seatGeekUrl: null,

  constructor: function(args){
    // specify class defaults
    this.distance = args.distance || "20mi"; // default seat geek range is 30mi
    this.perPage = args.perPage || 50; // default to 50 results per page
    this.seatGeekUrl = "http://api.seatgeek.com/2/events";
    
    // returnEvents is called by an external function, esri.request
    // dojo.hitch is to provide the proper context so that returnEvents
    // will have access to the instance of this class
    this.returnEvents = dojo.hitch(this, this.returnEvents);
  },

  searchByLoc: function(geopoint) {
    var eventsResponse;

    this.queryParams = {
      "lat": geopoint.y,
      "lon": geopoint.x,
      "page": 1,
      "per_page": this.perPage,
      "range": this.distance
    }

    // seat geek endpoints:
    // petco park search using lat, lon:
    // http://api.seatgeek.com/2/events?lat=32.7078&lon=-117.157&range=20mi&callback=c
    // lat, lon for petco park:  32.7078, -117.157
    eventsResponse = esri.request({
      "url": this.seatGeekUrl,
      "callbackParamName": "callback",
      "content": this.queryParams
    });
    return eventsResponse.then(this.returnEvents, this.err);
  },

  getMore: function() {
    var eventsResponse;

    // increment the page number
    this.queryParams.page++;
    
    eventsResponse = esri.request({
      "url": this.seatGeekUrl,
      "callbackParamName": "callback",
      "content": this.queryParams
    });
    return eventsResponse.then(this.returnEvents, this.err);
  },

  returnEvents: function(response) {  
    // check number of results
    if ( response.meta.total == 0 ) {
      // console.log("Seat Geek returned zero events: ", response);
      return null;
    }

    // save search result
    this.lastSearchResult = response;
    // console.log("set last search result: ", response, this);

    return response;
  },

  err: function(err) {
    console.log("Failed to get results from Seat Geek due to an error: ", err);
  }
}
</pre>
</div>

<p>Starting at the top, various class properties are initialized to null. When the class's constructor runs (which happens whenever <code>new extras.SeatGeekSearch({...})</code> is run), class properties are populated with values supplied via the object passed to the class constructor or with default values. The code in the constructor is also using <a href="http://dojotoolkit.org/reference-guide/1.7/dojo/hitch.html">dojo.hitch</a> to provide the proper contenxt for one of the class's methods so that it will be able to access the correct instance properties when called from another function (esri.request calls returnEvents as a callback function).</p>

<p>Once there is an instance of a class, its methods are accessible in the application where the instance is created. In the case of SeatGeekSearch, the main method of interest is searchByLoc. This method takes a point (a JavaScript object with an x and y property corresponding to longitude and latitude) and sends it to Seat Geek's API. Once the Seat Geek API returns, results are passed back to the application. There are a couple of additional methods that are also worth exploring but will not be discussed in this tutorial. An example of the SeatGeekSearch class in action is here:  <a href="http://servicesbeta.esri.com/demos/using-classes-with-javascript/legacy/seatgeek.html" target="_blank">search Seat Geek via a map click</a>. <a href="http://servicesbeta.esri.com/demos/using-classes-with-javascript/legacy/extras/SeatGeekUtils.js" target="_blank">Code for the extras.SeatGeekSearch class</a>.</p>

<h4>Inheriting from an Esri Provided Class</h4>
<p>Occasionally, it is necessary to extend an Esri provided class to create a new class that behaves slightly differently than the Esri version. For instance, one common request is to use a <a href="../jsapi/graphicslayer.htm" target="_blank">Graphics Layer</a> or <a href="../jsapi/featurelayer.htm" target="_blank">Feature Layer</a> as a base layer. The standard use case is that someone wants a map but does not necessarily need a detailed basemap&mdash; only feature geometries, such as State boundaries, are required for the map. With the out-of-the-box layer classes, this is not possible. The solution is to define a new class that inherits from <a href="../jsapi/dynamicmapservicelayer.htm" target="_blank">DynamicMapServiceLayer</a> that does nothing when its <a href="../jsapi/dynamicmapservicelayer.htm#getImageUrl" target="_blank">getImageUrl</a> method is called.</p>

<p>Here's the code for this new class which is defined in a namespace named "extras". The class name is "EmptyLayer":  </p>

<div class="detailSample">
<pre class="prettyprint" style="border:none;">
dojo.provide("extras.EmptyLayer");

dojo.require("esri.layers.agsdynamic");
dojo.require("esri.tasks.geometry");

dojo.declare("extras.EmptyLayer", [esri.layers.DynamicMapServiceLayer], {
  constructor: function(params) {
    this.params = params || {};
    this.wkid = this.params.wkid || 102100;
    this.spatialReference = new esri.SpatialReference({ wkid: this.wkid });
    // extent for contiguous US
    var ext = new esri.geometry.Extent({ 
      "xmin":-14325844,
      "ymin":2207331,
      "xmax":-7121642,
      "ymax":6867214,
      "spatialReference":{ "wkid": 102100 }
    });

    if ( this.wkid != ext.spatialReference.wkid ) {
      // project extent for contiguous US to the supplied wkid
      var gs = new esri.tasks.GeometryService("http://sampleserver3.arcgisonline.com/ArcGIS/rest/services/Geometry/GeometryServer"),
          that = this;

      gs.project([ext], this.spatialReference, function(result) {
        that.initialExtent = that.fullExtent = result[0];
        that.loaded = true;
        that.onLoad(that);
      });
    } else {
      this.initialExtent = this.fullExtent = ext;
      this.loaded = true;
      this.onLoad(this);
    }
  },
  getImageUrl: function(extent, width, height, callback) {
    // do nothing...we want an empty layer
  }
});
</pre>
</div>

<p>Like the SeatGeekSearch class, the EmptyLayer class begins with a call to dojo.provide, loads dependencies via dojo.require and then creates the class with dojo.declare. The two dependencies, esri.layers.agsdynamic and esri.tasks.geometry, are modules where the classes used by the custom class are defined. esri.layers.agsdynamic contains the parent class for extras.EmptyLayer and esri.tasks.geometry contains the GeometryService class which is used to project extents when the layer is created in a spatial reference other than Web Mercator.</p>

<p>The extras.EmptyLayer can be seen in action in this example:  <a href="http://servicesbeta.esri.com/demos/using-classes-with-javascript/legacy/empty_basemap.html" target="_blank">Feature Layer as a Basemap</a>. <a href="http://servicesbeta.esri.com/demos/using-classes-with-javascript/legacy/extras/EmptyLayer.js" target="_blank">Code for the extras.EmptyLayer class</a>.</p>

<h4>Other Examples of Custom Classes in the ArcGIS API for JavaScript SDK</h4>
<p>Some might notice that the majority of the <a href="../jssamples_start.htm" target="_blank">samples provided by the ArcGIS API for JavaScript team</a> are pages where all markup (HTML), CSS and JavaScript live in the same file, and most do not use classes. The reasoning behind this is that the purpose of the samples is to illustrate one or two specific concepts and that sometimes using a class-based approach would be overkill. After all, one of the primary benefits to using the ArcGIS API for JavaScript is its low barrier to entry and that small, simple apps can be built quickly and easily. For most of the SDK samples, putting everything in a single file allows anyone to copy the application code and run it locally. This allows new developers to easily and quickly run a sample and start to tweak it to explore the API. To be clear, the pattern shown in the samples is not intended to be copied for large applications but is appropiate for small apps.</p>
<p>That being said, there are serveral samples in the SDK that create custom classes:</p>
<ul>
  <li><a href="../jssamples/exp_rasterlayer.html" target="_blank">Canvas with Raster Layer</a>
  <li><a href="../jssamples/layers_custom_wms.html" target="_blank">Creating a Custom Layer</a>
  <li><a href="../jssamples/graphics_undoredo.html" target="_blank">Custom Undo/Redo Operation</a>
  <li><a href="../jssamples/widget_extendInfowindow.html" target="_blank">Info Window - Custom</a>
  <li><a href="../jssamples/layers_point_clustering.html" target="_blank">Point Clustering</a>
  <li><a href="../jssamples/layers_web_tiled.html" target="_blank">Web Tile Layer</a>
</ul>

<h4>Asynchronous Module Definition(AMD)</h4>
<p>While version 3.0 of the ArcGIS API for JavaScript uses <a href="http://dojotoolkit.org/reference-guide/1.7/releasenotes/1.7.html" target="_blank">Dojo 1.7</a>, Esri modules have not been migrated to AMD yet. For more information on using AMD to create modules, please refer to the <a href="inside_dojo_amd.html" target="_blank">Dojo and AMD conceptual help topic</a>.

<!--***END TEXT - DO NOT WRITE BELOW THIS LINE***-->
</div>
<div id="wh_footer" class="footnote" align="center" >
   Please visit the <a href="http://support.esri.com/en/feedback" target="_blank">Feedback</a> page to comment or give suggestions.<br>
   Copyright &copy; Environmental Systems Research Institute, Inc.
</div>

</body>
</html>
