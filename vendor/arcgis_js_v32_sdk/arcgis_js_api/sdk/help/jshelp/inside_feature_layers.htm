<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<meta name="keywords" content="ArcGIS JavaScript API" />
	<link href="jsdoc.css" rel="stylesheet" type="text/css"/>
  <link href="../jssamples/prettify/prettify.css" rel="stylesheet" type="text/css"/>
  <script src="../jssamples/prettify/prettify.js" type="text/javascript"></script>
  <script src="../../scripts/jsdoc.js" type="text/javascript"></script>
	<title>Working with Feature Layers</title>
</head>

<body onload="prettify();">

<div id="pageBanner">Feature Layers<span class="bannerLinks"><a id="wpart0" href="#" onclick="javascript:genEmailLink(this)"><img border="0" src="../../graphics/mail.png" title="E-mail This Topic" alt="E-mail This Topic"/></a><a id="wpart1" href="#" onclick="javascript:printTopic();"><img border="0" src="../../graphics/print.png" title="Printable Version" alt="Printable Version"/></a><a id="wpart2" href="http://support.esri.com/en/feedback" target="_blank"><img border="0" src="../../graphics/feedback.png" title="Give Us Feedback" alt="Give Us Feedback"/></a></span></div>

<div id="nstext">

<!--***START TEXT - DO NOT WRITE ABOVE THIS LINE***-->
<p>The ArcGIS JavaScript API version 2.0 offers a new <a href="../jsapi_start.htm#jsapi/featurelayer.htm">feature layer</a> for working with client-side graphic features. You may be familiar with the graphics layer from previous releases. The feature layer inherits from the graphics layer, but offers additional capabilities such as the ability to perform queries and selections. Feature layers are also used for Web editing.</p>
		
		<p>Feature layers differ from tiled and dynamic map service layers because feature layers bring geometry information across to the client computer to be drawn by the Web browser. Feature layers potentially cut down on round trips to the server. A client can request the features it needs, then perform selections and queries on those features without having to request more information from the server. Feature layers are especially appropriate for layers that respond to user interaction, such as a mouse click or hover.</p>
		<p>A feature layer honors any definition queries, scale dependencies, and other properties configured on the layer in the map service. Using a feature layer, you can access related tables, perform queries, display time slices, work with feature attachments, and do various other useful things.</p>
		<h3>How do you create a feature layer?</h3>
		<p>Feature layers can be created by referencing a layer from either a map service or a feature service or by specifying a feature colleciton object.</p>
		<ul>
			<li>Use a <b>map service</b> if you just want to retrieve geometries and attributes from the server and symbolize them yourself. (In the final release, the map service will be able to return its symbols.)</li>
			<li>Use a <b>feature service</b> if you want to take advantage of symbols from the service's source map document. Also use a feature service if you plan on doing editing with the feature layer. Feature layers honor any feature templates configured in the source map document.</li>
      <li>Feature collection objects are used to create a feature layer based on the supplied definition.</li>
		</ul>
		<h4>Choosing the display mode</h4>
		<p>When you create a feature service, you also need to specify a mode for retrieving features. Because the mode determines when and how features
    are brought from the server to the client, your choice can affect the speed and appearance of your application. You have the following mode choices:</p>
		<ul>
			<li><b>Snapshot mode</b> retrieves all the features from the layer immediately after the layer is added to the map. This eliminates any need to return to the server for information, but can become cumbersome if the layer contains many features. The browser may be overwhelmed by the number of features it needs to draw, or the service may reach its limit of how many features it can return at once. The default is 500 for ArcGIS Server 9.3.1 and 1000 for ArcGIS Server 10, but this limit is configurable using the Parameters tab of the Service Properties dialog box in ArcCatalog or Manager. Use snapshot mode only with smaller datasets or when dealing with temporal data.</li>
			<li><b>On demand mode</b> retrieves features as they are needed, requesting only the features within your current view extent. On demand mode requires more round trips between the client and server and should not be used for showing temporal animations. However, it protects you from retrieving more features than you need.</li>
			<li><p><b>Selection only mode</b> does not initially request any features. Features are added only when a selection is made. This mode is useful when you cannot or do not want to bring all features into the client, but you want to highlight one or more features for a certain reason.</p> 
			<p>For example, suppose you have a roads layer that's configured to use cartographic representations, which are not supported for display using a feature layer. You want to perform some Web editing on the roads, but that requires a feature layer. What do you do?</p>
			<p>In this scenario, you configure a dynamic map service to show the roads, then use a feature layer with selection only mode to display just the road currently being edited. This selected road is drawn using a simple client-side symbol. Once the edit is applied, you can clear the selection and refresh the dynamic map service to see the updated roads.</p></li>
		</ul>
		<p><b>Note:</b> No matter which of the display modes you choose for your feature layer, selected features are always held on the client. For example, if you are using the on demand display mode and you've selected some features, panning away from those selected features will still result in the features being available on the client. Your application will not have to re-select the features or make any other request to the server if you pan back to the original area. For this reason, it's a good practice to clear your selected features once they are no longer needed.</p>
		<h4>Choosing the attributes you need</h4>
		<p>Feature layers not only retrieve feature geometries; they can also get attribute information. When you create the feature layer, you can specify which attributes, or "out fields" will be retrieved for your features. It's possible to just request "*" to get all the fields, but to reduce the amount of information sent between the client and server, you should request only the fields you need in your application. If you later perform a query using the feature layer, the query will honor the out fields you set.</p>
		<h3>What can you do with a FeatureLayer?</h3>
		<p>Feature layers make it possible for you to do all of the following:</p>
		<h4>Editing</h4>
		<p>Feature layers are the key to editing features with the Web APIs. The editing widgets included in the APIs are designed to work with feature layers. You can only perform editing on feature layers that reference a feature service. See <a href="../jshelp_start.htm#jshelp/inside_editing.htm">Editing</a> to learn more about this requirement and how to create a feature service.</p>
		<h4>Definition expressions</h4>
		<p>There may be times where you want to work with features that satisfy certain attribute criteria and disregard the rest. In this type of situation, you can set a <i>definition expression</i> on the feature layer to retrieve just the features you need. Every feature layer has a default definition expression, which comes from the definition query set in the source map document. You can set a new definition expression as you work with the feature layer.</p>
		<h4>Time definitions</h4>
		<p>You can use a feature layer to isolate features that meet a certain temporal criteria. For example, you might want to show only houses that were constructed before 1930. If your house layer is time-aware, you can set a <i>time definition</i> on the feature layer to show only houses whose construction dates were before 1930.</p>
		<h4>Selection and display</h4>
		<p>The feature layer supports a <i>selection</i>, which represents a subset of features that the user has chosen to isolate for viewing, editing, or analytical purposes. Users can add or remove features from the selection based on spatial or attribute criteria. The feature layer makes it easy to draw the selection set with a different type of symbol than the other features. This was previously challenging to do without using two graphics layers.</p>
		<p>A selection set can be used as input for statistical calculations, geoprocessing tools, and graphing, opening the possibility for interactive GIS analysis and exploration. For example, selections could allow a user to draw a polygon around a set of buildings, then push a button to calculate the solar potential of those buildings. Selections also play an important role in some editing workflows where the feature currently undergoing edits is displayed as selected.</p>
		<p>To make your selection, use the FeatureLayer.SelectFeatures() method, which takes a Query object as an argument. If you're just interested in querying your feature layer without adding the results to the selection set, you can use either QueryFeatures() or QueryRelatedRecords() instead. With any of these methods, you cannot request features outside of any definition expressions set in the map document or on the feature layer.</p>
    <h3>Related Samples</h3>
<p>The ArcGIS JavaScript API has several samples that show how to work with feature layers. Here are a few you may find useful:</p>
<a href="../jssamples_start.htm#jssamples/fl_selectfeatures.html" target="_blank">Select features using a feature layer</a><br />
<a href="../jssamples_start.htm#jssamples/fl_query_related_multiple.html" target="_blank">Query related tables using a feature layer</a><br />
<a href="../jssamples_start.htm#jssamples/fl_layer_definition.html" target="_blank">Apply a layer definition using a feature layer</a> <br />
<a href="../jssamples_start.htm#jssamples/ed_default_editingwidget.html" target="_blank">Editing features using a feature layer</a> <br />
<!--***END TEXT - DO NOT WRITE BELOW THIS LINE***-->
</div>
<div id="wh_footer" class="footnote" align="center" >
   Please visit the <a href="http://support.esri.com/en/feedback" target="_blank">Feedback</a> page to comment or give suggestions.<br>
   Copyright &copy; Environmental Systems Research Institute, Inc.
</div>
</body>
</html>
