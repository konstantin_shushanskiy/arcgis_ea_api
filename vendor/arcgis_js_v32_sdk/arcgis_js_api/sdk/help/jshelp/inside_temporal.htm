<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<meta name="keywords" content="ArcGIS JavaScript API" />
	<link href="jsdoc.css" rel="stylesheet" type="text/css"/>
  <link href="../jssamples/prettify/prettify.css" rel="stylesheet" type="text/css"/>
  <script src="../jssamples/prettify/prettify.js" type="text/javascript"></script>
  <script src="../../scripts/jsdoc.js" type="text/javascript"></script>
	<title>Working with time-aware layers</title>
</head>

<body onload="prettify();">

<div id="pageBanner">Working with time-aware layers<span class="bannerLinks"><a id="wpart0" href="#" onclick="javascript:genEmailLink(this)"><img border="0" src="../../graphics/mail.png" title="E-mail This Topic" alt="E-mail This Topic"/></a><a id="wpart1" href="#" onclick="javascript:printTopic();"><img border="0" src="../../graphics/print.png" title="Printable Version" alt="Printable Version"/></a><a id="wpart2" href="http://support.esri.com/en/feedback" target="_blank"><img border="0" src="../../graphics/feedback.png" title="Give Us Feedback" alt="Give Us Feedback"/></a></span></div>

<div id="nstext">

<!--***START TEXT - DO NOT WRITE ABOVE THIS LINE***-->

<p>ArcGIS 10 includes support for time-aware layers, which store information about the changing state of a dataset over time. Time-aware layers allow you to step through periods of time revealing patterns and trends in your data.  For example, you can: </p>
<ul>
<li>Track hurricane paths and other meteorological events.</li>
<li>Explore historic patterns in data through time, such as population or land use changes.</li>
<li>Monitor changes in well production or status.</li>
<li>Map the progression of a wildfire or flood.</li>
<li>Visualize the spread of disease over time.</li>
</ul>
<p>When time-aware layers are present in a Web API based application, the map is considered time-aware and the map's time extent is set. The time extent defines the time period for which layers' data is displayed in the map. Setting the map's time extent is similar to setting the spatial extent because once the time extent is set the map display updates automatically to conform to the change. Every time the map's time extent is changed all time-aware layers update to reflect the new extent.</p>

<p>There are several ways to work with time-aware layers in a Web API based application. The simplest is to use the <i>TimeSlider</i> widget because it handles the process of updating the map's time extent for you. Alternatively, you can use the Web APIs to build applications that perform temporal queries, filter layers using time definitions, and set the map's time extent.</p>

<h3>How do I make my layers time aware?</h3>
<p>You can make your layers time-aware using the <i>Layer Properties</i> dialog in ArcMap(right-click on the layer in ArcMap and choose <b>Properties</b> to display the dialog).</p>
<p><b>Note:</b>The <b>Time Field</b> must be of type date.</p>
<img src="images/LayerProperties.png" alt="Layer Properties"/>
<p>The time information is preserved and accessible through the map service when you publish your map to ArcGIS Server. After publishing, you can work with a time-aware map or feature service using the <i>ArcGISDynamicMapServiceLayer</i> or <i>FeatureLayer</i> classes to perform temporal queries and view changes in data over time.</p>

<p>In the Web APIs time-aware layers have a <i>timeInfo</i> property that provides access to the <i>TimeInfo </i>class. The <i>TimeInfo</i> class provides detailed information about the layer's time properties including the time range and time reference. The snippet below retrieves the time extent for a layer using the timeInfo property.</p>
<pre class="prettyprint" style="border:none;">
var layerTimeExtent = results[2].layer.timeInfo.timeExtent;
</pre>
<h3>Creating JavaScript dates to use with time-aware layers</h3>
<p>When working with time-aware layers you may need to create JavaScript dates to define a time extent or query a layer. When creating a new JavaScript date always specify UTC for the time zone. <a href="http://en.wikipedia.org/wiki/Coordinated_Universal_Time">Coordinated Universal Time (UTC)</a> is a time standard based on atomic time and is functionally equivalent to <a href="http://en.wikipedia.org/wiki/Greenwich_Mean_Time">Greenwich Mean Time(GMT)</a>. If no time zone is specified then the REST API returns the date string without time zone information. If you use this date string in a temporal query the time zone defaults to the current local time. This can result in dates that are off by several hours.</p>

<pre class="prettyprint" style="border:none;">
var timeExtent = new esri.TimeExtent();
timeExtent.startTime = new Date("1/25/2010 6:30:00 UTC");
</pre>

<h3>How do I work with time in the ArcGIS API for JavaScript?</h3>
<p>The ArcGIS API for JavaScript provides a <i><a href="../jsapi_start.htm#jsapi/timeslider.htm">TimeSlider</a></i> widget that simplifies the process of visualizing temporal data. Using the time slider, you can filter the map to display cumulative data up to a point in time, a single point in time, or data that falls within a time range. The benefit of using the time slider is that it handles setting the map's time extent, which filters time-aware layers to only display data for the current time extent.</p>
<p>To set up the time slider, add a time-aware map or feature service to the map using the ArcGISDynamicMapService or FeatureLayer class. Next, associate the time slider with the map. Then define the time slice to visualize and the number of tics (or stops) the slider displays.</p>
<pre class="prettyprint" style="border:none;">
timeSlider = new esri.dijit.TimeSlider({},dojo.byId("timeSliderDiv"));
map.setTimeSlider(timeSlider);
timeSlider.setThumbCount(1);
var layerTimeExtent = results[0].layer.timeInfo.timeExtent;
timeSlider.createTimeStopsByTimeInterval(layerTimeExtent,1,'esriTimeUnitsWeeks');
timeSlider.startup();
</pre>
<p>Slider thumbs denote a location on the slider. Notice that in the code above the thumb count is set to one; this means that the map will display all features from the specified layers start time to the current thumb position. If you want to visualize features for an instant in time, set the thumb count to one and specify that <i>singleThumbAsTimeInstant</i> equals true.</p>
<img src="images/sliderthumb.png" alt="Slider Thumb"/>
<p>To visualize a time range set the thumb count to two.</p>
<img src="images/slider2thumb.png" alt="Slider Range"/>
<p><b>Note:</b>It is not recommended to use the time slider with feature layers in on demand mode because this can result in too many requests to the server. If you are not working with a large amount of data, you can use a feature layer in snapshot mode. If your dataset is large, consider using an <i>ArcGISDynamicMapServiceLayer</i> instead.</p>
<h4>Filtering data using the map's setTimeExtent method</h4>
<p>There are other ways besides the <i>TimeSlider</i> to visualize time-aware layers in your mapping applications. The map has a <i><a href="../jsapi_start.htm#jsapi/map.htm#setTimeExtent">setTimeExtent</a> method</i>, which acts as a filter for layers that support time such as the <i>ArcGISDynamicMapServiceLayer</i> and <i>FeatureLayer</i>.  Setting the map's time extent fires the <i>onTimeExtentChanged</i> event. Time-aware layers listen for this event and update to display content for the specified time extent.  In this example, only data that meets the input time definition of January 15, 1989 appears.</p>
<pre class="prettyprint" style="border:none;">
var timeExtent = new esri.TimeExtent();
timeExtent.startTime = new Date("1/15/1989 UTC");
map.setTimeExtent(timeExtent);
</pre>
<h4>Filtering data using the layer's time definition</h4>
<p>Feature layers support the ability to set a <a href="../jsapi_start.htm#jsapi/featurelayer.htm#setTimeDefinition">time definition</a>, which limits the data that is loaded in the initial query.  Setting a time definition on layer is similar to setting a definition expression, and restricts the data to only features that match the input time extent. When you set a time definition on a feature layer in snapshot or on demand mode, the features that match the time definition are retrieved once the layer is added to the map. If you modify the time definition after the layer is added, then the existing set of features is removed and a request is made to retrieve the new set of features. The current selection is always maintained and will not be removed if you set a new time definition.</p>
<pre class="prettyprint" style="border:none;">
 var timeDef = new esri.TimeExtent();
 timeDef.startTime = new Date("09/25/2003 UTC");
 timeDef.endTime = new Date("10/25/2005 UTC");
 featureLayer.setTimeDefinition(timeDef);
</pre>
<h4>Querying data using a time extent</h4>
<p>You can perform temporal queries against time-aware layers using the <i><a href="../jsapi_start.htm#jsapi/query.htm#timeExtent">timeExtent</a></i> property on the <i>Query</i> object.   For example, you might want to know all crimes that occurred during the night shift from 10 PM to 6 AM on a particular date. </p>
<pre class="prettyprint" style="border:none;">
var timeExtent = new esri.TimeExtent();
timeExtent.startTime = new Date("1/17/2010 22:00:00");
timeExtent.endTime = new Date("1/18/2010 6:00:00");
var query = new esri.tasks.Query();
query.timeExtent = timeExtent;
featureLayer.queryFeatures(query,function(featureSet){
  //do something with the results here
});
</pre>
<h3>Related Samples</h3>
<p>The ArcGIS JavaScript API has several samples that show how to work with time-aware layers. Here are a few you may find useful:</p>
<a href="../jssamples_start.htm#jssamples/time_sliderwithfeaturelayer.html" target="_blank">Time slider that displays a date range (two thumbs)</a><br />
<a href="../jssamples_start.htm#jssamples/time_slider_layerDef.html" target="_blank">Uses the time slider's onTimeExtentChange event to calculate statistical information for the specified date range.</a> <br />

<!--***END TEXT - DO NOT WRITE BELOW THIS LINE***-->
</div>
<div id="wh_footer" class="footnote" align="center" >
   Please visit the <a href="http://support.esri.com/en/feedback" target="_blank">Feedback</a> page to comment or give suggestions.<br>
   Copyright &copy; Environmental Systems Research Institute, Inc.
</div>
</body>
</html>
