<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<meta name="keywords" content="ArcGIS JavaScript API" />
	<link href="jsdoc.css" rel="stylesheet" type="text/css"/>
  <link href="../jssamples/prettify/prettify.css" rel="stylesheet" type="text/css"/>
  <script src="../jssamples/prettify/prettify.js" type="text/javascript"></script>
  <script src="../../scripts/jsdoc.js" type="text/javascript"></script>
	<title>Editing using the ArcGIS Server JavaScript API</title>
</head>

<body onload="prettify();">

<div id="pageBanner">Editing<span class="bannerLinks"><a id="wpart0" href="#" onclick="javascript:genEmailLink(this)"><img border="0" src="../../graphics/mail.png" title="E-mail This Topic" alt="E-mail This Topic"/></a><a id="wpart1" href="#" onclick="javascript:printTopic();"><img border="0" src="../../graphics/print.png" title="Printable Version" alt="Printable Version"/></a><a id="wpart2" href="http://support.esri.com/en/feedback" target="_blank"><img border="0" src="../../graphics/feedback.png" title="Give Us Feedback" alt="Give Us Feedback"/></a></span></div>

<div id="nstext">

<!--***START TEXT - DO NOT WRITE ABOVE THIS LINE***-->

	<p>In Version 2.0, the ArcGIS APIs for JavaScript, Flex, and Silverlight introduce the ability to edit geographic data over the Web. Since a Web browser is a simple, familiar, and free application, Web-based editing allows a larger group of people to add and improve on your data. These include field workers, analysts in other departments of your company, or perhaps even crowd-sourced volunteers who may be able to make valuable contributions to your data given a simple editing interface.</p>
		<p>The APIs include widgets that help you add editing functionality to your applications. You can do some design work in both your map and your Web application to simplify the end user experience when editing. For example, you might predefine a set of feature templates, new in ArcGIS 10, to make it easier for users to add certain types of common features. You might also limit the attributes that can be added, and add some available attribute options in a drop down list to facilitate accurate, uniform data entry. As the designer of a Web editing application, you have complete control over how simple or complex of an editing experience you expose to your users.</p>
		<p>Web editing requires some initial setup of a feature service, which exposes information about a layer's feature geometries and attributes. Also, geometry services can be used to accomplish some Web editing tasks. In order to use the main Editor widget, for example, you must have a running geometry service.</p>
		<h3>What can I do with editing in the ArcGIS Web APIs?</h3>
		<p>The ArcGIS Web APIs provide simple feature editing. The features must come from an ArcSDE geodatabase. This geodatabase can be licensed for either workgroup or enterprise use and does not have to be versioned.</p>
		<p>When editing data with the Web APIs, you can add, delete, move, cut, union, and reshape features. You can also edit feature attributes. If you attempt to modify the same feature at the same time as someone else, the last edits to be made are the ones committed to the database.</p>
		<p>For some Web editing workflows, it's a good idea to have an analyst using ArcGIS Desktop periodically review the edits to verify data integrity. Although the editing widgets can perform some data validation, other tasks such as validating topologies cannot be performed over the Web.</p>
		<h3>How do I add editing functionality to my application?</h3>
		<p>The ArcGIS APIs for JavaScript, Flex, and Silverlight provide widgets to make it easier for you to add editing to your Web applications. The following widgets are available.</p>
		<h4>Editor widget</h4>
		<p>If you want to get started quickly, or if you just want to use the default editing interface included with the API, use the <a href="
../jsapi_start.htm#jsapi/editor.htm">Editor widget</a>. The Editor widget combines the functionality of the other widgets to provide everything that you need for editing a layer. You can choose the number and types of tools that are available on the widget.</p>
		<p>The Editor widget saves your edits immediately after they are made, for example, as soon as you finish drawing a polygon. If you decide not to use the Editor widget, you must determine for yourself when and how often you want to apply edits.</p>
		<p>If you don't use the Editor widget, you need to code your own editing experience. However, you can still take advantage of the Template Picker, Attribute Inspector, and Attachment Editor widgets to help you.</p>
		<h4>Template Picker</h4>
		<p>The <a href="../jsapi_start.htm#jsapi/templatepicker.htm">Template Picker</a> displays a set of preconfigured feature choices that the user can create. The Template Picker is a great way to expose preconfigured feature types for easy editing and quality control. The symbols visible in the Template Picker can come from either the editing templates that you defined in the feature service's source map, or they can be a set of symbols that you define in the application.</p>
		<p>A Template Picker is included in the Editor widget. The Template Picker can optionally be used on its own as a simple legend for your map. Click <a href="../jssamples_start.htm#jssamples/time_slider_layerDef.html">here</a> to view a sample that uses the Template Picker to display a legend for the map.</p>
		<h4>Attribute Inspector</h4>
		<p>When you make attributes available for editing on the Web, you need to provide an interface for users to edit the attributes, and you must ensure that the data they enter is valid. The <a href="../jsapi_start.htm#jsapi/attributeinspector.htm"> Attribute Inspector</a> widget can help you with both these tasks.</p><p>The Attribute Inspector reads the attribute fields available through your feature layer and provides a user interface for easily updating those fields. It also validates data entered in the fields, ensuring that the input matches the expected data type. For example, if a coded value domain is applied to a field, the permitted values appear in a drop down list, restricting the possibility of other values being entered. If a field requires a date value, a calendar appears, helping the user to supply a valid date.</p>
		<p>The Attribute Inspector exposes all available attributes on the layer for editing. If you want to restrict the available attributes, you must code your own interface for entering and validating values.</p>
		<p>An Attribute Inspector is included in the Editor widget.</p>
		<h4>Attachment Editor</h4>
		<p>In some situations, you may want to associate a downloadable file with a feature. For example, you might want users to be able to click a feature representing a piece of real estate and see a link to a PDF file of the title deed. In the ArcGIS Web APIs, an associated downloadable file like this is known as a feature attachment.</p>
		<p>The <a target="_top" href="../jsapi_start.htm#jsapi/attachmenteditor.htm">Attachment Editor</a> is a widget that helps users upload and view feature attachments. The Attachment Editor includes a list of current attachments (with Remove buttons), as well as a Browse button that can be used to upload more attachments. The Attachment Editor works well inside an info window, but can be placed elsewhere on the page.</p>
		<p>In order to use feature attachments, attachments must be enabled on the source feature class. You can enable attachments for a feature class in ArcCatalog or the Catalog window in ArcMap. If the Editor widget detects that attachments are enabled, it will include an Attachment Editor.</p>
		<h3>How does the editing work?</h3>
		<p>Editing with the ArcGIS Web APIs works through the feature service, a new type of service available with ArcGIS 10. Editing workflows can also take advantage of the geometry service.</p>
		
		<h4>The role of the feature service</h4>
		<p>Web editing requires a feature service to provide the symbology and feature geometry of your data. The feature service is just a map service with the Feature Access capability enabled. This capability allows the map service to expose feature geometries and their symbols in a way that is easy for Web applications to use and update.</p>
		<p>Before you build a Web editing application, you need to do some work to create a feature service exposing the layers that you want to be edited. This involves setting up a map document and, optionally, defining some templates for editing. Templates allow you to pre-configure the symbology and attributes for some commonly-used feature types. For example, to prepare for editing roads, you might configure templates for "Controlled Access Freeway", "Other Divided Highway", "State Highway", and "Local Road". Templates are optional, but they make it easy for the end user of the application to create common features.</p>
		<p>Once your map is finished, you need to publish it to ArcGIS Server with the Feature Access capability enabled. This creates REST URLs, or endpoints, to both a map service <i>and</i> a feature service. You will use these URLs to reference the services in your application.</p>
		<p>Feature services are accessible in the Web APIs through a new type of layer called a feature layer.  Feature layers can do a variety of things and can reference either map services or feature services; however, when you use a feature layer <i>for editing purposes</i> you need to reference a feature service.</p>
		<p>When you perform editing, your Web application tells the feature layer which attributes have changed and, if applicable, how the geometry changed. The feature layer is also displays the updated features after editing. You can call the <a href="../jsapi_start.htm#jsapi/featurelayer.htm#applyEdits">applyEdits</a> method on the feature layer to apply the edits, which then commits them to the database.</p>
		<h4>The role of the geometry service</h4>
		<p>The ArcGIS Server geometry service helps perform common editing operations such as creating, cutting, and reshaping geographic features. Before you use the Editor widget, you need to create a geometry service on your ArcGIS Server. Then when you create the widget, you must provide the URL to the geometry service. The widget uses the service behind the scenes, and you won't have to call methods on the geometry service yourself unless you decide not to use the widget.</p> 
		<p>If you decide not to use the Editor widget, you can still use the geometry service to help you code your own editing tools. The geometry service can also help you validate data. For example, you can use the geometry service to perform checks on edits, such as "no edit may fall outside this box", or "a polygon boundary may not cross itself". (The Editor widget actually calls the Simplify() method on the geometry service before it commits a geometry to your database.) Although ArcGIS topology validation is not available through the Editor widget or editing-related classes, the geometry service may help you achieve a similar result through these types of data integrity checks.</p>
		
			<h3>The Edit toolbar</h3>
		<p>The Edit toolbar is a class that helps you code the placing and moving of vertices and graphics. This is helpful in scenarios where you are not using the Editor widget and you need to write your own editing logic, especially the client display behavior. Like the other toolbars (Draw and Navigation), the Edit toolbar is not a user interface toolbar. Instead, it is a helper class that makes it easier to code some common related functions.</p>
	<h3>Designing your editing experience</h3>
		<p>When you build a Web editing application, it's important to think about who will use the application and what they will need to do. You should provide your users no more and no less than the tools they need. In most situations you can design an appropriate editing experience for your users through a combination of the editing widgets included with the API. Keep in mind that although the API includes a comprehensive Editor widget to help you get started, this particular widget may not be the best fit for your scenario.</p>
		<p>In some ways, the editing experience with the API has already been simplified for you. The widgets include no buttons for starting and stopping edit sessions, or for saving edits. You can optionally provide these functions, but they're not necessary for most applications.</p>
<p>Consider the following approaches for designing a Web editing application. Each approach has its own merits and types applications for which it is most appropriate.</p>
	<h4>Feature sketching</h4>
		<p>In this type of application, field analysts need to sketch simple events or ideas on the map and add associated comments. A bird sighting
, an envisioned trail, or a proposed wilderness area all all examples of the types of features that might be sketched in this type of
 application. In this scenario, ease of use and freedom for drawing intuitively on top of the map are more important than geometric accuracy.</p>
<p>When building a feature sketching application, you can use a Template Picker to limit the types of items users can sketch. The Attribute Inspector
 is also useful, for allowing editors to add comments, either in a side panel or in an info window that appears when the feature is clicked. Options for splitting and merging features, adding a point at an X/Y location, snapping, or uploading feature attachments may clutter the application and should be avoided unless absolutely needed.</p><p>Click <a href="../jssamples_start.htm#jssamples/ed_default_editingwidget.html" target="_blank">here</a> to view a sample that shows how to build a feature sketching application.</p>
	<h4>Attribute-only editing</h4>
		<p>Some editing applications include a mapping component, but don't require any changing of geographic features. For example, you might want to create a Web application allowing any office employee to edit the attributes of a parcel feature in your database. You want to prevent them from editing the geometry, leaving that task to your GIS analysts. In this scenario, you can include a map in your application, but expose editing only through the Attribute Inspector. A simple map click on a parcel displays the attributes to be edited. You don't need to include any buttons or widgets for creating, deleting, or reshaping features.</p>
<p>Click <a href="../jssamples_start.htm#jssamples/ed_attribute_inspector.html" target="_blank">here</a> to view a sample that shows how to build an attribute editing application. </p>
<!--		<h4>Editing adjacent polygons</h4>
		<p>In some situations, you want users to be able to reshape existing polygons or edit their attributes without causing any gaps or overlaps between features. The features might represent sales districts within a region, or voter precincts within a county.</p>
		<p>Your editors may not be GIS professionals who understand concepts such as topological relationships, vertices, or snapping. You need to provide them with a subset of tools such as Merge, Cut, and Reshape that modify features while preserving topological integrity. At the same time, you might not provide tools for adding or deleting features. An Attribute Inspector is optional in this type of application.</p>-->
		<h4>Citizen participation, or geo-wiki</h4>
		<p>This is a variation of the feature sketching scenario in which citizens can be the editors and report problems to a government organization by placing points on the map and including ancillary information such as photographs or a brief description.</p>
		<p>Users of this type of application do not even know that they are editing GIS data. They are only allowed to add point features to the database, using a carefully selected set of incident types that you predefined in a Template Picker. Attachments are allowed through the Attachment Inspector widget so that citizens can upload images of the incident. Attribute editing may be allowed to a very limited degree so that citizens can describe the incidents.</p>
		<p>In this type of application, you may have to code some security checks to ensure that users can only edit or delete their own incidents. You may also include logic to periodically clean out the database, or allow a subset of authenticated users to close or delete incidents.</p> 
<p>Click <a href="../jssamples_start.htm#jssamples/ed_relatededits.html" target="_blank">here</a> to view a sample that shows how to build a citizen participation application.</p>
	<!--	<h4>Specialized field work</h4>
		<p>Some applications are designed for specific types of field work. You can design these to closely fit the intended audience. Suppose you have many field analysts in charge of updating a streets database. When building them an application for field work, you can choose to allow creation and deletion of line features only. Snapping is enabled, but only on the street network layer. A Split tool is included, but not tools that apply primarily to polygons, such as Cut or Merge. The Template Picker widget makes it easy to denote which type of street is being created (unpaved road, paved minor road, arterial, and so on). An Attribute Inspector is optional and can help analysts set other selected properties. However, you may choose to limit the attributes available for editing in order to keep the application usable and preserve screen real estate.</p>-->

<!--***END TEXT - DO NOT WRITE BELOW THIS LINE***-->
</div>
<div id="wh_footer" class="footnote" align="center" >
   Please visit the <a href="http://support.esri.com/en/feedback" target="_blank">Feedback</a> page to comment or give suggestions.<br>
   Copyright &copy; Environmental Systems Research Institute, Inc.
</div>
</body>
</html>
